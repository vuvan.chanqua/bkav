﻿$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'containerTNN_2',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([11,15,11,38,25]);
            //                //chart.series[1].setData([15]);
            //                //chart.series[2].setData([11]);
            //                //chart.series[3].setData([38]);
            //                //chart.series[4].setData([25]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0,0,0,0,0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                //chart.series[3].setData([0]);
            //                //chart.series[4].setData([0]);
            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'Nhu cầu nước sử dụng từng lĩnh vực'
        },
        subtitle: {
            text: '',
            
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemStyle: {
                fontSize: '8px'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}%',
                    distance: -20,
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Phản ánh của người dân',
            colorByPoint: true,
            data: [ {
                name: 'Sinh hoạt',
                y: 11,
            }, {
                name: 'Công nghiệp',
                y: 15
            }, {
                name: 'Du lịch, dịch vụ',
                y: 11
            }, {
                name: 'Nông nghiệp',
                y: 38
            }, {
                name: 'Thủy sản',
                y: 25
            }]
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});

$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container10_2',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([90, 10]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([82, 18]);
                            count = 0;
                        }
                    }, 2000);
                }
            },
            //options3d: {
            //    enabled: true,
            //    alpha: 65,
            //    beta: 0,
            //    depth: 60,
            //    viewDistance: 50
            //},
        },
        title: {
            text: 'Kết quả đăng kí đất đai'
        },
        subtitle: {
            text: '',
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemStyle: {
                fontSize: '12px'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}%',
                    distance: -20,
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Phản ánh của người dân',
            colorByPoint: true,
            data: [{
                name: 'Đã xử lý',
                y: 90,
                sliced: false,
                selected: false
            }, {
                name: 'Đang xử lý',
                y: 10,
                color: 'orange'
            }]
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});