﻿$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            type: 'column',
            options3d: {
                enabled: false,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([2.5]);
            //                chart.series[1].setData([6.9]);
            //                chart.series[2].setData([3.5]);
            //                chart.series[3].setData([7.2]);
            //                chart.series[4].setData([9.1]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0]);
            //                chart.series[1].setData([0]);
            //                chart.series[2].setData([0]);
            //                chart.series[3].setData([0]);
            //                chart.series[4].setData([0]);
            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'Biểu đồ hàm lượng pH',
        },
        subtitle: {

            text: 'Năm 2017',
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        xAxis: {
            categories: ['pH'],
            labels: {
                autoRotation: 0,
                style: {
                    fontSize: '6px'
                }
            },
        },
        yAxis: {
            categories: [],
            labels: {
                formatter: function () {
                    return this.value;
                },
                style: {
                    fontSize: '6px'
                }
            },
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemStyle: {
                fontSize: '8px'
            }
        },
        series: [
            {
                type: 'column',
                name: 'Suối Chà Răng',
                data: [6.5]
            },
                {
                    type: 'column',
                    name: 'Hồ Châu Pha',
                    data: [6.79]
                },
                {
                    type: 'column',
                    name: 'Hồ An Hải',
                    data: [6.65]
                },
                {
                    type: 'column',
                    name: 'Hồ Đá Đen',
                    data: [6.1]
                },
                {
                    type: 'column',
                    name: 'Hồ Sông Hỏa',
                    data: [6.25]
                }
        ]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});

$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container_2',
            type: 'column',
            options3d: {
                enabled: false,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([1.5]);
            //                chart.series[1].setData([2.9]);
            //                chart.series[2].setData([3.5]);
            //                chart.series[3].setData([4.2]);
            //                chart.series[4].setData([5.1]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0]);
            //                chart.series[1].setData([0]);
            //                chart.series[2].setData([0]);
            //                chart.series[3].setData([0]);
            //                chart.series[4].setData([0]);
            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'Biểu đồ hàm lượng NO2'
        },
        subtitle: {
            text: 'Quý I/2018'
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        xAxis: {
            categories: ['pH'],
            labels: {
                autoRotation: 0,
                style: {
                    fontSize: '6px'
                }
            },
        },
        yAxis: {
            categories: [],
            labels: {
                formatter: function () {
                    return this.value;
                },
                style: {
                    fontSize: '6px'
                }
            },
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemStyle: {
                fontSize: '8px'
            }
        },
        series: [
            {
                type: 'column',
                name: 'QCVN',
                data: [200]
            },
            {
                type: 'column',
                name: 'Khu liên hợp xử lý chất thải rắn 100ha Tóc Tiên',
                data: [29.78]
            },
            {
                type: 'column',
                name: 'Ngã tư Giếng Nước.',
                data: [19.23]
            },
            {
                type: 'column',
                name: 'Vòng Xoay Đài Phun nước Bà Rịa.',
                data: [27.64]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Phú Mỹ 1.',
                data: [29.12]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Đông Xuyên',
                data: [23.91]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Long Xuyên',
                data: [26.5]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Cái Mép',
                data: [34.76]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Đô Thị Châu Đức',
                data: [29.17]
            },
            {
                type: 'column',
                name: 'Khu công nghiệp Mỹ Xuân Á',
                data: [29.38]
            }
        ]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});