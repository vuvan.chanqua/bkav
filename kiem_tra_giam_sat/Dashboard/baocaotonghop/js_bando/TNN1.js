﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'containerTNN_1',
        type: 'column',
        options3d: {
            enabled: false,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
        //events: {
        //    load: function () {
        //        var count = 0;
        //        setInterval(function () {
        //            if (count == 0) {
        //                chart.series[0].setData([319, 900, 474, 307, 371, 304, 938, 746]);
        //                count = 1;
        //            }
        //            else {
        //                chart.series[0].setData([0, 0, 0, 0, 0,0,0,0]);
        //                count = 0;
        //            }
        //        }, 2000);
        //    }
        //}
    },
    title: {
        text: 'Tiềm năng nguồn nước mặt'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: ['TP Bà Rịa', 'TP Vũng Tàu', 'Tx Phú Mỹ', 'Huyện Châu Đốc', 'Huyện Côn Đảo', 'Huyện Đất Đỏ', 'Huyện Long Biên', 'Huyện Xuyên Mộc'],
        labels: {
            autoRotation: 0,
            style: {
                fontSize: '8px'
            }
        },
    },
    yAxis: {
        max:1000,
        categories: [0, 150],
        labels: {
            formatter: function () {
                return this.value + ' %';
            },
            style: {
                fontSize: '8px'
            }
        },
        title:{
            text: "Wnm",
            style: {
                fontSize: '8px'
            }
            }
    },
    credits: {
        enabled:false
    },
    exporting: {
        enabled: false
    },
    legend: {
        enabled: false,
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y:.1f}%'
            }
        }
    },
    "series": [
    {
        "colorByPoint": false,
        "data": [319, 900, 474, 307, 371, 304, 938, 746]
        

    }
    ]
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});
