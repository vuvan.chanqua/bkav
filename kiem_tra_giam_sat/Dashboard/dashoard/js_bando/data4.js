﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container4',
        type: 'line',
        options3d: {
            enabled: false,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
        //events: {
        //    load: function () {
        //        var count = 0;
        //        setInterval(function () {
        //            if (count == 0) {
        //                chart.series[0].setData([104.09, 122.06, 85.16, 112.63, 65.88, 57.44, 75.99, 60.28]);
        //                count = 1;
        //            }
        //            else {
        //                chart.series[0].setData([0, 0, 0, 0, 0, 0, 0, 0]);

        //                count = 0;
        //            }
        //        }, 2000);
        //    }
        //}
    },
    title: {
        text: 'Nguồn thu từ đất'
    },
    subtitle: {
        text: ''
    },
    credits: {
        enabled:false
    },
    exporting: {
        enabled: false
    },
    xAxis: {
        categories: ['2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016'],
        labels: {
            autoRotation: 0,
            style: {
                fontSize: '8px'
            }
        },
    },
    yAxis: {
        categories: [],
        labels: {
            formatter: function () {
                return this.value;
            },
            style: {
                fontSize: '8px'
            }
        },
    },
    legend: {
        enabled: false,
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y:.1f}'
            }
        }
    },
    "series": [
    {
        "colorByPoint": false,
        "data": [1622,
                811,
                1064,
                1012,
                933,
                1270,
                2159,
                2325,
                ],
        color: 'orange'
    }
    ]
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});

$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container4_2',
            type: 'line',
            options3d: {
                enabled: false,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([104.09, 122.06, 85.16, 112.63, 65.88, 57.44, 75.99, 60.28]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0, 0, 0, 0, 0]);

            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'Nguồn thu từ đất'
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        xAxis: {
            categories: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8'],
            labels: {
                autoRotation: 0,
                style: {
                    fontSize: '8px'
                }
            },
        },
        yAxis: {
            categories: [],
            labels: {
                formatter: function () {
                    return this.value;
                },
                style: {
                    fontSize: '8px'
                }
            },
        },
        legend: {
            enabled: false,
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y:.1f}'
                }
            }
        },
        "series": [
        {
            "colorByPoint": false,
            "data": [622,
                    511,
                    464,
                    712,
                    633,
                    570,
                    159,
                    425,
            ],
            color: 'orange'
        }
        ]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});