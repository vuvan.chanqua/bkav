﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container5',
        type: 'column',
        options3d: {
            enabled: false,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
        //events: {
        //    load: function () {
        //        var count = 0;
        //        setInterval(function () {
        //            if (count == 0) {
        //                chart.series[0].setData([94.63, 91.44, 99.26, 99.49, 93.25]);
        //                count = 1;
        //            }
        //            else {
        //                chart.series[0].setData([0, 0, 0, 0, 0]);
        //                count = 0;
        //            }
        //        }, 2000);
        //    }
        //}
    },
    title: {
        text: 'Kết quả cấp GCNQSDĐ'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: ['Đất nông nghiệp', 'Đất lâm nghiệp', 'Đất nuôi trồng thủy sản', 'Đất ở tại nông thôn', 'Đất ở tại đô thị'],
        labels: {
            autoRotation: 0,
            style: {
                fontSize: '8px'
            }
        },
    },
    yAxis: {
        labels: {
            formatter: function () {
                return this.value + ' %';
            },
            style: {
                fontSize: '8px'
            }
        },
        title:{
            text: "Hoàn thành",
            style: {
                fontSize: '8px'
            }
            }
    },
    credits: {
        enabled:false
    },
    exporting: {
        enabled: false
    },
    legend: {
        enabled: false,
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y:.1f}%'
            }
        }
    },
    "series": [
    {
        "colorByPoint": false,
        "data": [94.63, 91.44, 99.26, 99.49, 93.25]
    }
    ]
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});

$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container5_2',
            type: 'column',
            options3d: {
                enabled: false,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([74.63, 81.44, 59.26, 69.49, 93.25]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0, 0]);
            //                count = 0;
            //            }
            //        }, 2000);
            //    }
            //}
        },
        title: {
            text: 'Kết quả cấp GCNQSDĐ'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Đất nông nghiệp', 'Đất lâm nghiệp', 'Đất nuôi trồng thủy sản', 'Đất ở tại nông thôn', 'Đất ở tại đô thị'],
            labels: {
                autoRotation: 0,
                style: {
                    fontSize: '8px'
                }
            },
        },
        yAxis: {
            categories: [0, 150],
            labels: {
                formatter: function () {
                    return this.value + ' %';
                },
                style: {
                    fontSize: '8px'
                }
            },
            title: {
                text: "Hoàn thành",
                style: {
                    fontSize: '8px'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        legend: {
            enabled: false,
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: false,
                    format: '{point.y:.1f}%'
                }
            }
        },
        "series": [
        {
            "colorByPoint": false,
            "data": [74.63, 81.44, 59.26, 69.49, 93.25]
        }
        ]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});