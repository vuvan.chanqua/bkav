﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container6',
        type: 'column',
        options3d: {
            enabled: false,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Kết quả thu phí (Triệu đồng)'
    },
    credits: {
        enabled:false
    },
    exporting: {
        enabled: false
    },
    xAxis: {
        categories: ['Tiền sử dụng đất', 'Tiền thuê đất', 'Lệ phí trước bạ', 'Thuế sử dụng đất', 'Thuế thu nhập cá nhân'],
        labels: {
            style: {
                fontSize: '6px'
            }
        },
        crosshair: true
    },
    yAxis: {
        max:350000,
        categories: [],
        labels: {
            formatter: function () {
                return this.value;
            },
            style: {
                fontSize: '6px'
            }
        },
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        itemStyle: {
            fontSize: '8px'
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: false,
                format: '{point.y:.1f}%'
            }
        }
    },
    "series": [
          {
              type: 'column',
              name: 'Tháng 4',
              data: [25000, 24500, 5000, 6000, 7000]
          },
          {
              type: 'column',
              name: 'Tháng 5',
              data: [40000, 45000, 6000, 7000, 1000]
          },
          {
              type: 'column',
              name: 'Năm 2017',
              data: [300000, 250000, 30000, 45000, 30000]
          }
        ]

});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});