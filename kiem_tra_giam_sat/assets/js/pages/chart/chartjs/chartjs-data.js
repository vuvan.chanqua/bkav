
$(document).ready(function() {
	  var config = {
        type: 'line',
        data: {
            labels: ["Châu Á", "Châu Âu", "Châu Mỹ", "Châu Phi","Châu ĐD", "TT chưa PB"],
            datasets: [{
                label: "Xuất khẩu",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: [
                    10122,
                    3963,
                    4822,
                    208,
                    370,
                    359
                ],
                fill: false,
            }, {
                label: "Nhập khẩu",
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [
                    15241,
                    1298,
                    1531,
                    174,
                    328,
                    474
                ],
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Kim ngạch XNK hàng hóa theo thị trường'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Thị trường'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Giá trị'
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById("chartjs_line").getContext("2d");
    window.myLine = new Chart(ctx, config);
	});
$(document).ready(function () {
    var config = {
        type: 'bar',
        data: {
            labels: ["Toàn ngành","Khai khoáng", "CN CBCT","SX và PP điện,..","CC nước, QL thải"],
            datasets: [{
                label: "So với tháng 6",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: [102.5 ,
                    96.5 ,
                     103.5 ,
                     101.9 ,
                    100.9
                ],
                fill: false,
            }, {
                    label: "So với cùng kỳ",
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [
                    114.3 ,
                     102.8 ,
                     116.6 ,
                     112.2 ,
                     107.6 
                ],
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Lĩnh vực'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: '%'
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById("chartjs_lineCN").getContext("2d");
    window.myLine = new Chart(ctx, config);
});        
      
$(document).ready(function() {
	var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'pie',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.green,
                window.chartColors.blue,
            ],
            label: 'Dataset 1'
        }],
        labels: [
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue"
        ]
    },
    options: {
        responsive: true
    }
};

    var ctx = document.getElementById("chartjs_pie").getContext("2d");
    window.myPie = new Chart(ctx, config);
}); 

$(document).ready(function () {
  
    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                    40.71,
                    20.32,
                    0.30,
                    38.66

                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.yellow,
                    window.chartColors.blue,
                    window.chartColors.green,
                  
                ],
                label: "Điện"
            }],
            labels: [
                "Điện SX",
                "Các đơn vị khác",
                "Điện NK từ Trung Quốc",
                "Điện thương phẩm"                
            ]
        },
        options: {
            responsive: true
        }
    };

    var ctx = document.getElementById("chartjs_pie1").getContext("2d");
    window.myPie = new Chart(ctx, config);
}); 
$(document).ready(function() {
	   var color = Chart.helpers.color;
     var barChartData = {
         labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7"],
         datasets: [{
             label: 'Năm trước',
             backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
             borderColor: window.chartColors.red,
             borderWidth: 1,
             data: [
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor()
             ]
         }, {
             label: 'Năm nay',
             backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
             borderColor: window.chartColors.blue,
             borderWidth: 1,
             data: [
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor(),
                 randomScalingFactor()
             ]
         }]

     };

         var ctx = document.getElementById("chartjs_bar").getContext("2d");
         window.myBar = new Chart(ctx, {
             type: 'bar',
             data: barChartData,
             options: {
                 responsive: true,
                 legend: {
                     position: 'top',
                 },
                 title: {
                     display: true,
                     text: 'Biểu đồ cột'
                 }
             }
         });

	});

$(document).ready(function() {
	
    var chartColors = window.chartColors;
    var color = Chart.helpers.color;
    var config = {
        data: {
            datasets: [{
                data: [
                    74,
                    13,
                    1,
                    12
                ],
                backgroundColor: [
                    color(chartColors.red).alpha(0.5).rgbString(),
                    color(chartColors.orange).alpha(0.5).rgbString(),
                    color(chartColors.blue).alpha(0.5).rgbString(),
                    color(chartColors.green).alpha(0.5).rgbString()                 
                ],
                label: "Hàng hóa, dịch vụ"// for legend
            }],
            labels: [
                "Bán lẻ HH",
                "Lưu trú, ăn uống",
                "Du lịch",
                "Dịch vụ"                
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Tỷ lệ mức bán lẻ hàng hóa, dịch vụ tháng 6 năm 2018'
            },
            scale: {
              ticks: {
                beginAtZero: true
              },
              reverse: false
            },
            animation: {
                animateRotate: false,
                animateScale: true
            }
        }
    };

        var ctx = document.getElementById("chartjs_polar");
        window.myPolarArea = Chart.PolarArea(ctx, config);

	});

$(document).ready(function() {
	
    var config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    74,
                    13,
                    1,
                    12
                ],
                backgroundColor: [
                    window.chartColors.red,                   
                    window.chartColors.yellow,
                    window.chartColors.green,
                    window.chartColors.blue,
                ],
                label: 'Hàng hóa, dịch vụ'
            }],
            labels: [
                "Bán lẻ HH",
                "Lưu trú, ăn uống",
                "Du lịch",
                "Dịch vụ"    
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Tỷ lệ mức bán lẻ hàng hóa, dịch vụ tháng 6 năm 2018'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

        var ctx = document.getElementById("chartjs_doughnut").getContext("2d");
        window.myDoughnut = new Chart(ctx, config);
    
	});

$(document).ready(function() {
	var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var color = Chart.helpers.color;
    var config = {
        type: 'radar',
        data: {
            labels: [["Eating", "Dinner"], ["Drinking", "Water"], "Sleeping", ["Designing", "Graphics"], "Coding", "Cycling", "Running"],
            datasets: [{
                label: "New Students",
                backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
                borderColor: window.chartColors.red,
                pointBackgroundColor: window.chartColors.red,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            }, {
                label: "Old Students",
                backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                borderColor: window.chartColors.blue,
                pointBackgroundColor: window.chartColors.blue,
                data: [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            },]
        },
        options: {
            legend: {
                position: 'Trên',
            },
            title: {
                display: true,
                text: 'Biểu đồ Radar '
            },
            scale: {
              ticks: {
                beginAtZero: true
              }
            }
        }
    };

        window.myRadar = new Chart(document.getElementById("radar_chart"), config);

   
	});

$(document).ready(function () {
    var MONTHS = ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7"];

    var color = Chart.helpers.color;
    var config = {
        type: 'radar',
        data: {
            labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7"],
            datasets: [{
                label: "Toàn ngành",
                backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
                borderColor: window.chartColors.red,
                pointBackgroundColor: window.chartColors.red,
                data: [126.8,
                    101.2,
                    126.1,
                    124.4,
                    127.9,
                    127.3,
                    130.5                    
                ]
            }, {
                    label: "Khai khoáng",
                backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
                borderColor: window.chartColors.blue,
                pointBackgroundColor: window.chartColors.blue,
                data: [
                    100.7,
                    81.5,
                    99.4,
                    95.8,
                    97.8,
                    92.9,
                    89.6 
                ]
            }, {
                    label: "Công nghiệp CB, CT",
                    backgroundColor: color(window.chartColors.yellow).alpha(0.2).rgbString(),
                    borderColor: window.chartColors.yellow,
                    pointBackgroundColor: window.chartColors.yellow,
                    data: [
                        131.3,
                        103.6,
                        131.9,
                        128.9,
                        132.1,
                        130.9,
                        135.6 
                    ]
            }, {
                    label: "SX và PP điện,..",
                backgroundColor: color(window.chartColors.green).alpha(0.2).rgbString(),
                borderColor: window.chartColors.green,
                pointBackgroundColor: window.chartColors.green,
                data: [
                    137.4,
                    115.3,
                    129.2,
                    140.7,
                    148.6,
                    160.5,
                    163.5 
                ]
            }, {
                    label: "CC nước,QL và XL rác thải, nước thải",
                backgroundColor: color(window.chartColors.orange).alpha(0.2).rgbString(),
                borderColor: window.chartColors.orange,
                pointBackgroundColor: window.chartColors.orange,
                data: [
                    110.9,
                    104.0,
                    104.8,
                    107.2,
                    111.6,
                    112.7,
                    113.6 
                ]
          
            },]
        },
        options: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Chỉ số SX công nghiệp tháng so với tháng bình quân năm 2015 (đơn vị: %)'
            },
            scale: {
                ticks: {
                    beginAtZero: true
                }
            }
        }
    };

    window.myRadar = new Chart(document.getElementById("radar_chart1"), config);


});

