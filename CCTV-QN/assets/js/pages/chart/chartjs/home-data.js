
$(document).ready(function() {
	var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };

    var config = {
        type: 'pie',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.green,
                window.chartColors.blue,
            ],
            label: 'Dataset 1'
        }],
        labels: [
            "Điện sản xuất",
            "Than sạch",
            "Dầu thô",
            "Thép các loại",
            "Phân bón"
        ]
    },
    options: {
        responsive: true
    }
};

    var ctx = document.getElementById("chartjs_pie").getContext("2d");
    window.myPie = new Chart(ctx, config);
});

$(document).ready(function() 
{
	var color = Chart.helpers.color;
    var barChartData = {
        labels: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7"],
        datasets: [{
            type: 'bar',
            label: 'Kế hoạch',
            backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
            borderColor: window.chartColors.red,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ]
        }, {
            type: 'line',
            label: 'Thực hiện',
            backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
            borderColor: window.chartColors.blue,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ]
        }, {
            type: 'bar',
            label: 'Cùng kỳ',
            backgroundColor: color(window.chartColors.green).alpha(0.2).rgbString(),
            borderColor: window.chartColors.green,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ]
        }]
    };

        var ctx = document.getElementById("canvas1").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: ''
                },
            }
        });


});

$(document).ready(function () {
    var color = Chart.helpers.color;
    var barChartData = {
        labels: ["NLTS", "NL và KS", "CN CB", "HH khác"],
        datasets: [{
            type: 'bar',
            label: 'Kế hoạch',
            backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
            borderColor: window.chartColors.red,
            data: [
                2410,
                326,
                15970,
                893
            ]
        }, {
            type: 'line',
            label: 'Thực hiện',
            backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
            borderColor: window.chartColors.blue,
            data: [
                2303,
                375,
                16273,
                894
            ]
        }, {
            type: 'bar',
            label: 'Cùng kỳ',
            backgroundColor: color(window.chartColors.green).alpha(0.2).rgbString(),
            borderColor: window.chartColors.green,
            data: [
                2272,
                500,
                14148,
                911

            ]
        }]
    };

    var ctx = document.getElementById("canvasXMH").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'XUẤT KHẨU HÀNG HÓA THEO MẶT HÀNG THÁNG 6 NĂM 2018'
            },
        }
    });


});

$(document).ready(function () {
    var color = Chart.helpers.color;
    var barChartData = {
        labels: ["NH cần", "NH cần KS",  "HH khác"],
        datasets: [{
            type: 'bar',
            label: 'Kế hoạch',
            backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
            borderColor: window.chartColors.red,
            data: [
                17437,
                1264,
                999
            ]
        }, {
            type: 'line',
            label: 'Thực hiện',
            backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
            borderColor: window.chartColors.blue,
            data: [
                16996,
                1195,            
                894
            ]
        }, {
            type: 'bar',
            label: 'Cùng kỳ',
            backgroundColor: color(window.chartColors.green).alpha(0.2).rgbString(),
            borderColor: window.chartColors.green,
            data: [
                16254,
                1067,
                841,
               
            ]
        }]
    };

    var ctx = document.getElementById("canvasNMH").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'NHẬP KHẨU HÀNG HÓA THEO MẶT HÀNG THÁNG 6 NĂM 2018'
            },
        }
    });


});