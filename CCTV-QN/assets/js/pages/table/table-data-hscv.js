﻿/**
 *  Document   : table_data.js
 *  Author     : redstar
 *  Description: advance table page script
 *
 **/

$(document).ready(function() {
	'use strict';
    $('#example1').DataTable();
    
    var table = $('#example2').DataTable( {
        "scrollY": "200px",
        "paging": false
    } );
 
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );
    
    var t = $('#example3').DataTable();
    var counter = 1;
 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );
 
        counter++;
    } );
 
    // Automatically add a first row of data
    $('#addRow').click();

    var dataSetvb1 = [
        ["1", "<b>Thông tư hướng dẫn</b>", "<b>21-08-2018</b>", "<a class='viewvbphapluat'><b>Thông tư hướng dẫn về đấu giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Quy định văn bản</b>", "<b>19-07-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xây dựng và ban hành văn bản quy phạm pháp luật của Bộ Công Thương</b></a>", "<b>Quy định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Thông tư", "29-06-2018", "Thông tư quy định về việc phân luồng trong quy trình cấp Giấy chứng nhận xuất xứ hàng hóa ưu đãi", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Tài liệu quy định", "15-06-2018", "Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["5", "Tài liệu hướng dẫnn<", "20-04-2018", "Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["6", "Tài liệu xuất xử", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["7", "Công văn gửi", "22-08-2018", "Công văn về việc điều hành kinh doanh xăng dầu", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["8", "Dự thảo chỉ thị", "19-07-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", " Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["9", "Dự thảo quyết định", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["10", "Dự thảo thông tư", "04-07-2018", "Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["11", "Dự thảo quy định", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["12", "Dự thảo nghị định", "22-02-2018", " Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["13", "Thông tư hướng dẫn", "22-02-2018", "Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["14", "Nghị định trình ký", "12-02-2018", "Nghị định về thương mại điện tử", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["15", "Nghị định gửi", "09-02-2018", "Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb2 = [
        ["1", "<b>Thông tư</b>", "<b>29-06-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định về việc phân luồng trong quy trình cấp Giấy chứng nhận xuất xứ hàng hóa ưu đãi</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Tài liệu hướng dẫn</b>", "<b>20-04-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Công văn gửi", "22-08-2018", "Công văn về việc điều hành kinh doanh xăng dầu", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Dự thảo chỉ thị", "19-07-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", " Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["5", "Dự thảo thông tư", "04-07-2018", "Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["6", "Dự thảo quy định", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb3 = [
        ["1", "<b>Tài liệu xuất xử</b>", "<b> 03-04-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xuất xứ hàng hóa</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Công văn gửi</b>", "<b>22-08-2018</b>", "<a class='viewvbphapluat'><b>Công văn về việc điều hành kinh doanh xăng dầu</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Dự thảo chỉ thị", "19-07-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", " Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Dự thảo quy định", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb4 = [
       ["1", "<b>Nghị định gửi</b>", "<b> 09-02-2018</b>", "<a class='viewvbphapluat'><b>Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Tài liệu hướng dẫn</b>", "<b>20-04-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Tài liệu xuất xử", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Dự thảo chỉ thị", "19-07-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", " Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["5", "Dự thảo quyết định", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["6", "Dự thảo thông tư", "04-07-2018", "Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["7", "Dự thảo nghị định", "22-02-2018", " Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["8", "Thông tư hướng dẫn", "22-02-2018", "Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["9", "Nghị định trình ký", "12-02-2018", "Nghị định về thương mại điện tử", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb5 = [
        ["1", "<b> Dự thảo thông tư</b>", "<b>04-07-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Dự thảo quyết định</b>", "<b>18-07-2018</b>", "<a class='viewvbphapluat'><b>Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018</b></a>", "<b>Quyết định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],        ["3", "Nghị định trình ký", "12-02-2018", "Nghị định về thương mại điện tử", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Dự thảo nghị định", "22-02-2018", " Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["5", "Tài liệu quy định", "15-06-2018", "Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["6", "Tài liệu hướng dẫnn", "20-04-2018", "Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["7", "Tài liệu xuất xử", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["7", "Công văn gửi", "22-08-2018", "Công văn về việc điều hành kinh doanh xăng dầu", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["8", "Dự thảo quy định", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["10", "Thông tư hướng dẫn", "22-02-2018", "Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["11", "Nghị định gửi", "09-02-2018", "Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb6 = [
        ["1", "<b>Thông tư hướng dẫn</b>", "<b>22-02-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Nghị định trình ký</b>", "<b>21-02-2018</b>", "<a class='viewvbphapluat'><b>Nghị định về thương mại điện tử</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Nghị định gửi", "09-02-2018", "Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];

    var dataSetvb7 = [
        ["1", "<b>Dự thảo nghị định</b>", "<b>22-02-2018</b>", "<a class='viewvbphapluat'><b>Nghị định về vật liệu nổ công nghiệp</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>Công văn gửi</b>", "<b>22-08-2018</b>", "<a class='viewvbphapluat'><b>Công văn về việc điều hành kinh doanh xăng dầu</b></a>", "<b>Quy định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "Nghị định trình ký", "12-02-2018", "Nghị định về thương mại điện tử", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["4", "Tài liệu quy định", "15-06-2018", "Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["5", "Tài liệu xuất xử", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["6", "Dự thảo quyết định", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
        ["7", "Dự thảo quy định", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a> "],
    ];


    $(document).on('click', '.viewvbphapluat', function () {
        $('#ModalLuat').modal('show');
    });

    $(document).on('click', '[data-nodeid="0"]', function () {
alert('ahihi');
        table3.clear().rows.add(dataSetvb1).draw();
    })
    $(document).on('click', '[data-nodeid="1"]', function () {
        table3.clear().rows.add(dataSetvb2).draw();
    })
    $(document).on('click', '[data-nodeid="4"]', function () {
        table3.clear().rows.add(dataSetvb3).draw();
    })
    $(document).on('click', '[data-nodeid="5"]', function () {
        table3.clear().rows.add(dataSetvb4).draw();
    })
    $(document).on('click', '[data-nodeid="6"]', function () {
        table3.clear().rows.add(dataSetvb5).draw();
    })
    $(document).on('click', '[data-nodeid="7"]', function () {
        table3.clear().rows.add(dataSetvb6).draw();
    })
    $(document).on('click', '[data-nodeid="8"]', function () {
        table3.clear().rows.add(dataSetvb7).draw();
    })

    var table3 = $('#example6').DataTable({
        data: dataSetvb1,
        columns: [
            { title: "TT" },
            { title: "Tên tài liệu" },
            { title: "Ngày" },
            { title: "Mô tả" },
            { title: "Loại" },
            { title: "Tải" }
        ]
    });

$('#taohscv').click(function(){
    $('#ModalTaoHSCV').modal('show');
})

    function getImg(data, type, full, meta) {
        //
        return '<img  src="" />';
    }
    $('#saveStage').DataTable( {
        stateSave: true
    } );
    
    var table = $('#tableGroup').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"<td colspan="5"'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#tableGroup tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
    
    
    var dataSet = [
                   [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ],
                   [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750" ],
                   [ "Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000" ],
                   [ "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060" ],
                   [ "Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700" ],
                   [ "Brielle Williamson", "Integration Specialist", "New York", "4804", "2012/12/02", "$372,000" ],
                   [ "Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012/08/06", "$137,500" ],
                   [ "Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010/10/14", "$327,900" ],
                   [ "Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009/09/15", "$205,500" ],
                   [ "Sonya Frost", "Software Engineer", "Edinburgh", "1667", "2008/12/13", "$103,600" ],
                   [ "Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560" ],
                   [ "Quinn Flynn", "Support Lead", "Edinburgh", "9497", "2013/03/03", "$342,000" ],
                   [ "Charde Marshall", "Regional Director", "San Francisco", "6741", "2008/10/16", "$470,600" ],
                   [ "Haley Kennedy", "Senior Marketing Designer", "London", "3597", "2012/12/18", "$313,500" ],
                   [ "Tatyana Fitzpatrick", "Regional Director", "London", "1965", "2010/03/17", "$385,750" ],
                   [ "Michael Silva", "Marketing Designer", "London", "1581", "2012/11/27", "$198,500" ],
                   [ "Paul Byrd", "Chief Financial Officer (CFO)", "New York", "3059", "2010/06/09", "$725,000" ],
                   [ "Gloria Little", "Systems Administrator", "New York", "1721", "2009/04/10", "$237,500" ],
                   [ "Bradley Greer", "Software Engineer", "London", "2558", "2012/10/13", "$132,000" ],
                   [ "Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500" ],
                   [ "Jenette Caldwell", "Development Lead", "New York", "1937", "2011/09/03", "$345,000" ],
                   [ "Yuri Berry", "Chief Marketing Officer (CMO)", "New York", "6154", "2009/06/25", "$675,000" ],
                   [ "Caesar Vance", "Pre-Sales Support", "New York", "8330", "2011/12/12", "$106,450" ],
                   [ "Doris Wilder", "Sales Assistant", "Sidney", "3023", "2010/09/20", "$85,600" ],
                   [ "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "5797", "2009/10/09", "$1,200,000" ],
                   [ "Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
                   [ "Jennifer Chang", "Regional Director", "Singapore", "9239", "2010/11/14", "$357,650" ],
                   [ "Brenden Wagner", "Software Engineer", "San Francisco", "1314", "2011/06/07", "$206,850" ],
                   [ "Fiona Green", "Chief Operating Officer (COO)", "San Francisco", "2947", "2010/03/11", "$850,000" ],
                   [ "Shou Itou", "Regional Marketing", "Tokyo", "8899", "2011/08/14", "$163,000" ],
                   [ "Michelle House", "Integration Specialist", "Sidney", "2769", "2011/06/02", "$95,400" ],
                   [ "Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500" ],
                   [ "Prescott Bartlett", "Technical Author", "London", "3606", "2011/05/07", "$145,000" ],
                   [ "Gavin Cortez", "Team Leader", "San Francisco", "2860", "2008/10/26", "$235,500" ],
                   [ "Martena Mccray", "Post-Sales support", "Edinburgh", "8240", "2011/03/09", "$324,050" ],
                   [ "Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009/12/09", "$85,675" ]
               ];
                
		    $('#dataTable').DataTable( {
		        data: dataSet,
		        columns: [
		            { title: "Name" },
		            { title: "Position" },
		            { title: "Office" },
		            { title: "Extn." },
		            { title: "Start date" },
		            { title: "Salary" }
		        ]
		    } );
		    
} );