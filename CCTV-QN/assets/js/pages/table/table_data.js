﻿/**
 *  Document   : table_data.js
 *  Author     : redstar
 *  Description: advance table page script
 *
 **/

$(document).ready(function() {
	'use strict';
    $('#example1').DataTable();
    
    var table = $('#example2').DataTable( {
        "scrollY": "200px",
        "paging": false
    } );
 
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
 
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );
    
    var t = $('#example3').DataTable();
    var counter = 1;
 
    $('#addRow').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );
 
        counter++;
    } );
 
    // Automatically add a first row of data
    $('#addRow').click();
    var dataSet1 = [
        ["1", "../assets/img/dp.jpg", "Dương Duy Hưng", "Vụ kế hoạch", "Vụ trưởng", "hungdd@moit.gov.vn", "024. 2220 2333"],
        ["2", "../assets/img/dp.jpg", "Nguyễn Hoàng Giang", "Vụ kế hoạch", "Phó vụ trưởng", "giangngh@moit.gov.vn", "024. 2220 2317"],
        ["3", "../assets/img/dp.jpg", "Nguyễn Sỹ Cường", "Vụ kế hoạch", "Phó vụ trưởng", "CuongNS@moit.gov.vn", "024. 2220 2319"],
        ["4", "../assets/img/dp.jpg", "Nguyễn Thuý Hiền", "Vụ kế hoạch", "Phó vụ trưởng", "hungdd@moit.gov.vn", "(04) 22 202 303"],
        ["5", "../assets/img/dp.jpg", "Vũ Quốc Anh", "Vụ tài chính và đổi mới doanh nghiệp", "Vụ trưởng", "AnhVQ@moit.gov.vn", "024 2220 5467"],
        ["6", "../assets/img/dp.jpg", "Nguyễn Thành Lam", "Vụ tài chính và đổi mới doanh nghiệp", "Phó Vụ trưởng", "lamnt@moit.gov.vn", "024 2220 5472"],
        ["7", "../assets/img/dp.jpg", "Phạm Đào Chương", "Vụ tài chính và đổi mới doanh nghiệp", "Phó Vụ trưởng", "ChuongPD@moit.gov.vn", "024 2220 5595"],
        ["8", "../assets/img/dp.jpg", "Tào Thị Kim Vân", "Vụ tài chính và đổi mới doanh nghiệp", "Phó Vụ trưởng", "vanttk@moit.gov.vn", "(04) 2220 5469"],
        ["9", "../assets/img/dp.jpg", "Nguyễn Thị Hoa", "Vụ tài chính và đổi mới doanh nghiệp", "Phó Vụ trưởng ", "HoaNgt@moit.gov.vn", "024 2220 5473"],
        ["10", "../assets/img/dp.jpg", "Trần Việt Hòa", "Vụ khoa học và công nghệ", "Vụ trưởng ", "HoaTV@moit.gov.vn", "024 2220 2436"],
        ["11", "../assets/img/dp.jpg", "Phạm Thu Giang", "Vụ khoa học và công nghệ", "Phó vụ trưởng ", "GiangPT@moit.gov.vn", "024 2220 2314"],
        ["12", "../assets/img/dp.jpg", "Nguyễn Huy Hoàn", "Vụ khoa học và công nghệ", "Phó Vụ trưởng", "HoanNH@moit.gov.vn", "024 2220 2316"],
        ["13", "../assets/img/dp.jpg", "Trần Minh", "Vụ khoa học và công nghệ", "Phó Vụ trưởng", "MinhT@moit.gov.vn", "024 2220 2416"],
        ["14", "../assets/img/dp.jpg", "Lê Hoàng Oanh", "Vụ thị trường châu Á - châu Phi", "Vụ trưởng ", "OanhLH@moit.gov.vn", "04. 2220 5426"],
        ["15", "../assets/img/dp.jpg", "Ngô Khải Hoàn", "Vụ thị trường châu Á - châu Phi", "Phó Vụ trưởng ", "HoanNK@moit.gov.vn", "(04) 22205479"]
    ];
    var dataSet2 = [
        ["1", "../assets/img/dp.jpg", "Dương Duy Hưng", "Vụ kế hoạch", "Vụ trưởng", "hungdd@moit.gov.vn", "024. 2220 2333"],
        ["2", "../assets/img/dp.jpg", "Nguyễn Hoàng Giang", "Vụ kế hoạch", "Phó vụ trưởng", "giangngh@moit.gov.vn", "024. 2220 2317"],
        ["3", "../assets/img/dp.jpg", "Nguyễn Sỹ Cường", "Vụ kế hoạch", "Phó vụ trưởng", "CuongNS@moit.gov.vn", "024. 2220 2319"],
        ["4", "../assets/img/dp.jpg", "Nguyễn Thuý Hiền", "Vụ kế hoạch", "Phó vụ trưởng", "hungdd@moit.gov.vn", "(04) 22 202 303"],
        ["5", "../assets/img/dp.jpg", "Vũ Quốc Anh", "Vụ tài chính và đổi mới doanh nghiệp", "Vụ trưởng", "AnhVQ@moit.gov.vn", "024 2220 5467"],
        ["6", "../assets/img/dp.jpg", "Nguyễn Thành Lam", "Vụ tài chính và đổi mới doanh nghiệp", "Phó Vụ trưởng", "lamnt@moit.gov.vn", "024 2220 5472"]
    ];
    var dataSet3 = [];
    var dataSet4 = [
        ["1", "../assets/img/dp.jpg", "Trịnh Văn Ngọc", "Cục quản lý thị trường", "Cục trưởng", "NgocTV@moit.gov.vn", "024) 38255868"],
        ["2", "../assets/img/dp.jpg", "Nguyễn Trọng Tín", "Cục quản lý thị trường", "Phó Cục trưởng", "TinNT@moit.gov.vn", "024) 39387161"],
        ["3", "../assets/img/dp.jpg", "Trần Hùng", "Cục quản lý thị trường", "Phó Cục trưởng", "HungT@moit.gov.vn", "(024) 38255868"],
        ["4", "../assets/img/dp.jpg", "Chu Thị Thu Hương", "Cục quản lý thị trường", "Phó Cục trưởng", "Huongctt@moit.gov.vn", "(024) 38255868"],
        ["5", "../assets/img/dp.jpg", "Nguyễn Thanh Bình", "Cục quản lý thị trường", "Phó Cục trưởng", "BinhNGTH@moit.gov.vn", "(024) 38255868"],
        ["6", "../assets/img/dp.jpg", "Vũ Hùng Sơn", "Cục quản lý thị trường", "Phó Cục trưởng", "SonVHung@moit.gov.vn", ""],
        ["7", "../assets/img/dp.jpg", "Nguyễn Vân Nga", "Cục công tác phía Nam", "Phó Cục trưởng", "nganv@moit.gov.vn", "(028) 3829.9220"],
        ["8", "../assets/img/dp.jpg", "Phan Thế Anh", "Cục công tác phía Nam", "Phó Cục trưởng", "vanttk@moit.gov.vn", ""],
        ["9", "../assets/img/dp.jpg", "Nguyễn Quang Huy", "Cục công tác phía Nam", "Phó Cục trưởng ", "huyngq@moit.gov.vn", "(028) 3829.9220"],
        ["10", "../assets/img/dp.jpg", "Nguyễn Anh Tuấn", "Cục điều tiết điện lực", "Cục trưởng", "TuanNguyenAnh@moit.gov.vn", "024. 22468062"],
        ["11", "../assets/img/dp.jpg", "Đinh Thế Phúc", "Cục điều tiết điện lực", "Phó Cục trưởng", "PhucDT@moit.gov.vn", "024. 3543195"],
        ["12", "../assets/img/dp.jpg", "Nguyễn Quang Huy", "Cục điều tiết điện lực", "Phó Cục trưởng ", "HuyPQ@moit.gov.vn", "024. 22147470"],
        ["13", "../assets/img/dp.jpg", "Trần Tuệ Quang", "Cục điều tiết điện lực", "Phó Cục trưởng ", "QuangTT@moit.gov.vn", "024. 62918824"],
        ["14", "../assets/img/dp.jpg", "Trương Thanh Hoài", "Cục công nghiệp", "Cục trưởng", "HoaiTT@moit.gov.vn", "(024) 3.974.8989"],
        ["15", "../assets/img/dp.jpg", "Trần Quang Hà", "Cục công nghiệp", "Phó Cục trưởng", "HaTQ@moit.gov.vn", "024. 3.823.8372"],
        ["16", "../assets/img/dp.jpg", "Nguyễn Ngọc Thành", "Cục công nghiệp", "Phó Cục trưởng ", "ThanhNN@moit.gov.vn", "(024) 3.823.8375"],
        ["17", "../assets/img/dp.jpg", "Phạm Tuấn Anh", "Cục công nghiệp", "Phó Cục trưởng ", "AnhPT@moit.gov.vn", "024. 3.823.8374"]
    ]
    var dataSet5 = [
        ["1", "../assets/img/dp.jpg", "Trịnh Văn Ngọc", "Cục quản lý thị trường", "Cục trưởng", "NgocTV@moit.gov.vn", "024) 38255868"],
        ["2", "../assets/img/dp.jpg", "Nguyễn Trọng Tín", "Cục quản lý thị trường", "Phó Cục trưởng", "TinNT@moit.gov.vn", "024) 39387161"],
        ["3", "../assets/img/dp.jpg", "Trần Hùng", "Cục quản lý thị trường", "Phó Cục trưởng", "HungT@moit.gov.vn", "(024) 38255868"],
        ["4", "../assets/img/dp.jpg", "Chu Thị Thu Hương", "Cục quản lý thị trường", "Phó Cục trưởng", "Huongctt@moit.gov.vn", "(024) 38255868"],
        ["5", "../assets/img/dp.jpg", "Nguyễn Thanh Bình", "Cục quản lý thị trường", "Phó Cục trưởng", "BinhNGTH@moit.gov.vn", "(024) 38255868"],
        ["6", "../assets/img/dp.jpg", "Vũ Hùng Sơn", "Cục quản lý thị trường", "Phó Cục trưởng", "SonVHung@moit.gov.vn", ""]
    ];
    var dataSet6 = [
        ["1", "../assets/img/imgns/dp.jpg", "Nguyễn Vân Nga", "Cục công tác phía Nam", "Phó Cục trưởng", "nganv@moit.gov.vn", "(028) 3829.9220"],
        ["2", "../assets/img/imgns/dp.jpg", "Phan Thế Anh", "Cục công tác phía Nam", "Phó Cục trưởng", "vanttk@moit.gov.vn", ""],
        ["3", "../assets/img/imgns/dp.jpg", "Nguyễn Quang Huy", "Cục công tác phía Nam", "Phó Cục trưởng ", "huyngq@moit.gov.vn", "(028) 3829.9220"]
    ];
    var dataSet7 = [
        ["1", "../assets/img/dp.jpg", "Nguyễn Anh Tuấn", "Cục điều tiết điện lực", "Cục trưởng", "TuanNguyenAnh@moit.gov.vn", "024. 22468062"],
        ["2", "../assets/img/dp.jpg", "Đinh Thế Phúc", "Cục điều tiết điện lực", "Phó Cục trưởng", "PhucDT@moit.gov.vn", "024. 3543195"],
        ["3", "../assets/img/dp.jpg", "Nguyễn Quang Huy", "Cục điều tiết điện lực", "Phó Cục trưởng ", "HuyPQ@moit.gov.vn", "024. 22147470"],
        ["4", "../assets/img/dp.jpg", "Trần Tuệ Quang", "Cục điều tiết điện lực", "Phó Cục trưởng ", "QuangTT@moit.gov.vn", "024. 62918824"]
    ]
    var dataSet8 = [
        ["1", "../assets/img/dp.jpg", "Trương Thanh Hoài", "Cục công nghiệp", "Cục trưởng", "HoaiTT@moit.gov.vn", "(024) 3.974.8989"],
        ["2", "../assets/img/dp.jpg", "Trần Quang Hà", "Cục công nghiệp", "Phó Cục trưởng", "HaTQ@moit.gov.vn", "024. 3.823.8372"],
        ["3", "../assets/img/dp.jpg", "Nguyễn Ngọc Thành", "Cục công nghiệp", "Phó Cục trưởng ", "ThanhNN@moit.gov.vn", "(024) 3.823.8375"],
        ["4", "../assets/img/dp.jpg", "Phạm Tuấn Anh", "Cục công nghiệp", "Phó Cục trưởng ", "AnhPT@moit.gov.vn", "024. 3.823.8374"]
    ]
    var dataSet9 = [
        ["1", "../assets/img/dp.jpg", "Vũ Việt Nga", "Thương Vụ Đại sứ quán Việt Nam tại Phi-líp-pin ", "Tham tán Thương mại", " ngavuv@moit.gov.vn", "(+63 2) 404 3659"],
        ["2", "../assets/img/dp.jpg", "Lê Biên Cương", "Thương vụ Việt Nam tại Cam-pu-chia", "Tham tán Thương mại", "kh@moit.gov.vn", "024. 2220 2317"],
        ["3", "../assets/img/dp.jpg", "Phạm Khắc Tuyên", "Thương vụ Việt Nam tại Hàn Quốc", "Bí thư thứ nhất phụ trách Thương vụ", " kr@moit.gov.vn", "024. 2220 2319"],
        ["4", "../assets/img/dp.jpg", "Ngô Quang Huy", "Thương vụ Việt Nam tại Hàn Quốc", "Tùy viên thương mại", "huyngoquang@moit.gov.vn", "(04) 22 202 303"],
        ["5", "../assets/img/dp.jpg", "Bùi Việt Trường", "Thương vụ Việt Nam tại Hàn Quốc", "Tùy viên thương mại", "truongbv@moit.gov.vn", "024 2220 5467"],
        ["6", "../assets/img/dp.jpg", "Nguyễn Duy Kiên", "Thương vụ Việt Nam tại Hồng Kông", "Tham tán thương mại", "kiennd@moit.gov.vn", "024 2220 5472"],
        ["7", "../assets/img/dp.jpg", "Đoàn Hải", "Thương vụ Việt Nam tại Hồng Kông", "Tùy viên thương mại", "haid@moit.gov.vn", "024 2220 5595"],
        ["8", "../assets/img/dp.jpg", "Vũ Thị Thúy", "Thương vụ Việt Nam tại Hồng Kông", "Tùy viên thương mại", "thuyvt@moit.gov.vn", "(04) 2220 5469"],
        ["9", "../assets/img/dp.jpg", "Trương Xuân Trung", "Thương vụ Việt Nam tại In-Đô-Nê-Xi-A", " Phụ trách Thương vụ", "HoaNgt@moit.gov.vn", "024 2220 5473"],
        ["10", "../assets/img/dp.jpgg", "Phạm Văn Khánh", "Thương vụ Việt Nam tại Lào", " Tham tán thương mại", "trungtx@moit.gov.vn", "024 2220 2436"],
        ["11", "../assets/img/dp.jpg", "Hồ Đức Dũng", "Thương vụ Việt Nam tại Lào", " Tham tán thương mại", "la@moit.gov.vn", "024 2220 2314"]
    ];
    var dataSet11 = [
        ["1", "../assets/img/dp.jpg", "Phạm Nguyên Minh", "Viện nghiên cứu thương mại", "Viện trưởng", "minhpn@moit.gov.vn", "(04) 38 260716"],
        ["2", "../assets/img/dp.jpg", "Phạm Ngọc Hải", "Viện nghiên cứu chiến lược chính sách Công Thương", "Phó viện trưởng", "HaiPN.ipsi@moit.gov.vn", "(04) 39 346 198"],
        ["3", "../assets/img/dp.jpg", "Trịnh Thị Thanh Thủy", "Viện nghiên cứu chiến lược chính sách Công Thương", "Phó viện trưởng", "huyttt.vit@moit.gov.vn", "04.39426672"],
        ["4", "../assets/img/dp.jpg", "Vũ Quang Hùng", "Viện nghiên cứu chiến lược chính sách Công Thương", "Phó viện trưởng", "HungVQ@moit.gov.vn", "04 38 246 237"]
    ];
    var dataSet12 = [
       
    ];

    $(document).on('click', '[data-nodeid="0"]', function () {
        table1.clear().rows.add(dataSet1).draw();
    })
    $(document).on('click', '[data-nodeid="1"]', function () {
        table1.clear().rows.add(dataSet2).draw();
    })
    $(document).on('click', '[data-nodeid="4"]', function () {
        table1.clear().rows.add(dataSet3).draw();
    })
    $(document).on('click', '[data-nodeid="5"]', function () {
        table1.clear().rows.add(dataSet4).draw();
    })
    $(document).on('click', '[data-nodeid="6"]', function () {
        table1.clear().rows.add(dataSet5).draw();
    })
    $(document).on('click', '[data-nodeid="7"]', function () {
        table1.clear().rows.add(dataSet6).draw();
    })
    $(document).on('click', '[data-nodeid="8"]', function () {
        table1.clear().rows.add(dataSet7).draw();
    })
    $(document).on('click', '[data-nodeid="9"]', function () {
        table1.clear().rows.add(dataSet8).draw();
    })
    $(document).on('click', '[data-nodeid="10"]', function () {
        table1.clear().rows.add(dataSet9).draw();
    })
    $(document).on('click', '[data-nodeid="11"]', function () {
        table1.clear().rows.add(dataSet11).draw();
    })
    $(document).on('click', '[data-nodeid="12"]', function () {
        table1.clear().rows.add(dataSet12).draw();
    })
    var table1 = $('#example4').DataTable({
        data: dataSet1,
        columns: [
            { title: "TT" },
            {
                title: "Ảnh",
                render: function (data) {
                    return '<img style="width: 50px;" src="' + data + '">';
                }},
            { title: "Tên nhân viên" },
            { title: "Phòng ban" },
            { title: "Chức vụ" },
            { title: "Email" },
            { title: "Điện thoại" }
        ]
    });

    var dataSetvb1 = [
        ["1", "<b>22/2018/TT-BCT</b>", "<b>21-08-2018</b>", "<a class='viewvbphapluat'><b>Thông tư hướng dẫn về đấu giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>19/2018/TT-BCT</b>", "<b>19-07-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xây dựng và ban hành văn bản quy phạm pháp luật của Bộ Công Thương</b></a>", "<b>Quy định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "15/2018/TT-BCT", "29-06-2018", "Thông tư quy định về việc phân luồng trong quy trình cấp Giấy chứng nhận xuất xứ hàng hóa ưu đãi", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["4", "13/2018/TT-BCT", "15-06-2018", "Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["5", "06/2018/TT-BCT", "20-04-2018", "Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["6", "05/2018/TT-BCT", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["7", "6761/BCT-TTTN", "22-08-2018", "Công văn về việc điều hành kinh doanh xăng dầu", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["8", "06/CT-BCT", "06-08-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", "Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["9", "2537/QĐ-BCT", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["10", "16/2018/TT-BCT", "04-07-2018", "Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["11", "2265/QĐ-BCT", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["12", "14/VBHN-BCT", "22-02-2018", "Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["13", "13/VBHN-BCT", "22-02-2018", "Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["14", "11/VBHN-BCT", "12-02-2018", "Nghị định về thương mại điện tử", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["15", "10/VBHN-BCT", "09-02-2018", "Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],

    ];

    var dataSetvb2 = [
        ["1", "<b>13/2018/TT-BCT</b>", "<b>15-06-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>06/2018/TT-BCT</b>", "<b>20-04-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "05/2018/TT-BCT", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["4", "6761/BCT-TTTN", "22-08-2018", "Công văn về việc điều hành kinh doanh xăng dầu", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["5", "06/CT-BCT", "06-08-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", "Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],

    ];

    var dataSetvb3 = [
        ["1", "<b>19/2018/TT-BCT</b>", "<b>19-07-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xây dựng và ban hành văn bản quy phạm pháp luật của Bộ Công Thương</b></a>", "<b>Quy định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>13/2018/TT-BCT</b>", "<b>15-06-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định về quản lý, sử dụng vật liệu nổ công nghiệp, tiền chất thuốc nổ sử dụng để sản xuất vật liệu nổ công nghiệp</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "06/2018/TT-BCT", "20-04-2018", "Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["4", "05/2018/TT-BCT", "03-04-2018", "Quy định về xuất xứ hàng hóa", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["5", "2537/QĐ-BCT", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["6", "2265/QĐ-BCT", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["7", "13/VBHN-BCT", "22-02-2018", "Thông tư quy định danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
    ];

    var dataSetvb4 = [
        ["1", "<b>19/2018/TT-BCT</b>", "<b>19-07-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xây dựng và ban hành văn bản quy phạm pháp luật của Bộ Công Thương</b></a>", "<b>Quy định</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>15/2018/TT-BCT</b>", "<b>29-06-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định về việc phân luồng trong quy trình cấp Giấy chứng nhận xuất xứ hàng hóa ưu đãi</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "06/2018/TT-BCT", "20-04-2018", "Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["4", "16/2018/TT-BCT", "04-07-2018", "Thông tư quy định chi tiết thi hành công tác thi đua, khen thưởng trong ngành Công Thương", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["5", "14/VBHN-BCT", "22-02-2018", "Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
    ];

    var dataSetvb5 = [
        ["1", "<b>06/2018/TT-BCT</b>", "<b>20-04-2018</b>", "<a class='viewvbphapluat'><b>Thông tư quy định chi tiết một số nội dung về các biện pháp phòng vệ thương mại</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["2", "<b>05/2018/TT-BCT</b>", "<b>03-04-2018</b>", "<a class='viewvbphapluat'><b>Quy định về xuất xứ hàng hóa</b></a>", "<b>Thông tư</b>", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["3", "06/CT-BCT", "06-08-2018", "Chỉ thị về việc tăng cường công tác quản lý Nhà nước đối với hoạt động nhập khẩu phế liệu", "Chỉ thị", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["4", "2537/QĐ-BCT", "18-07-2018", "Quyết định về việc thành lập Hội đồng đánh giá thí điểm hạn ngạch thuế quan nhập khẩu đường năm 2018", "Quyết định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["5", "2265/QĐ-BCT", "28-06-2018", "Quy định về khung giá bán buôn điện của Tập đoàn Điện lực Việt Nam cho các Tổng công ty Điện lực năm 2018", "Quy định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["6", "14/VBHN-BCT", "22-02-2018", "Nghị định về vật liệu nổ công nghiệp", "Nghị định", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
        ["7", "10/VBHN-BCT", "09-02-2018", "Nghị định quy định chi tiết thi hành một số điều của Luật điện lực và Luật sửa đổi, bổ sung một số điều của Luật điện lực", "Thông tư", "<a href='../assets/docs/vbtheodoi.pdf' download='VB-PhapLuat'><i class='fa fa-download'></i></a>"],
    ];

    $(document).on('click', '#tatca', function () {
        table2.clear().rows.add(dataSetvb1).draw();
    })
    $(document).on('click', '#vbphapquy', function () {
        table2.clear().rows.add(dataSetvb2).draw();
    })
    $(document).on('click', '#vbdieuhanh', function () {
        table2.clear().rows.add(dataSetvb3).draw();
    })
    $(document).on('click', '#vbhopnhap', function () {
        table2.clear().rows.add(dataSetvb4).draw();
    })
    $(document).on('click', '#tknangcao', function () {
        table2.clear().rows.add(dataSetvb5).draw();
    })

    var table2 = $('#example5').DataTable({
        data: dataSetvb1,
        columns: [
            { title: "TT" },
            { title: "Số/Ký hiệu" },
            { title: "Ngày" },
            { title: "Trích yếu" },
            { title: "Loại" },
            { title: "Tải" }
        ]
    });

    $(document).on('click', '.viewvbphapluat', function () {
        $('#ModalLuat').modal('show');
    });

    function getImg(data, type, full, meta) {
        //
        return '<img  src="" />';
    }
    $('#saveStage').DataTable( {
        stateSave: true
    } );
    
    var table = $('#tableGroup').DataTable({
        "columnDefs": [
            { "visible": false, "targets": 2 }
        ],
        "order": [[ 2, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#tableGroup tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
            table.order( [ 2, 'desc' ] ).draw();
        }
        else {
            table.order( [ 2, 'asc' ] ).draw();
        }
    } );
    
    
    var dataSet = [
                   [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ],
                   [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750" ],
                   [ "Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000" ],
                   [ "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060" ],
                   [ "Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700" ],
                   [ "Brielle Williamson", "Integration Specialist", "New York", "4804", "2012/12/02", "$372,000" ],
                   [ "Herrod Chandler", "Sales Assistant", "San Francisco", "9608", "2012/08/06", "$137,500" ],
                   [ "Rhona Davidson", "Integration Specialist", "Tokyo", "6200", "2010/10/14", "$327,900" ],
                   [ "Colleen Hurst", "Javascript Developer", "San Francisco", "2360", "2009/09/15", "$205,500" ],
                   [ "Sonya Frost", "Software Engineer", "Edinburgh", "1667", "2008/12/13", "$103,600" ],
                   [ "Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560" ],
                   [ "Quinn Flynn", "Support Lead", "Edinburgh", "9497", "2013/03/03", "$342,000" ],
                   [ "Charde Marshall", "Regional Director", "San Francisco", "6741", "2008/10/16", "$470,600" ],
                   [ "Haley Kennedy", "Senior Marketing Designer", "London", "3597", "2012/12/18", "$313,500" ],
                   [ "Tatyana Fitzpatrick", "Regional Director", "London", "1965", "2010/03/17", "$385,750" ],
                   [ "Michael Silva", "Marketing Designer", "London", "1581", "2012/11/27", "$198,500" ],
                   [ "Paul Byrd", "Chief Financial Officer (CFO)", "New York", "3059", "2010/06/09", "$725,000" ],
                   [ "Gloria Little", "Systems Administrator", "New York", "1721", "2009/04/10", "$237,500" ],
                   [ "Bradley Greer", "Software Engineer", "London", "2558", "2012/10/13", "$132,000" ],
                   [ "Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500" ],
                   [ "Jenette Caldwell", "Development Lead", "New York", "1937", "2011/09/03", "$345,000" ],
                   [ "Yuri Berry", "Chief Marketing Officer (CMO)", "New York", "6154", "2009/06/25", "$675,000" ],
                   [ "Caesar Vance", "Pre-Sales Support", "New York", "8330", "2011/12/12", "$106,450" ],
                   [ "Doris Wilder", "Sales Assistant", "Sidney", "3023", "2010/09/20", "$85,600" ],
                   [ "Angelica Ramos", "Chief Executive Officer (CEO)", "London", "5797", "2009/10/09", "$1,200,000" ],
                   [ "Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
                   [ "Jennifer Chang", "Regional Director", "Singapore", "9239", "2010/11/14", "$357,650" ],
                   [ "Brenden Wagner", "Software Engineer", "San Francisco", "1314", "2011/06/07", "$206,850" ],
                   [ "Fiona Green", "Chief Operating Officer (COO)", "San Francisco", "2947", "2010/03/11", "$850,000" ],
                   [ "Shou Itou", "Regional Marketing", "Tokyo", "8899", "2011/08/14", "$163,000" ],
                   [ "Michelle House", "Integration Specialist", "Sidney", "2769", "2011/06/02", "$95,400" ],
                   [ "Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500" ],
                   [ "Prescott Bartlett", "Technical Author", "London", "3606", "2011/05/07", "$145,000" ],
                   [ "Gavin Cortez", "Team Leader", "San Francisco", "2860", "2008/10/26", "$235,500" ],
                   [ "Martena Mccray", "Post-Sales support", "Edinburgh", "8240", "2011/03/09", "$324,050" ],
                   [ "Unity Butler", "Marketing Designer", "San Francisco", "5384", "2009/12/09", "$85,675" ]
               ];
                
		    $('#dataTable').DataTable( {
		        data: dataSet,
		        columns: [
		            { title: "Name" },
		            { title: "Position" },
		            { title: "Office" },
		            { title: "Extn." },
		            { title: "Start date" },
		            { title: "Salary" }
		        ]
		    } );
		    
} );