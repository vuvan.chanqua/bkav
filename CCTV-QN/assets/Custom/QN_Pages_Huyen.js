﻿$(document).ready(function () {
    //thủy lợi
    var chartTL = new CanvasJS.Chart("chartTL", {
        theme: "dark2",
        title: {
            text: "Mức nước hiện tại của các trạm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },

        axisY: {
            title: "Mực nước (cm)",

        },
        data: [{
            type: "column",
            yValueFormatString: "#,### cm",
            indexLabel: "{y}",
            dataPoints: [
                { label: "Bãi Cháy", y: 206 },
                { label: "Bến Triều", y: 163 },
                { label: "Cô Tô", y: 154 },
                { label: "Cửa Ông", y: 176 },
                { label: "Đồ Sơn", y: 164 },
                { label: "Móng Cái", y: 156 },
                { label: "Quảng Hà", y: 143 },
                { label: "Tiên Yên", y: 164 },
                { label: "Uông Bí", y: 153 },
            ]
        }]
    });

    function updateChartTL() {
        var boilerColor, deltaY, yVal;
        var dps = chartTL.options.data[0].dataPoints;
        var ten = ["Bãi Cháy", "Bến Triều", "Cô Tô", "Cửa Ông", "Đồ Sơn", "Móng Cái", "Quảng Hà", "Tiên Yên", "Uông Bí"];
        for (var i = 0; i < dps.length; i++) {
            deltaY = Math.round(2 + Math.random() * (-2 - 2));
            yVal = deltaY + dps[i].y > 0 ? dps[i].y + deltaY : 0;
            boilerColor = yVal > 200 ? "#FF2500" : yVal >= 170 ? "#FF6000" : yVal < 170 ? "#6B8E23 " : null;
            dps[i] = { label: ten[i], y: yVal, color: boilerColor };
        }
        chartTL.options.data[0].dataPoints = dps;
        chartTL.render();
    };
    updateChartTL();

    setInterval(function () { updateChartTL() }, 500);
    //Y tế

    var chartYte = new CanvasJS.Chart("chartYte", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Thống kê ca mắc cúm và chân tay miệng theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },

        axisX: {
            title: "Tháng"
        },
        axisY: {
            title: "mắc bệnh mới"
        },
        data: [{
            type: "bubble",
            legendMarkerType: "circle",
            toolTipContent: "<span style=\"color:#4F81BC \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            name: "Bệnh cúm",
            showInLegend: true,
            color: "#f1a129",
            dataPoints: [
                { x: 1, z: 23, y: 330, },
                { x: 2, z: 28, y: 390 },
                { x: 3, z: 39, y: 400 },
                { x: 4, z: 34, y: 430 },
                { x: 5, z: 24, y: 321 },
                { x: 6, z: 29, y: 250 },
                { x: 7, z: 29, y: 370 },
            ]
        },
        {
            type: "bubble",
            legendMarkerType: "circle",
            name: "Bệnh chân tay miệng",
            showInLegend: true,
            color: "#65c4f3",
            toolTipContent: "<span style=\"color:#C0504E \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            dataPoints: [
                { x: 1, z: 19, y: 200 },
                { x: 2, z: 27, y: 300 },
                { x: 3, z: 35, y: 330 },
                { x: 4, z: 32, y: 190 },
                { x: 5, z: 29, y: 189 },
                { x: 6, z: 22, y: 150 },
                { x: 7, z: 27, y: 200 },
            ]
        }]
    });
    chartYte.render();
    var chartYT = new CanvasJS.Chart("chartYT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Tình hình ngộ độc thực phẩm ở địa phương",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: [{
            title: "Người",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
        axisY2: {
            title: "Số vụ ngộ độc TP",
            lineColor: "#fb9c3a",
            tickColor: "#fb9c3a",
            labelFontColor: "#fb9c3a",
            titleFontColor: "#fb9c3a",

        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Bị ngộ độc TP",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "TP. HCM", y: 3 },
                { label: "Cẩm Phả", y: 104 },
                { label: "Móng Cái", y: 8 },
                { label: "Vân Đồn", y: 39 },
                { label: "Đầm Hà", y: 171 },
                { label: "Tiên Yên", y: 53 }
            ]
        },
        {
            type: "column",
            name: "Tử vong do NĐTP",
            color: "#72c6f1",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "TP. HCM", y: 0 },
                { label: "Cẩm Phả", y: 3 },
                { label: "Móng Cái", y: 0},
                { label: "Vân Đồn", y: 1 },
                { label: "Đầm Hà", y: 2 },
                { label: "Tiên Yên", y: 0 }
            ]
        },
        {
            type: "column",
            name: "Vụ NĐTP",
            color: "#fb9c3a",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { label: "TP. HCM", y: 3 },               
                { label: "Cẩm Phả", y: 6 },
                { label: "Móng Cái", y: 1 },
                { label: "Vân Đồn", y: 2 },
                { label: "Đầm Hà", y: 5 },
                { label: "Tiên Yên", y: 1 }
            ]
        }]
    });
    chartYT.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
    var chartDautu = new CanvasJS.Chart("chartDautu", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Giải quyết đơn thư khiếu nại, tố cáo",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {
            interval: 1,
            intervalType: "đơn vị"
        },
        axisY: {
            title: "đơn thư",
            valueFormatString: "#,000",
            gridColor: "#dedbd7",
            tickColor: "#dedbd7"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toolTipContentDautu
        },
       
        data: [{
            type: "stackedColumn",
            showInLegend: true,

            color: "#0e9ce2",
            name: "Tiếp nhận",
            dataPoints: [
                { label: "TP. HCM", y: 300 },
                { label: "Cẩm Phả", y: 104 },
                { label: "Móng Cái", y: 150 },
                { label: "Vân Đồn", y: 93 },
                { label: "Đầm Hà", y: 171 },
                { label: "Tiên Yên", y: 153 }
            ]
        },
        {
            type: "stackedColumn",
            showInLegend: true,
            name: "Đã giải quyết",
            color: "#98d8f7",
            dataPoints: [
                { label: "TP. HCM", y: 280 },
                { label: "Cẩm Phả", y: 84 },
                { label: "Móng Cái", y: 100 },
                { label: "Vân Đồn", y: 75 },
                { label: "Đầm Hà", y: 71 },
                { label: "Tiên Yên", y: 53 }
            ]
        }]
    });
    chartDautu.render();

    function toolTipContentDautu(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }

   

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    var chartHanhKhach = new CanvasJS.Chart("chartHanhKhach", {
        animationEnabled: true,
        theme: "dark2", // "light1", "dark2", "dark1", "dark2"
        title: {
            text: "Lượng hành khách du lịch theo năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Triệu lượt",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "column",
            showInLegend: true,

            name: "Số lượt khách du lịch",
            //  markerType: "square",

            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 9200 },
                { label: "2011", y: 9800 },
                { label: "2012", y: 10900 },
                { label: "2013", y: 11400 },
                { label: "2014", y: 12400 },
                { label: "2015", y: 13300 },
                { label: "2016", y: 14600 },
                { label: "2017", y: 16800 },


            ]
        },
        {
            type: "column",
            showInLegend: true,
            name: "Số lượt khách quốc tế đếns",
            // lineDashType: "dash",
            color: "#ea9407",
            dataPoints: [
                { label: "2010", y: 1200 },
                { label: "2011", y: 1456 },
                { label: "2012", y: 1653 },
                { label: "2013", y: 1768 },
                { label: "2014", y: 1898 },
                { label: "2015", y: 1973.3 },
                { label: "2016", y: 2113.1 },
                { label: "2017", y: 2501.7 },
            ]
        }]
    });
    chartHanhKhach.render();  
   
    $("#HTPage111").height($("#HTPage1").height());
   
});
var stateSliderNow = true;
var _stateSliderNow = 0; 
$('#NumberPage1').click(function () {
    if (!stateSliderNow || _stateSliderNow == 0) return;
    _stateSliderNow = 0;
    stateSliderNow = false;
    $('#NumberPage1').toggleClass('mauChu');
    $('#NumberPage2').toggleClass('mauChu');
    $('#HTPage1').toggleClass('hide');
    $("#HTPage111").height($("#HTPage1").height());
    $('#HTPage2').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage2').toggleClass('hide');
        $('#HTPage2').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)
  
})
$('#NumberPage2').click(function () {
    if (!stateSliderNow || _stateSliderNow == 1) return;
    _stateSliderNow = 1;
    stateSliderNow = false;
    $('#HTPage2').toggleClass('hide');
    $('#NumberPage1').toggleClass('mauChu');
    $('#NumberPage2').toggleClass('mauChu');
    $("#HTPage111").height($("#HTPage2").height());
    $('#HTPage1').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage1').toggleClass('hide');
        $('#HTPage1').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)
    var chartCLCT = new CanvasJS.Chart("chartCLCT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Công tác kiểm soát chất lượng công trình theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisY: {
            valueFormatString: "#0",
            title: "công trình"
        },
        axisX: {
            title: "tháng"
        },
        toolTip: {
            shared: true
        },
        data: [{
            type: "stackedArea",
            color: "#0e9ce2",
            showInLegend: true,
            toolTipContent: "<span style=\"color:#4F81BC\"><strong>{name}: </strong></span> {y}",
            name: "Số công trình đã kiểm tra",
            dataPoints: [

                { x: 2, y: 7 },
                { x: 3, y: 39 },
                { x: 4, y: 23 },
                { x: 5, y: 28 },
                { x: 6, y: 31 },
                { x: 7, y: 28 },
                { x: 8, y: 22 },
                { x: 9, y: 30 },
                { x: 10, y: 24 },

            ]
        },
        {
            type: "stackedArea",
            color: "#08608c",
            name: "Số công trình trọng điểm",
            toolTipContent: "<span style=\"color:#C0504E\"><strong>{name}: </strong></span> {y}",
            showInLegend: true,
            dataPoints: [
                { x: 2, y: 3 },
                { x: 3, y: 11 },
                { x: 4, y: 11 },
                { x: 5, y: 9 },
                { x: 6, y: 9 },
                { x: 7, y: 7 },
                { x: 8, y: 7 },
                { x: 9, y: 7 },
                { x: 10, y: 8 }
            ]
        }]
    });
    chartCLCT.render();
    var dataPoints1 = [];
    var dataPoints2 = [];

    //var chartMXH = new CanvasJS.Chart("chartMXH", {
    //    zoomEnabled: true,
    //    theme: "dark2",
    //    title: {
    //        text: "Chỉ số mạng xã hội",
    //        fontSize: 16,
    //        indexLabelFontFamily: "arial",
    //    },
    //    axisX: {
    //        title: ""
    //    },
    //    axisY: {
    //        prefix: "",
    //        includeZero: false
    //    },
    //    toolTip: {
    //        shared: true
    //    },
    //    legend: {
    //        cursor: "pointer",
    //        verticalAlign: "top",
    //        fontSize: 22,
    //        fontColor: "dimGrey",
    //        itemclick: toggleDataSeriesMXH
    //    },
    //    data: [{
    //        type: "line",
    //        xValueType: "dateTime",
    //        yValueFormatString: "$####.00",
    //        xValueFormatString: "hh:mm:ss TT",
    //        showInLegend: true,
    //        name: "Facebook",
    //        dataPoints: dataPoints1
    //    },
    //    {
    //        type: "line",
    //        xValueType: "dateTime",
    //        yValueFormatString: "$####.00",
    //        showInLegend: true,
    //        name: "Twitter",
    //        dataPoints: dataPoints2
    //    }]
    //});

    //function toggleDataSeriesMXH(e) {
    //    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    //        e.dataSeries.visible = false;
    //    }
    //    else {
    //        e.dataSeries.visible = true;
    //    }
    //    chartMXH.render();
    //}

    //var updateInterval = 3000;
    //// initial value
    //var yValue1 = 600;
    //var yValue2 = 605;

    //var time = new Date;
    //// starting at 9.30 am
    //time.setHours(9);
    //time.setMinutes(30);
    //time.setSeconds(00);
    //time.setMilliseconds(00);

    //function updateChart(count) {
    //    count = count || 1;
    //    var deltaY1, deltaY2;
    //    for (var i = 0; i < count; i++) {
    //        time.setTime(time.getTime() + updateInterval);
    //        deltaY1 = .5 + Math.random() * (-.5 - .5);
    //        deltaY2 = .5 + Math.random() * (-.5 - .5);

    //        // adding random value and rounding it to two digits.
    //        yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
    //        yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

    //        // pushing the new values
    //        dataPoints1.push({
    //            x: time.getTime(),
    //            y: yValue1
    //        });
    //        dataPoints2.push({
    //            x: time.getTime(),
    //            y: yValue2
    //        });
    //    }

    //    // updating legend text with  updated with y Value
    //    chartMXH.options.data[0].legendText = " Facebook " + yValue1;
    //    chartMXH.options.data[1].legendText = " Twitter " + yValue2;
    //    chartMXH.render();
    //}
    //// generates first set of dataPoints
    //updateChart(100);
    //setInterval(function () { updateChart() }, updateInterval);

    //Xay dung
    var chartGiaXD = new CanvasJS.Chart("chartGiaXD", {
        // "light1", "dark2", "dark1", "dark2"

        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Giá trị sản xuất XD theo loại CT",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 48.59, label: "CT nhà ở ", color: "#e8880c" },
                { y: 34.77, label: "CT KT dân dụng", color: "#f5bd55" },
                { y: 16.64, label: "CT khác", color: "#f9daa0" }

            ]
        }]
    });
    chartGiaXD.render();

    var chartDTSan = new CanvasJS.Chart("chartDTSan", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Xu hướng nhà riêng lẻ XD hoàn thành",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisY: {
            includeZero: false,
            title: "m2",
            valueFormatString: "#,000",
        },
        data: [{
            type: "line",
            name: "Nhà kiên cố",
            color: "#e8880c",
            showInLegend: true,
            // axisYIndex: 1,
            dataPoints: [             
                { label: "2010", y: 351722.17 },               
                { label: "2013", y: 321295 },
                { label: "2014", y: 353517 },
                { label: "2015", y: 389527 },
                { label: "2016", y: 308536 },
            ]
        }, {
            type: "line",
                name: "Nhà khác",
            color: "#f5bd55",
            showInLegend: true,
            //axisYIndex: 1,
            dataPoints: [
                { label: "2010", y: 440769.7 },
                { label: "2013", y: 447856 },
                { label: "2014", y: 388843 },
                { label: "2015", y: 385839 },
                { label: "2016", y: 475914 },
            ]
        }]
    });
    chartDTSan.render();
    var chartGiaodich = new CanvasJS.Chart("chartGiaodich", {
        theme: "dark2", // "light1", "dark2", "dark1", "dark2"
        animationEnabled: true,
        title: {
            text: "Lượng giao dịch bất động sản 2017 theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {
            interval: 1,
            intervalType: "month",
            valueFormatString: "MM"
        },
        axisY: {
            title: "Giao dịch BĐS",
            valueFormatString: "#0"
        },
        data: [{
            type: "column",
            markerSize: 12,
            showInLegend: true,
            color: "#f7900d",
            name: "Năm 2018",
            xValueFormatString: "MM",
            yValueFormatString: "###.#",
            dataPoints: [
                { x: new Date(2017, 00, 1), y: 1300 },
                { x: new Date(2017, 01, 1), y: 800 },
                { x: new Date(2017, 02, 1), y: 1000 },
                { x: new Date(2017, 03, 1), y: 1050 },
                { x: new Date(2017, 04, 1), y: 1200 },
                { x: new Date(2017, 05, 1), y: 1300 },
                { x: new Date(2017, 06, 1), y: 1300 },
                { x: new Date(2017, 07, 1), y: 1350 },
                { x: new Date(2017, 08, 1), y: 1200 },
                { x: new Date(2017, 09, 1), y: 1400 },

            ]
        }, {
            type: "column",
            markerSize: 12,
            showInLegend: true,
            color: "#f7b969",
            name: "Năm 2017",
            xValueFormatString: "MM",
            yValueFormatString: "###.#",
            dataPoints: [
                { x: new Date(2017, 00, 1), y: 1395 },
                { x: new Date(2017, 01, 1), y: 900 },
                { x: new Date(2017, 02, 1), y: 1100 },
                { x: new Date(2017, 03, 1), y: 1170 },
                { x: new Date(2017, 04, 1), y: 1300 },
                { x: new Date(2017, 05, 1), y: 1405 },
                { x: new Date(2017, 06, 1), y: 1350 },
                { x: new Date(2017, 07, 1), y: 1400 },
                { x: new Date(2017, 08, 1), y: 1300 },
                { x: new Date(2017, 09, 1), y: 1500 },
            ]

        }]
    });
    chartGiaodich.render();
    var chartVBPL = new CanvasJS.Chart("chartVBPL", {
        theme: "dark2",
        animationEnabled: true,
        //  exportEnabled: true,
        title: {
            text: "Tình hình giải quyết hồ sơ trong tháng",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} hồ sơ (#percent%)",
            dataPoints: [
                { y: 6, label: "Tiếp nhận", color: "#045b9a" },
                { y: 11, label: "Đang xử lý", color: "#1296f7" },
                { y: 15, label: "Đã trả kết quả", color: "#5cb3f3" },
            ]
        }]
    });
    chartVBPL.render();
    var chartTieuchuan = new CanvasJS.Chart("chartTieuchuan", {
        theme: "dark2",
        animationEnabled: true,
        // exportEnabled: true,
        title: {
            text: "Tình hình xử lý hồ sơ ",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} hồ sơ (#percent%)",
            dataPoints: [
                { y: 99, label: "Đúng hạn", color: "#045b9a" },
                { y: 10, label: "Quá hạn", color: "#1296f7" },

            ]
        }]
    });
    chartTieuchuan.render();
    //Biểu đồ tội phạm
    var chartToipham = new CanvasJS.Chart("chartToipham", {
        theme: "dark2", // "light1", "light2", "dark1", "dark2"

        animationEnabled: true,
        title: {
            text: "Các loại tội phạm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 47.4, label: "Tội phạm trật tự xã hội", color: "#ef960e" },
                { y: 33.1, label: "Tội phạm trật tự QL kinh tế", color: "#e8a43a" },
                { y: 10.5, label: "Tội phạm ma túy", color: "#f1c177" },
                { y: 9, label: "Tội phạm môi trường", color: "#dcccb2" }

            ]
        }]
    });
    chartToipham.render();
    var chartTainanGT = new CanvasJS.Chart("chartTainanGT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "An ninh trật tự",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Tai nạn giao thông",
            color: "#3ea8f7",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 7 },
                { label: "T4", y: 12 },
                { label: "T5", y: 1 },
            ]
        }, {
            type: "line",
            name: "Cướp giật",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 2 },
                { label: "T4", y: 7 },
                { label: "T5", y: 5 },
            ]

        }, {
            type: "line",
            name: "Hỏa hoạn",
            color: "#e809f5",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 2 },
                { label: "T2", y: 4 },
                { label: "T3", y: 2 },
                { label: "T4", y: 0 },
                { label: "T5", y: 1 },
            ]

        }, {
            type: "line",
            name: "Gây rối trật tự công cộng",
            color: "#09f3f5",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 2 },
                { label: "T2", y: 0 },
                { label: "T3", y: 1 },
                { label: "T4", y: 0 },
                { label: "T5", y: 2 },
            ]

        }, {
            type: "line",
            name: "Giết người",
            color: "#41f509",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 1 },
                { label: "T2", y: 0 },
                { label: "T3", y: 0 },
                { label: "T4", y: 1 },
                { label: "T5", y: 0 },
            ]
        }]
    });
    chartTainanGT.render();
    var chartTainanGT1 = new CanvasJS.Chart("chartTainanGT1", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "An ninh trật tự",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "Số vụ",
            color: "#ea9407",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 500 },
                { label: "T2", y: 300 },
                { label: "T3", y: 700 },
                { label: "T4", y: 120 },
                { label: "T5", y: 100 },
            ]
        }, {
            type: "column",
            name: "Bị thương",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 250 },
                { label: "T2", y: 230 },
                { label: "T3", y: 420 },
                { label: "T4", y: 70 },
                { label: "T5", y: 150 },
            ]

        }, {
            type: "column",
            name: "Người chết",
            color: "#a56908",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 150 },
                { label: "T2", y: 90 },
                { label: "T3", y: 150 },
                { label: "T4", y: 50 },
                { label: "T5", y: 70 },
            ]


        }]
    });
    chartTainanGT1.render();
   
})

$('.showIframSVH').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/790a8179-db18-4a5f-b68e-9018a9a50d3f/sheet/859418a0-4ad4-49c3-9e26-8938705bf8be/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
});

$('.showIframSKH').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/a7e3ff43-5227-401a-b86d-66e4559a8d6f/sheet/800e6d01-9725-4dbf-bfaf-7f9aff99c4c2/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSTC').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/2fb069d5-c011-471f-a070-d07dfac059a6/sheet/7de6e614-7b07-4a07-94fc-066cbb0739ad/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSCT').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/2fb069d5-c011-471f-a070-d07dfac059a6/sheet/7de6e614-7b07-4a07-94fc-066cbb0739ad/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSXD').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/2fb069d5-c011-471f-a070-d07dfac059a6/sheet/7de6e614-7b07-4a07-94fc-066cbb0739ad/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSTP').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/2fb069d5-c011-471f-a070-d07dfac059a6/sheet/7de6e614-7b07-4a07-94fc-066cbb0739ad/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSGD').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/f611cf91-2c34-4209-913e-65a9fd86e545/sheet/7017f4fa-a502-47a8-94dc-082a97efb815/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframSVH').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/790a8179-db18-4a5f-b68e-9018a9a50d3f/sheet/859418a0-4ad4-49c3-9e26-8938705bf8be/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})
$('.showIframSNV').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/5f40c75d-e721-4ae8-9ac4-0fadd0529422/sheet/18b71436-d17b-4e23-924a-850216b390ef/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})
$('.showIframSLD').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/f611cf91-2c34-4209-913e-65a9fd86e545/sheet/b3b70c93-a6be-43f3-85cc-cdb5b9ac4aa4/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})
$('.showIframSYT').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/635686fa-d043-497a-9933-ff87f33272c1/sheet/5b961c5d-da8a-4f95-be7c-1a389c0d2a77/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})

$('.showIframHuyen').click(function () {
    $('#ifamModal iframe').remove();
    $('#ifamModal').append('<iframe src="http://222.254.35.115/sense/app/cec0a457-616a-4510-8732-923350bca4e7/sheet/859418a0-4ad4-49c3-9e26-8938705bf8be/state/analysis" style="width:100%; height:80vh;" frameborder="0"></iframe>');
    $('#ModalConfirm').modal('show');
})
