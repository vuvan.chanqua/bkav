﻿$(document).ready(function () {
    $('#dstatca').show();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').hide();
});

$('#btntatca').click(function () {
    $('#dstatca').show();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').hide();
})
$('#btnvbchoxuly').click(function () {
    $('#dstatca').hide();
    $('#dsvbchoxuly').show();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').hide();
})
$('#btnvanbandaxuly').click(function () {
    $('#dstatca').hide();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').show();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').hide();
})
$('#btnvanbantheodoi').click(function () {
    $('#dstatca').hide();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').show();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').hide();
})
$('#btnvanbanlienthong').click(function () {
    $('#dstatca').hide();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').show();
    $('#dsvbhoanthanh').hide();
})
$('#btnvanbanhoanthanh').click(function () {
    $('#dstatca').hide();
    $('#dsvbchoxuly').hide();
    $('#dsvbdaxuly').hide();
    $('#dsvbtheodoi').hide();
    $('#dsvblienthong').hide();
    $('#dsvbhoanthanh').show();
})

$('.itemvbtatca').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vbtatca.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})
$('.itemvbchoxuly').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vbchoxuly.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})
$('.itemvbdaxuly').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vbdaxuly.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})
$('.itemvbtheodoi').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vbtheodoi.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})
$('.itemvblienthong').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vblienthong.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})
$('.itemvbhoanthanh').click(function () {
    $('ifram').remove();
    $('#content-modal').append('<iframe src="../assets/docs/vbdahoanthanh.pdf" style="width: 100%; height: 80vh;"></iframe>');
    $('#ModalVanBan').modal('show');
})

$('#soanvanban').click(function () {
    $('#ModalSoanVanBan').modal('show');
})