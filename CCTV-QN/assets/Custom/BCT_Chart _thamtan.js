﻿$(document).ready(function () {
   
    var chartDV = new CanvasJS.Chart("chartDV", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Cơ cấu theo loại hình dịch vụ",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 36.1, label: "Dịch vụ du lịch", color: "#f9b042" },
                { y: 44.5, label: "Dịch vụ vận tải", color: "#f79706" },
                { y: 3, label: "Dịch vụ bảo hiểm", color: "#8e5a0c" },
                { y: 20.4, label: "Dịch vụ khác", color: "#e2cb94" }

            ]
        }]
    });
    chartDV.render();
    var chartDV1 = new CanvasJS.Chart("chartDV1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Cơ cấu theo loại hình dịch vụ",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 40, label: "Dịch vụ du lịch", color: "#f9b042" },
                { y: 34, label: "Dịch vụ vận tải", color: "#f79706" },
                { y: 10, label: "Dịch vụ bảo hiểm", color: "#8e5a0c" },
                { y: 16, label: "Dịch vụ khác", color: "#e2cb94" }

            ]
        }]
    });
    chartDV1.render();
    var chartDV2 = new CanvasJS.Chart("chartDV2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Cơ cấu theo loại hình dịch vụ",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 50, label: "Dịch vụ du lịch", color: "#f9b042" },
                { y: 30, label: "Dịch vụ vận tải", colosr: "#f79706" },
                { y: 5, label: "Dịch vụ bảo hiểm", color: "#8e5a0c" },
                { y: 15, label: "Dịch vụ khác", color: "#e2cb94" }

            ]
        }]
    });
    chartDV2.render();
    var chartNK = new CanvasJS.Chart("chartNK", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Nhập khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 47, label: "Nguyên, nhiên, vật liệu", color: "#0b7879" },
                { y: 41, label: "Máy móc, thiết bị , phụ tùng", color: "#09d2d4" },
                { y: 12, label: "Hàng tiêu dùng", color: "#77f1f3" }
               

            ]
        }]
    });
    chartNK.render();
    var chartNK1 = new CanvasJS.Chart("chartNK1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Nhập khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 41, label: "Nguyên, nhiên, vật liệu", color: "#0b7879" },
                { y: 39, label: "Máy móc, thiết bị , phụ tùng", color: "#09d2d4" },
                { y: 20, label: "Hàng tiêu dùng", color: "#77f1f3" }


            ]
        }]
    });
    chartNK1.render();
    var chartNK2 = new CanvasJS.Chart("chartNK2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Nhập khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 25, label: "Nguyên, nhiên, vật liệu", color: "#0b7879" },
                { y: 35, label: "Máy móc, thiết bị , phụ tùng", color: "#09d2d4" },
                { y: 40, label: "Hàng tiêu dùng", color: "#77f1f3" }


            ]
        }]
    });
    chartNK2.render();
    var chartKHCN = new CanvasJS.Chart("chartKHCN", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Xuất khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 41, label: "Công nghiệp nhẹ" },
                { y: 39, label: "Công nghiệp nặng và khoáng sản" },
                { y: 10, label: "Nông sản" },
                { y: 7, label: "Thủy sản" },
                { y: 3, label: "Khác" },
            ]
        }]
    });
    chartKHCN.render();
    var chartKHCN1 = new CanvasJS.Chart("chartKHCN1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Xuất khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 35, label: "Công nghiệp nhẹ" },
                { y: 39, label: "Công nghiệp nặng và khoáng sản" },
                { y: 15, label: "Nông sản" },
                { y: 6, label: "Thủy sản" },
                { y: 5, label: "Khác" },
            ]
        }]
    });
    chartKHCN1.render();
    var chartKHCN2 = new CanvasJS.Chart("chartKHCN2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Xuất khẩu theo nhóm hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 19, label: "Công nghiệp nhẹ" },
                { y: 25, label: "Công nghiệp nặng và khoáng sản" },
                { y: 35, label: "Nông sản" },
                { y: 15, label: "Thủy sản" },
                { y: 6, label: "Khác" },
            ]
        }]
    });
    chartKHCN2.render();
    var chartKTKS = new CanvasJS.Chart("chartKTKS", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,

        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 55.6, label: "Xuất khẩu", color: "#f9b042" },
                { y: 44.4, label: "Nhập khẩu", color: "#f79706" },
               

            ]
        }]
    });
    chartKTKS.render();
    var chartXNK = new CanvasJS.Chart("chartXNK", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 7 },
                { label: "T4", y: 12 },
                { label: "T5", y: 1 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 4 },
                { label: "T2", y: 3 },
                { label: "T3", y: 2 },
                { label: "T4", y: 7 },
                { label: "T5", y: 5 },
            ]

       
        }]
    });
    chartXNK.render();
    var chartKTKS1 = new CanvasJS.Chart("chartKTKS1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 60, label: "Xuất khẩu", color: "#f9b042" },
                { y: 40, label: "Nhập khẩu", color: "#f79706" },


            ]
        }]
    });
    chartKTKS1.render();
    var chartXNK1 = new CanvasJS.Chart("chartXNK1", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 8 },
                { label: "T3", y: 15 },
                { label: "T4", y: 10 },
                { label: "T5", y: 50 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 30 },
                { label: "T2", y: 15 },
                { label: "T3", y: 20 },
                { label: "T4", y: 17 },
                { label: "T5", y: 25 },
            ]


        }]
    });
    chartXNK1.render();
    var chartKTKS2 = new CanvasJS.Chart("chartKTKS2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 49, label: "Xuất khẩu", color: "#f9b042" },
                { y: 51, label: "Nhập khẩu", color: "#f79706" },


            ]
        }]
    });
    chartKTKS2.render();
    var chartXNK2 = new CanvasJS.Chart("chartXNK2", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 120 },
                { label: "T2", y: 150},
                { label: "T3", y: 90 },
                { label: "T4", y: 100 },
                { label: "T5", y: 159 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 120 },
                { label: "T2", y: 170},
                { label: "T3", y: 200 },
                { label: "T4", y: 160 },
                { label: "T5", y: 150},
            ]


        }]
    });
    chartXNK2.render();
    var chartTTDien = new CanvasJS.Chart("chartTTDien", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Bình quân xuất nhập khẩu theo năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 4800 },
                { label: "2014", y: 5403 },
                { label: "2015", y: 6750 },
                { label: "2016", y: 7500 },
                { label: "2017", y: 8020 }
            ]
        }, {
            type: "column",
            name: "nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 2580 },
                { label: "2014", y: 3578 },
                { label: "2015", y: 4500 },
                { label: "2016", y: 3590 },
                { label: "2017", y: 4079 }
            ]

       

        }]
    });
    chartTTDien.render();
    var chartTTDien1 = new CanvasJS.Chart("chartTTDien1", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Bình quân xuất nhập khẩu theo năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 2350 },
                { label: "2014", y: 4500 },
                { label: "2015", y: 5340 },
                { label: "2016", y: 6580 },
                { label: "2017", y: 7200 }
            ]
        }, {
            type: "column",
            name: "nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 4570 },
                { label: "2014", y: 2345 },
                { label: "2015", y: 3450 },
                { label: "2016", y: 3590 },
                { label: "2017", y: 4100 }
            ]



        }]
    });
    chartTTDien1.render();
    var chartTTDien2 = new CanvasJS.Chart("chartTTDien2", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Bình quân xuất nhập khẩu theo năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 3070 },
                { label: "2014", y: 3900 },
                { label: "2015", y: 5500 },
                { label: "2016", y: 6800 },
                { label: "2017", y: 7600 }
            ]
        }, {
            type: "column",
            name: "nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "2013", y: 4570 },
                { label: "2014", y: 2345 },
                { label: "2015", y: 3450 },
                { label: "2016", y: 4500 },
                { label: "2017", y: 5780 }
            ]



        }]
    });
    chartTTDien2.render();

    $('#chisoKHCN1').show();
    $('#chisoKHCN2').hide();
    $('#chisoKHCN3').hide();
    $('#XNK1').show();
    $('#XNK2').hide();
    $('#XNK3').hide();
})

$(document).on('click', '#jqvmap1_ca', function () {
    $('#ttdefault').addClass('hidden');
    $('#ttAustralia').addClass('hidden');
    $('#ttTrungQuoc').addClass('hidden');
    $('#ttNga').addClass('hidden');
    $('#ttCanada').removeClass('hidden');
    $('#chisoKHCN2').show();
    $('#chisoKHCN1').hide();
    $('#chisoKHCN3').hide();
    $('#XNK2').show();
    $('#XNK1').hide();
    $('#XNK3').hide();
})
$(document).on('click', '#jqvmap1_ru', function () {
    $('#ttdefault').addClass('hidden');
    $('#ttAustralia').addClass('hidden');
    $('#ttTrungQuoc').addClass('hidden');
    $('#ttNga').removeClass('hidden');
    $('#ttCanada').addClass('hidden');
    $('#chisoKHCN1').show();
    $('#chisoKHCN2').hide();
    $('#chisoKHCN3').hide();
    $('#XNK1').show();
    $('#XNK2').hide();
    $('#XNK3').hide();
})
$(document).on('click', '#jqvmap1_cn', function () {
    $('#ttdefault').addClass('hidden');
    $('#ttAustralia').addClass('hidden');
    $('#ttTrungQuoc').removeClass('hidden');
    $('#ttNga').addClass('hidden');
    $('#ttCanada').addClass('hidden');
    $('#XNK3').show();
    $('#XNK2').hide();
    $('#XNK1').hide();
    $('#chisoKHCN3').show();
    $('#chisoKHCN2').hide();
    $('#chisoKHCN1').hide();
})

$(document).on('click', '#jqvmap1_au', function () {
    $('#ttdefault').addClass('hidden');
    $('#ttAustralia').removeClass('hidden');
    $('#ttTrungQuoc').addClass('hidden');
    $('#ttNga').addClass('hidden');
    $('#ttCanada').addClass('hidden');
    $('#XNK2').show();
    $('#XNK1').hide();
    $('#XNK3').hide();
    $('#chisoKHCN2').show();
    $('#chisoKHCN1').hide();
    $('#chisoKHCN3').hide();
})