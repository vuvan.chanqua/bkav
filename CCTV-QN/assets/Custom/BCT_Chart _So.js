﻿$(document).ready(function () {
    var chartATTP = new CanvasJS.Chart("chartATTP", {
        theme: "light2",
        title: {
            text: "Biểu đồ kiểm tra, xử lý về an toàn thực phẩm theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: [{
            title: "Số vụ vi phạm bị phạt hành chính",
            lineColor: "#C24642",
            tickColor: "#C24642",
            labelFontColor: "#C24642",
            titleFontColor: "#C24642",
            suffix: "k"
        },
        {
            title: "Số vụ kiểm tra",
            lineColor: "#369EAD",
            tickColor: "#369EAD",
            labelFontColor: "#369EAD",
            titleFontColor: "#369EAD",
            suffix: "k"
        }],
        axisY2: {
            title: "Thu về ngân sách",
            lineColor: "#7F6084",
            tickColor: "#7F6084",
            labelFontColor: "#7F6084",
            titleFontColor: "#7F6084",
            prefix: "triệu đồng",
            suffix: "k"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATTP
        },
        data: [{
            type: "line",
            name: "Số vụ kiểm tra",
            color: "#369EAD",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 85.4 },
                { x: 2, y: 92.7 },
                { x: 3, y: 64.9 },
                { x: 4, y: 58.0 },
                { x: 5, y: 63.4 },
                { x: 6, y: 69.9 },
                { x: 7, y: 88.9 },
                { x: 8, y: 66.3 },

            ]
        },
        {
            type: "line",
            name: "Số vụ vi phạm bị phạt hành chính",
            color: "#C24642",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 32.3 },
                { x: 2, y: 33.9 },
                { x: 3, y: 26.0 },
                { x: 4, y: 15.8 },
                { x: 5, y: 18.6 },
                { x: 6, y: 34.6 },
                { x: 7, y: 37.7 },
                { x: 8, y: 24.7 },

            ]
        },
        {
            type: "line",
            name: "Thu về ngân sách",
            color: "#7F6084",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42.5 },
                { x: 2, y: 44.3 },
                { x: 3, y: 28.7 },
                { x: 4, y: 22.5 },
                { x: 5, y: 25.6 },
                { x: 6, y: 45.7 },
                { x: 7, y: 54.6 },
                { x: 8, y: 32.0 },

            ]
        }]
    });
    chartATTP.render();

    function toggleDataSeriesATTP(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATTP.render();
    }
    var chartATTP_TK = new CanvasJS.Chart("chartATTP_TK", {
        animationEnabled: true,
        title: {
            text: ""
        },
        axisX: {
            interval: 1
        },
        axisY: {
            title: "",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y}",
            dataPoints: [
                { label: "Cấp mới Giấy chứng nhận ATTP", y: 178 },
                { label: "Cấp lại giấy chứng nhận ATTP", y: 228 },
                { label: "Cơ sở được cấp giấy chứng nhận ATTP", y: 128 },
                { label: "Cơ sở vi phạm phạt hành chính", y: 24 },
                { label: "Cơ sở vi phạm bị thu hồi giấy chứng nhận", y: 8 }

            ]
        }]
    });
    chartATTP_TK.render();
    var chartATTP1 = new CanvasJS.Chart("chartATTP1", {
        theme: "light2",
        title: {
            text: "Biểu đồ kiểm tra, xử lý về an toàn thực phẩm theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: [{
            title: "Số vụ vi phạm bị phạt hành chính",
            lineColor: "#C24642",
            tickColor: "#C24642",
            labelFontColor: "#C24642",
            titleFontColor: "#C24642",
            suffix: "k"
        },
        {
            title: "Số vụ kiểm tra",
            lineColor: "#369EAD",
            tickColor: "#369EAD",
            labelFontColor: "#369EAD",
            titleFontColor: "#369EAD",
            suffix: "k"
        }],
        axisY2: {
            title: "Thu về ngân sách",
            lineColor: "#7F6084",
            tickColor: "#7F6084",
            labelFontColor: "#7F6084",
            titleFontColor: "#7F6084",
            prefix: "triệu đồng",
            suffix: "k"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATTP1
        },
        data: [{
            type: "line",
            name: "Số vụ kiểm tra",
            color: "#369EAD",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 60},
                { x: 2, y: 78 },
                { x: 3, y: 90 },
                { x: 4, y: 120 },
                { x: 5, y: 74 },
                { x: 6, y: 80 },
                { x: 7, y: 90 },
                { x: 8, y: 60 },

            ]
        },
        {
            type: "line",
            name: "Số vụ vi phạm bị phạt hành chính",
            color: "#C24642",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 50 },
                { x: 2, y: 60 },
                { x: 3, y: 67 },
                { x: 4, y: 90 },
                { x: 5, y: 50 },
                { x: 6, y: 76 },
                { x: 7, y: 65 },
                { x: 8, y: 30 },

            ]
        },
        {
            type: "line",
            name: "Thu về ngân sách",
            color: "#7F6084",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 35.5 },
                { x: 2, y: 44.3 },
                { x: 3, y: 28.7 },
                { x: 4, y: 57.0 },
                { x: 5, y: 25.6 },
                { x: 6, y: 54.6 },
                { x: 7, y: 47.0 },
                { x: 8, y: 32.0 },

            ]
        }]
    });
    chartATTP1.render();
    var chartATTP2 = new CanvasJS.Chart("chartATTP2", {
        theme: "light2",
        title: {
            text: "Biểu đồ kiểm tra, xử lý về an toàn thực phẩm theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: [{
            title: "Số vụ vi phạm bị phạt hành chính",
            lineColor: "#C24642",
            tickColor: "#C24642",
            labelFontColor: "#C24642",
            titleFontColor: "#C24642",
            suffix: "k"
        },
        {
            title: "Số vụ kiểm tra",
            lineColor: "#369EAD",
            tickColor: "#369EAD",
            labelFontColor: "#369EAD",
            titleFontColor: "#369EAD",
            suffix: "k"
        }],
        axisY2: {
            title: "Thu về ngân sách",
            lineColor: "#7F6084",
            tickColor: "#7F6084",
            labelFontColor: "#7F6084",
            titleFontColor: "#7F6084",
            prefix: "triệu đồng",
            suffix: "k"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATTP2
        },
        data: [{
            type: "line",
            name: "Số vụ kiểm tra",
            color: "#369EAD",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 100 },
                { x: 2, y: 80 },
                { x: 3, y: 90 },
                { x: 4, y: 95 },
                { x: 5, y: 110 },
                { x: 6, y: 80 },
                { x: 7, y: 90 },
                { x: 8, y: 70 },

            ]
        },
        {
            type: "line",
            name: "Số vụ vi phạm bị phạt hành chính",
            color: "#C24642",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 65 },
                { x: 2, y: 35 },
                { x: 3, y: 47 },
                { x: 4, y: 55 },
                { x: 5, y: 60 },
                { x: 6, y: 70 },
                { x: 7, y: 55 },
                { x: 8, y: 40 },

            ]
        },
        {
            type: "line",
            name: "Thu về ngân sách",
            color: "#7F6084",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 35.5 },
                { x: 2, y: 24.3 },
                { x: 3, y: 28.7 },
                { x: 4, y: 47.0 },
                { x: 5, y: 55.6 },
                { x: 6, y: 57.6 },
                { x: 7, y: 47.0 },
                { x: 8, y: 32.0 },

            ]
        }]
    });
    chartATTP2.render();
    function toggleDataSeriesATTP2(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATTP2.render();
    }
    function toggleDataSeriesATTP1(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATTP1.render();
    }
    var chartATTP_TK2 = new CanvasJS.Chart("chartATTP_TK2", {
        animationEnabled: true,
        title: {
            text: ""
        },
        axisX: {
            interval: 1
        },
        axisY: {
            title: "",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y}",
            dataPoints: [
                { label: "Cấp mới Giấy chứng nhận ATTP", y: 25 },
                { label: "Cấp lại giấy chứng nhận ATTP", y: 80},
                { label: "Cơ sở được cấp giấy chứng nhận ATTP", y: 70 },
                { label: "Cơ sở vi phạm phạt hành chính", y: 7 },
                { label: "Cơ sở vi phạm bị thu hồi giấy chứng nhận", y: 5 }

            ]
        }]
    });
    chartATTP_TK2.render();
    var chartATTP_TK1 = new CanvasJS.Chart("chartATTP_TK1", {
        animationEnabled: true,
        title: {
            text: ""
        },
        axisX: {
            interval: 1
        },
        axisY: {
            title: "",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y}",
            dataPoints: [
                { label: "Cấp mới Giấy chứng nhận ATTP", y: 55 },
                { label: "Cấp lại giấy chứng nhận ATTP", y: 90 },
                { label: "Cơ sở được cấp giấy chứng nhận ATTP", y: 90 },
                { label: "Cơ sở vi phạm phạt hành chính", y: 17 },
                { label: "Cơ sở vi phạm bị thu hồi giấy chứng nhận", y: 8 }

            ]
        }]
    });
    chartATTP_TK1.render();
    //khoa học và công nghệ
    var totalDTNC = 224,
        totalDTQG = 6,
        totalDA = 2;
    var chartDTNC = new CanvasJS.Chart("chartDTNC", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: " 47% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 47, color: "#c70000", toolTipContent: "Đề tài nghiên cứu KH hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTNC), '#,###,###') + "</span>" },
                    { y: 53, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTNC.render();

    var chartDTQG = new CanvasJS.Chart("chartDTQG", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "90% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 90, color: "#e0db0ae0", toolTipContent: "Chương trình/Đề án cấp QG đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTQG), '#,###,###') + "</span>" },
                    { y: 5, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTQG.render();
    var chartDA = new CanvasJS.Chart("chartDA", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "50% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 50, color: "#08efdb", toolTipContent: "Dự án đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDA), '#,###,###') + "</span>" },
                    { y: 50, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDA.render();
    var chartKHCN = new CanvasJS.Chart("chartKHCN", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Quản lý KHCN",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 96.55, label: "Đề tài nghiên cứu khoa học" },
                { y: 2.58, label: "Chương trình/Đề án KHCN cấp QG " },
                { y: 0.87, label: "Dự án KHCN" }


            ]
        }]
    });
    chartKHCN.render();
    ////////////////////
    var totalDTNC1 = 80,
        totalDTQG1 = 10,
        totalDA1 = 10;
    var chartDTNC1 = new CanvasJS.Chart("chartDTNC1", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: " 47% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 90, color: "#c70000", toolTipContent: "Đề tài nghiên cứu KH hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTNC1), '#,###,###') + "</span>" },
                    { y: 10, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTNC1.render();

    var chartDTQG1 = new CanvasJS.Chart("chartDTQG1", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "90% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 75, color: "#e0db0ae0", toolTipContent: "Chương trình/Đề án cấp QG đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTQG1), '#,###,###') + "</span>" },
                    { y: 25, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTQG1.render();
    var chartDA1 = new CanvasJS.Chart("chartDA1", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "50% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 70, color: "#08efdb", toolTipContent: "Dự án đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDA1), '#,###,###') + "</span>" },
                    { y: 30, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDA1.render();
    var chartKHCN1 = new CanvasJS.Chart("chartKHCN1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Quản lý KHCN",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 80, label: "Đề tài nghiên cứu khoa học", color:"#07d0c8"},
                { y: 10, label: "Chương trình/Đề án KHCN cấp QG ", color: "#099a94"},
                { y: 10, label: "Dự án KHCN", color:"#0d504d" }


            ]
        }]
    });
    chartKHCN1.render();
    ////////////////////
    var totalDTNC2 = 10,
        totalDTQG2 = 1,
        totalDA2 = 4;
    var chartDTNC2 = new CanvasJS.Chart("chartDTNC2", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: " 70% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 70, color: "#c70000", toolTipContent: "Đề tài nghiên cứu KH hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTNC2), '#,###,###') + "</span>" },
                    { y: 30, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTNC2.render();

    var chartDTQG2 = new CanvasJS.Chart("chartDTQG2", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "45% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 45, color: "#e0db0ae0", toolTipContent: "Chương trình/Đề án cấp QG đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDTQG2), '#,###,###') + "</span>" },
                    { y: 55, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDTQG2.render();
    var chartDA2 = new CanvasJS.Chart("chartDA2", {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontColor: "#848484",
            fontSize: 18,
            horizontalAlign: "center",
            text: "63% hoàn thành",
            verticalAlign: "center"
        },
        toolTip: {
            backgroundColor: "#ffffff",
            borderThickness: 0,
            cornerRadius: 0,
            fontColor: "#424242"
        },
        data: [
            {
                explodeOnClick: false,
                innerRadius: "80%",
                radius: "90%",
                startAngle: 270,
                type: "doughnut",
                dataPoints: [
                    { y: 63, color: "#08efdb", toolTipContent: "Dự án đã hoàn thành: $<span>" + CanvasJS.formatNumber(Math.round(47 / 100 * totalDA2), '#,###,###') + "</span>" },
                    { y: 37, color: "#424242", toolTipContent: null }
                ]
            }
        ]
    });
    chartDA2.render();
    var chartKHCN2 = new CanvasJS.Chart("chartKHCN2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Quản lý KHCN",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 10, label: "Đề tài nghiên cứu khoa học", color: "#07d0c8" },
                { y: 1, label: "Chương trình/Đề án KHCN cấp QG ", color: "#099a94" },
                { y: 4, label: "Dự án KHCN", color: "#0d504d" }


            ]
        }]
    });
    chartKHCN2.render();
    var chartKTKS = new CanvasJS.Chart("chartKTKS", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,

        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 47.4, label: "Than sạch" },
                { y: 33.1, label: "Khí đốt" },
                { y: 10.5, label: "Quặng" },
                { y: 9, label: "Thép" }

            ]
        }]
    });
    chartKTKS.render();
    var chartXNK = new CanvasJS.Chart("chartXNK", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 7 },
                { label: "T4", y: 12 },
                { label: "T5", y: 1 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 4 },
                { label: "T2", y: 3 },
                { label: "T3", y: 2 },
                { label: "T4", y: 7 },
                { label: "T5", y: 5 },
            ]

       
        }]
    });
    chartXNK.render();
    var chartKTKS1 = new CanvasJS.Chart("chartKTKS1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,

        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 36, label: "Bán lẻ hàng hóa", color:"#f9b042"},
                { y: 41.5, label: "Lưu trú, ăn uống", color: "#f79706"},
                { y: 10.5, label: "Du lịch", color: "#8e5a0c"},
                { y: 12, label: "Dịch vụ", color: "#f1cd97"}

            ]
        }]
    });
    chartKTKS1.render();
    var chartXNK1 = new CanvasJS.Chart("chartXNK1", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 8 },
                { label: "T3", y: 15 },
                { label: "T4", y: 10 },
                { label: "T5", y: 20 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 30 },
                { label: "T2", y: 15 },
                { label: "T3", y: 20 },
                { label: "T4", y: 17 },
                { label: "T5", y: 25 },
            ]


        }]
    });
    chartXNK1.render();
    var chartKTKS2 = new CanvasJS.Chart("chartKTKS2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,

        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 46, label: "Bán lẻ hàng hóa", color: "#f9b042" },
                { y: 34.5, label: "Lưu trú, ăn uống", color: "#f79706" },
                { y: 9.5, label: "Du lịch", color: "#8e5a0c" },
                { y: 10, label: "Dịch vụ", color: "#f1cd97" }

            ]
        }]
    });
    chartKTKS2.render();
    var chartXNK2 = new CanvasJS.Chart("chartXNK2", {
        animationEnabled: true,
        theme: "light2",

        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Xuất khẩu",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 15 },
                { label: "T2", y: 10 },
                { label: "T3", y: 15 },
                { label: "T4", y: 17 },
                { label: "T5", y: 25 },
            ]
        }, {
            type: "line",
            name: "Nhập khẩu",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 20 },
                { label: "T2", y: 15 },
                { label: "T3", y: 8 },
                { label: "T4", y: 10 },
                { label: "T5", y: 05 },
            ]


        }]
    });
    chartXNK2.render();
    var chartTTDien = new CanvasJS.Chart("chartTTDien", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tiêu thụ điện năng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "Bán lẻ",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 500 },
                { label: "T2", y: 300 },
                { label: "T3", y: 700 },
                { label: "T4", y: 120 },
                { label: "T5", y: 100 },
            ]
        }, {
            type: "column",
            name: "bán buôn",
            color: "#008000",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 250 },
                { label: "T2", y: 230 },
                { label: "T3", y: 420 },
                { label: "T4", y: 70 },
                { label: "T5", y: 150 },
            ]

       

        }]
    });
    chartTTDien.render();
    var chartTTDien1 = new CanvasJS.Chart("chartTTDien1", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tiêu thụ điện năng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "Bán lẻ",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 500 },
                { label: "T2", y: 600 },
                { label: "T3", y: 700 },
                { label: "T4", y: 750 },
                { label: "T5", y: 650 },
            ]
        }, {
            type: "column",
            name: "bán buôn",
                color: "#07d0c8",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 250 },
                { label: "T2", y: 230 },
                { label: "T3", y: 420 },
                { label: "T4", y: 500 },
                { label: "T5", y: 550 },
            ]



        }]
    });
    chartTTDien1.render();
    var chartTTDien2 = new CanvasJS.Chart("chartTTDien2", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tiêu thụ điện năng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "Bán lẻ",
            color: "#d50fea",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 650 },
                { label: "T2", y: 780 },
                { label: "T3", y: 720 },
                { label: "T4", y: 670 },
                { label: "T5", y: 750 },
            ]
        }, {
            type: "column",
            name: "bán buôn",
            color: "#07d0c8",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 400 },
                { label: "T2", y: 380 },
                { label: "T3", y: 350 },
                { label: "T4", y: 420 },
                { label: "T5", y: 500 },
            ]



        }]
    });
    chartTTDien2.render();
    $('#chisoATTP1').show();
    $('#chisoATTP2').hide();
    $('#chisoATTP3').hide();
    $('#chisoKhac1').show();
    $('#chisoKhac2').hide();
    $('#chisoKhac3').hide();
    $('#chisoKHCN1').show();
    $('#chisoKHCN2').hide();
    $('#chisoKHCN3').hide();
    $('#XNK1').show();
    $('#XNK2').hide();
    $('#XNK3').hide();
})

$('#soct1').click(function () {
    $('#chisoATTP1').show();
    $('#chisoATTP2').hide();
    $('#chisoATTP3').hide();
    $('#chisoKhac1').show();
    $('#chisoKhac2').hide();
    $('#chisoKhac3').hide();
    $('#chisoKHCN1').show();
    $('#chisoKHCN2').hide();
    $('#chisoKHCN3').hide();
    $('#XNK1').show();
    $('#XNK2').hide();
    $('#XNK3').hide();
})
$('#soct2').click(function () {
    $('#chisoATTP2').show();
    $('#chisoATTP1').hide();
    $('#chisoATTP3').hide();
    $('#chisoKhac2').show();
    $('#chisoKhac1').hide();
    $('#chisoKhac3').hide();
    $('#chisoKHCN2').show();
    $('#chisoKHCN1').hide();
    $('#chisoKHCN3').hide();
    $('#XNK2').show();
    $('#XNK1').hide();
    $('#XNK3').hide();
})

$('#soct3').click(function () {
    $('#chisoATTP3').show();
    $('#chisoATTP1').hide();
    $('#chisoATTP2').hide();
    $('#chisoKhac3').show();
    $('#chisoKhac1').hide();
    $('#chisoKhac2').hide();
    $('#chisoKHCN3').show();
    $('#chisoKHCN1').hide();
    $('#chisoKHCN2').hide();
    $('#XNK3').show();
    $('#XNK1').hide();
    $('#XNK2').hide();
})