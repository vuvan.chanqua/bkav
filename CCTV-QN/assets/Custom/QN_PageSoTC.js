﻿$(document).ready(function () {
    var dataPoints1 = [];
    var dataPoints2 = [];

    var chartMXH = new CanvasJS.Chart("chartMXH", {
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: "Chỉ số mạng xã hội",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisX: {
            title: ""
        },
        axisY: {
            prefix: "",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            verticalAlign: "top",
            fontSize: 22,
            fontColor: "dimGrey",
            itemclick: toggleDataSeriesMXH
        },
        data: [{
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            xValueFormatString: "hh:mm:ss TT",
            showInLegend: true,
            name: "Facebook",
            dataPoints: dataPoints1
        },
        {
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            showInLegend: true,
            name: "Twitter",
            dataPoints: dataPoints2
        }]
    });

    function toggleDataSeriesMXH(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartMXH.render();
    }

    var updateInterval = 3000;
    // initial value
    var yValue1 = 600;
    var yValue2 = 605;

    var time = new Date;
    // starting at 9.30 am
    time.setHours(9);
    time.setMinutes(30);
    time.setSeconds(0);
    time.setMilliseconds(0);

    function updateChart(count) {
        count = count || 1;
        var deltaY1, deltaY2;
        for (var i = 0; i < count; i++) {
            time.setTime(time.getTime() + updateInterval);
            deltaY1 = .5 + Math.random() * (-.5 - .5);
            deltaY2 = .5 + Math.random() * (-.5 - .5);

            // adding random value and rounding it to two digits.
            yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
            yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

            // pushing the new values
            dataPoints1.push({
                x: time.getTime(),
                y: yValue1
            });
            dataPoints2.push({
                x: time.getTime(),
                y: yValue2
            });
        }

        // updating legend text with  updated with y Value
        chartMXH.options.data[0].legendText = " Facebook " + yValue1;
        chartMXH.options.data[1].legendText = " Twitter " + yValue2;
        chartMXH.render();
    }
    // generates first set of dataPoints
    updateChart(100);
    setInterval(function () { updateChart() }, updateInterval);
    var chartVBPL1 = new CanvasJS.Chart("chartVBPL1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Chỉ số xây dựng VB QPPL 2016",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 21.88, label: "Nghi định", color: "#c70000", },
                { y: 9.37, label: "Quyết định" },
                { y: 67.19, label: "Thông tư", color: "#f1f706" },
                { y: 1.56, label: "VB chưa trình/ban hành", color: "#06eefb" },

            ]
        }]
    });
    chartVBPL1.render();
    
    var chartVBPL = new CanvasJS.Chart("chartVBPL", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tình hình giải quyết hồ sơ về TTHC",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Hồ sơ",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
      
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Hồ sơ đúng hạn",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Tháng 1", y: 300 },
                { label: "Tháng 2", y: 204 },
                { label: "Tháng 3", y: 450 },
                { label: "Tháng 4", y: 390 },
                { label: "Tháng 5", y: 171 },
                { label: "Tháng 6", y: 153 },
                { label: "Tháng 7", y: 139 },
                { label: "Tháng 8", y: 171 },
                { label: "Tháng 9", y: 239 },
                { label: "Tháng 10", y: 210 }
            ]
        },
        {
            type: "column",
            name: "Trễ hạn xử lý",
            color: "#FF6500",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Tháng 1", y: 20 },
                { label: "Tháng 2", y: 24 },
                { label: "Tháng 3", y: 25 },
                { label: "Tháng 4", y: 19 },
                { label: "Tháng 5", y: 17 },
                { label: "Tháng 6", y: 15 },
                { label: "Tháng 7", y: 13 },
                { label: "Tháng 8", y: 21 },
                { label: "Tháng 9", y: 23 },
                { label: "Tháng 10", y: 21 }
            ]
        
        }]
    });
    chartVBPL.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }

  
  
    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "light2",
        animationEnabled: true,

        title: {
            text: "Tiến độ thực hiện các dự án",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "pie",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} dự án (#percent%)",
            dataPoints: [
                { y: 3, label: "Hoàn thành", color: "#0a94cc" },               
                { y: 9, label: "Đang triển khai", color: "#4fc5f5" },
                { y: 6, label: "Chưatriển khai", color: "#07648a" },
            ]
        }]
    });
    chartDautuCong.render();

    //Thực hiện thu chi ngân sách theo tháng
    var chartATGT = new CanvasJS.Chart("chartATGT", {
        theme: "light2",
        title: {
            text: "Thực hiện thu , chi ngân sách theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: [{
            title: "Chi ngân sách (tỷ đồng)",
            lineColor: "#08aef3",
            tickColor: "#08aef3",
            labelFontColor: "#08aef3",
            titleFontColor: "#08aef3",


        }],
        axisY2: [
            {
                title: "Thu ngân sách (tỷ đồng)",
                lineColor: "#d88805",
                tickColor: "#d88805",
                labelFontColor: "#d88805",
                titleFontColor: "#d88805",

            }],
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATGT
        },
        data: [{
            
            type: "column",
            name: "Chi ngân sách",
            color: "#08aef3",
            axisYIndex: 1,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 5 },
                { x: 2, y: 7 },
                { x: 3, y: 4 },
                { x: 4, y: 6 },
                { x: 5, y: 4 },
                { x: 6, y: 3 },
                { x: 7, y: 7 },
                { x: 8, y: 2 },

            ]
        },
        {
            type: "column",
            name: "Thu ngân sách",
            color: "#d88805",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42 },
                { x: 2, y: 15 },
                { x: 3, y: 28 },
                { x: 4, y: 22 },
                { x: 5, y: 25 },
                { x: 6, y: 16 },
                { x: 7, y: 33 },
                { x: 8, y: 20 },

            ]
        }]
    });
    chartATGT.render();

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    var chartHanhKhach = new CanvasJS.Chart("chartHanhKhach", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Dự toán chi ngân sách địa phương",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "triệu đồng",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "line",
            showInLegend: true,

            name: "Đầu tư phát triển",
            //  markerType: "square",

            color: "#f1b656",
            dataPoints: [
                { label: "NSĐP", y: 9389355 },
                { label: "Tỉnh", y: 6381000 },
                { label: "NS H,X", y: 3008355 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Chi thường xuyên",
            // lineDashType: "dash",
            color: "#ea9407",
            dataPoints: [
                { label: "NSĐP", y: 11717609 },
                { label: "Tỉnh", y: 5028816 },
                { label: "NS H,X", y: 6688793 }
            ]
        
        }]
    });
    chartHanhKhach.render();
   
    var chartQLTT = new CanvasJS.Chart("chartQLTT", {
        theme: "light2",
        title: {
            text: "Xu hướng Giá trị xuất nhập khẩu qua các năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        axisY: {
            title: "Nghìn USD",
          
            lineColor: "#4F81BC",
            tickColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            gridThickness: 0
        },
     
        data: [{
            type: "area",
            name: "Xuất khẩu",
            legendText: "Xuất khẩu",
            yValueFormatString: "0.##' nghìn USD'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            color: "#d6d2ce",
            dataPoints: [
                { label: "2010", y: 1585.4 },
                { label: "2011", y: 1537.4 },
                { label: "2012", y: 1637 },
                { label: "2013", y: 1780 },
                { label: "2014", y: 1885.4 },
                { label: "2015", y: 1393.8 },
                { label: "2016", y: 1935.1},
                { label: "2017", y: 1981.7 }
            ]
        },

        {
            type: "area",
            name: "Nhập khẩu",
            legendText: "Nhập khẩu",
            yValueFormatString: "0.##' nghìn USD'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            color: "#ef7a08",
            dataPoints: [
                { label: "2010", y: 1270.2 },
                { label: "2011", y: 1130 },
                { label: "2012", y: 837 },
                { label: "2013", y: 780 },
                { label: "2014", y: 950 },
                { label: "2015", y: 752.9 },
                { label: "2016", y: 669.4 },
                { label: "2017", y: 756.4 }
            ]
        }]
    });
    chartQLTT.render();
    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartQLTT.render();
    }
  
   
    var totalVisitors = 1374210;
    var visitorsData = {
        "Dự toán thu": [{
            click: visitorsChartDrilldownHandler,
            // backgroundColor: "transparent",
            cursor: "pointer",
            explodeOnClick: false,
            innerRadius: "65%",
            legendMarkerType: "square",
            name: "Dự toán thu",
            radius: "100%",
            showInLegend: true,
            startAngle: 90,
            type: "doughnut",
            dataPoints: [
                { y: 36360000, name: "Ngân sách nhà nước", color: "#07648a" },
                { y: 23371884.55, name: "Ngân sách địa phương", color: "#08aef3" }
                
            ]
        }],
        "Ngân sách nhà nước": [{
            color: "#07648a",
            name: "Ngân sách nhà nước",
            type: "column",
            dataPoints: [
               
                { label: "XN quốc doanh", y: 13919567 },
                { label: "XN vốn ĐT NN", y: 2315233 },
                { label: "NQD", y: 2668000 },
                { label: "Thuế TNCN", y: 672000 },
                { label: "Lệ phí TB", y: 630000 },
                { label: "Đất", y: 2496200 },
                { label: "Thuế BVMT", y: 2401000 },
                { label: "Phí, lệ phí", y: 2640000 },
                { label: "Cấp quyền KTKS", y: 2060000 },
                { label: "XSKT", y: 58000 },
                { label: "Thu tại xã", y: 20000 },               
                { label: "Cổ tức, LNST", y: 80000 },     
                { label: "Thu khác NS", y: 400000 },   
                { label: "Hải quan", y: 6000000 },    
                { label: "BS CÓ MT TỪ NSTƯ", y: null },    
            ]
        }],
        "Ngân sách địa phương": [{
            color: "#08aef3",
            name: "Ngân sách địa phương",
            type: "column",
            dataPoints: [
              
                { label: "XN quốc doanh", y: 11484943.55 },
                { label: "XN vốn ĐT NN", y: 1579358 },
                { label: "NQD", y: 1768500 },
                { label: "Thuế TNCN", y: 436800 },
                { label: "Lệ phí TB", y: 630000 },
                { label: "Đất", y: 2496200 },
                { label: "Thuế BVMT", y: 917150 },
                { label: "Phí, lệ phí", y: 2230000 },
                { label: "Cấp quyền KTKS", y: 636656 },
                { label: "XSKT", y: 58000 },
                { label: "Thu tại xã", y: 20000 },
                { label: "Cổ tức, LNST", y: null },
                { label: "Thu khác NS", y: 132107 },
                { label: "Hải quan", y: null },
                { label: "BS CÓ MT TỪ NSTƯ", y: 982170 },   

            ]
        }],
       
        "Dự toán thu chi tiết": [{
            color: "#07648a",
            name: "Ngân sách nhà nước",
            type: "column",
            dataPoints: [
            
                { label: "XN quốc doanh", y: 13919567 },
                { label: "XN vốn ĐT NN", y: 2315233 },
                { label: "NQD", y: 2668000 },
                { label: "Thuế TNCN", y: 672000 },
                { label: "Lệ phí TB", y: 630000 },
                { label: "Đất", y: 2496200 },
                { label: "Thuế BVMT", y: 2401000 },
                { label: "Phí, lệ phí", y: 2640000 },
                { label: "Cấp quyền KTKS", y: 2060000 },
                { label: "XSKT", y: 58000 },
                { label: "Thu tại xã", y: 20000 },
                { label: "Cổ tức, LNST", y: 80000 },
                { label: "Thu khác NS", y: 400000 },
                { label: "Hải quan", y: 6000000 },
                { label: "BS CÓ MT TỪ NSTƯ", y: null },    

            ]
        },
        {
            color: "#08aef3",
            name: "Ngân sách địa phương",
            type: "column",
            dataPoints: [
               
                { label: "XN quốc doanh", y: 11484943.55 },
                { label: "XN vốn ĐT NN", y: 1579358 },
                { label: "NQD", y: 1768500 },
                { label: "Thuế TNCN", y: 436800 },
                { label: "Lệ phí TB", y: 630000 },
                { label: "Đất", y: 2496200 },
                { label: "Thuế BVMT", y: 917150 },
                { label: "Phí, lệ phí", y: 2230000 },
                { label: "Cấp quyền KTKS", y: 636656 },
                { label: "XSKT", y: 58000 },
                { label: "Thu tại xã", y: 20000 },
                { label: "Cổ tức, LNST", y: null },
                { label: "Thu khác NS", y: 132107 },
                { label: "Hải quan", y: null },
                { label: "BS CÓ MT TỪ NSTƯ", y: 982170 },   

            ]        
        }],
    };
    var newVSReturningVisitorsOptions = {
        animationEnabled: true,
        backgroundColor: "transparent",
        // theme: "light2",
        title: {
            text: "Dự toán thu (triệu đồng)",
            fontFamily: "tahoma",
            fontSize: 16,
            fontColor: "#000"
        },
        subtitles: [{
            text: "Chọn Chỉ tiêu để xem Dự toán thu chi tiết",
            backgroundColor: "#beeafb",
            fontFamily: "tahoma",
            fontSize: 16,
            fontColor: "#000",
            padding: 5
        }],
        toolTip: {
            backgroundColor: "#fff",
            fontColor: "#000",
            shared: true
            //   Content: "{x} : {y}"
        },

        legend: {
            fontFamily: "tahoma",
            fontSize: 14,
            fontColor: "#000",
            itemTextFormatter: function (e) {
                return e.dataPoint.name + ": " + Math.round(e.dataPoint.y / totalVisitors * 100) + "%";
            }
        },
        data: []
    };

    var visitorsDrilldownedChartOptions = {
        animationEnabled: true,
        backgroundColor: "transparent",
        title: {
            fontFamily: "tahoma",
            fontSize: 16,
            fontColor: "#000"
        },
        toolTip: {
            backgroundColor: "#fff",
            fontColor: "#000",
            shared: true
            //   Content: "{x} : {y}"
        },
        axisX: {
            labelFontColor: "#000",
            lineColor: "#a2a2a2",
            tickColor: "#a2a2a2"
        },
        axisY: {
            gridThickness: 0,
            includeZero: false,
            labelFontColor: "#000",
            lineColor: "#a2a2a2",
            tickColor: "#a2a2a2",
            lineThickness: 1
        },
        data: []
    };
    var chart = new CanvasJS.Chart("chartThuChiDT", newVSReturningVisitorsOptions);
    chart.options.data = visitorsData["Dự toán thu"];
    chart.render();
    var chart1 = new CanvasJS.Chart("chartThuChiDT1", visitorsDrilldownedChartOptions);
    chart1.options.data = visitorsData["Dự toán thu chi tiết"];
    chart1.render();

    function visitorsChartDrilldownHandler(e) {
        chart1 = new CanvasJS.Chart("chartThuChiDT1", visitorsDrilldownedChartOptions);
        chart1.options.data = visitorsData[e.dataPoint.name];
        chart1.options.title = {
            text: e.dataPoint.name,
            fontFamily: "tahoma",
            fontSize: 16,
            fontColor: "#000"
        }
        chart1.render();
        var hClass = $(backButton).hasClass('invisible');
        if (hClass) {
            $("#backButton").toggleClass("invisible");
        }

    }

    $("#backButton").click(function () {
        $(this).toggleClass("invisible");
        chart2 = new CanvasJS.Chart("chartThuChiDT1", visitorsDrilldownedChartOptions);
        chart2.options.data = visitorsData["Dự toán thu chi tiết"];
        chart2.options.title = {
            text: "Dự toán thu chi tiết",
            fontFamily: "tahoma",
            fontSize: 16,
            fontColor: "#000"
        }
        chart2.render();
    });
    var chartDonthu = new CanvasJS.Chart("chartDonthu", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tình hình giải quyết đơn, thư khiếu nại theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Đơn, thư",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],

        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Đã giải quyết",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Tháng 1", y: 300 },
                { label: "Tháng 2", y: 204 },
                { label: "Tháng 3", y: 450 },
                { label: "Tháng 4", y: 390 },
                { label: "Tháng 5", y: 171 },
                { label: "Tháng 6", y: 153 },
                { label: "Tháng 7", y: 139 },
                { label: "Tháng 8", y: 171 },
                { label: "Tháng 9", y: 239 },
                { label: "Tháng 10", y: 210 }
            ]
        },
        {
            type: "column",
            name: "Trễ hạn xử lý",
            color: "#FF6500",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Tháng 1", y: 20 },
                { label: "Tháng 2", y: 24 },
                { label: "Tháng 3", y: 25 },
                { label: "Tháng 4", y: 19 },
                { label: "Tháng 5", y: 17 },
                { label: "Tháng 6", y: 15 },
                { label: "Tháng 7", y: 13 },
                { label: "Tháng 8", y: 21 },
                { label: "Tháng 9", y: 23 },
                { label: "Tháng 10", y: 21 }
            ]

        }]
    });
    chartDonthu.render();
    var chartTiendo = new CanvasJS.Chart("chartTiendo", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"

        animationEnabled: true,
        title: {
            text: "Tiến độ giải quyết khiếu nại trong tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 21, label: "Đã trả lời", color: "#008ec7" },
                { y: 10, label: "Đang xử lý", color: "#62c6ef" },
                { y: 6, label: "Chờ tiếp nhận", color: "#046086" },


            ]
        }]
    });
    chartTiendo.render();
});
