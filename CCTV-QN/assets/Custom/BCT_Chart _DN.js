﻿$(document).ready(function () {
    var chartCCDIEN2 = new CanvasJS.Chart("chartCCDIEN2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Sản lượng phát điện",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 36, label: "Thủy điện", color: "#f9b042" },
                { y: 36, label: "Than đốt", color: "#f79706" },
                { y: 1, label: "Dầu và Diesel", color: "#8e5a0c" },
                { y: 25, label: "Ga/Các khí tổng hợp", color: "#f1cd97" },
                { y: 2, label: "Nhập khẩu từ Lào- Trung quốc", color: "#f1cd97" }

            ]
        }]
    });
    chartCCDIEN2.render();
    var chartSXDIEN2 = new CanvasJS.Chart("chartSXDIEN2", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        animationEnabled: true,
        title: {
            text: "Sản xuất và mua bán điện",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
            fontFamily: "arial black",
            fontColor: "#695A42"
        },
        axisX: {
            interval: 1,
            intervalType: "year"
        },
        axisY: {
            valueFormatString: "#0 Triệu kWh",
            gridColor: "#B6B1A8",
            tickColor: "#B6B1A8"
        },
        toolTip: {
            shared: true,
            content: toolTipContentEVN
        },
        data: [{
            type: "stackedColumn",
            showInLegend: true,
            color: "#f9b042",
            name: "sản xuất",
            dataPoints: [              
                { y: 48000, x: new Date(2011, 0) },
                { y: 51000, x: new Date(2012, 0) },
                { y: 55000, x: new Date(2013, 0) },
                { y: 60000, x: new Date(2014, 0) },
                { y: 62000, x: new Date(2015, 0) },
                { y: 81000, x: new Date(2016, 0) }
            ]
        },
        
        {
            type: "stackedColumn",
            showInLegend: true,
            name: "Mua bán",
            color: "#695A42",
            dataPoints: [               
                { y: 86000, x: new Date(2011, 0) },
                { y: 99000, x: new Date(2012, 0) },
                { y: 112000, x: new Date(2013, 0) },
                { y: 129000, x: new Date(2014, 0) },
                { y: 160000, x: new Date(2015, 0) },
                { y: 175000, x: new Date(2016, 0) }
            ]
        
        }]
    });
    chartSXDIEN2.render();

    function toolTipContentEVN(e) {
        var str = "";
        var total = 0;
        var str2, str3;
        for (var i = 0; i < e.entries.length; i++) {
            var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\"> " + e.entries[i].dataSeries.name + "</span>: <strong>" + e.entries[i].dataPoint.y + "</strong> Triệu kWh<br/>";
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
        }
        str2 = "<span style = \"color:DodgerBlue;\"><strong>" + (e.entries[0].dataPoint.x).getFullYear() + "</strong></span><br/>";
        total = Math.round(total * 100) / 100;
        str3 = "<span style = \"color:Tomato\">Total:</span><strong> " + total + "</strong> Triệu kWh<br/>";
        return (str2.concat(str)).concat(str3);
    }
    var chartCCDIEN = new CanvasJS.Chart("chartCCDIEN", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        title: {
            text: "Cơ cấu đầu tư phát triển",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pyramid",
            yValueFormatString: "#' Tỷ đồng'",
            indexLabelFontColor: "black",
            indexLabelFontSize: 16,
            indexLabel: "{label} - {y}",
            //reversed: true, // Reverses the pyramid
            dataPoints: [
                { y: 42.85, label: "Vốn ngân sách" },
                { y: 29.79, label: "Vốn vay" },
                { y: 20.51, label: "Nguồn vốn khác " }
              
            ]
        }]
    });
    chartCCDIEN.render();
    var chartCCDIEN1 = new CanvasJS.Chart("chartCCDIEN1", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        title: {
            text: "Cơ cấu đầu tư phát triển",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pyramid",
            yValueFormatString: "#' tỷ đồng'",
            indexLabelFontColor: "black",
            indexLabelFontSize: 16,
            indexLabel: "{label} - {y}",
            //reversed: true, // Reverses the pyramid
            dataPoints: [
                { y: 462, label: "Nguồn vốn khác " },
             { y: 50, label: "Vốn ngân sách" },
                 { y: 134, label: "Vốn vay" }

            ]
        }]
    });
    chartCCDIEN1.render();
    var chartSXDIEN = new CanvasJS.Chart("chartSXDIEN", {
        theme: "light2",
        animationEnabled: true,
        title: {
            text: "Tiến đô thực hiện cá dự án trọng điểm",
            horizontalAlign: "left",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} (#percent%)",
            dataPoints: [
                { y: 53, label: "Dự án đã hoàn thành" },
                { y: 40, label: "Dự án đang thực hiện" },
                { y: 7, label: "Dự án trễ tiến độ" },
            ]
        }]
    });
    chartSXDIEN.render();
    var chartSXDIEN1 = new CanvasJS.Chart("chartSXDIEN1", {
        theme: "light2",
        animationEnabled: true,
        title: {
            text: "Tiến đô thực hiện cá dự án trọng điểm",
            horizontalAlign: "left",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} (#percent%)",
            dataPoints: [
                { y: 40, label: "Dự án đã hoàn thành" },
                { y: 50, label: "Dự án đang thực hiện" },
                { y: 10, label: "Dự án trễ tiến độ" },
            ]
        }]
    });
    chartSXDIEN1.render();
    var chartDN_SX = new CanvasJS.Chart("chartDN_SX", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",        
        title: {
            text: "Giá trị sản xuất công nghiệp qua các năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            title: "Giá trị sản xuất (Tr. đồng)"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "spline",
            name: "CĐ 1994",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2010", y: 13773037 },
                { label: "Năm 2011", y: 14837388 },
                { label: "Năm 2012", y: 15655750 },
                { label: "Năm 2013", y: 16237792 }
               
            ]
        },
        {
            type: "spline",
            name: "Giá thực tế",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2010", y: 28199227 },
                { label: "Năm 2011", y: 38641709 },
                { label: "Năm 2012", y: 40595956 },
                { label: "Năm 2013", y: 41499218 }
            ]
        },
        {
            type: "spline",
            name: "Giá SS 2010",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2010", y: 28199227 },
                { label: "Năm 2011", y: 33636567 },
                { label: "Năm 2012", y: 35043773 },
                { label: "Năm 2013", y: 36597154 }
            ]      
        }]
    });

    chartDN_SX.render();

    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartDN_SX.render();
    }
    var chartDN_KH = new CanvasJS.Chart("chartDN_KH", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "KẾ HOẠCH NĂM",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
       
        axisX: {
            interval: 1
        },
        axisY: {
            title: "Tr. Đồng",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y} Tr.đồng",
            dataPoints: [
                { label: "Doanh thu", y: 44363881 },
                { label: "Giá trị SX CN (CĐ 1994)", y: 17287420 },
                { label: "Giá trị SX CN theo giá Thực tế", y: 41948200 },
                { label: "Giá trị SX CN theo giá SS 2010", y: 38080165 }               
            ]
        }]
    });
    chartDN_KH.render();
    var chartDN_SX2 = new CanvasJS.Chart("chartDN_SX2", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        title: {
            text: "Các chỉ tiêu sản phẩm chủ yếu",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            title: "Giá trị (tấn)"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries2
        },
        data: [{
            type: "spline",
            name: "Than sạch sản xuất",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2013", y: 35626 },
                { label: "Năm 2014", y: 36292 },
                { label: "Năm 2015", y: 35889 },
                { label: "Năm 2016", y: 32700 }

            ]
        },
        {
            type: "spline",
            name: "Khoáng sản Kẽm thỏi",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2013", y: 8300 },
                { label: "Năm 2014", y: 9333 },
                { label: "Năm 2015", y: 10540 },
                { label: "Năm 2016", y: 10000 }
            ]
        },
        {
            type: "spline",
            name: "Khoáng sản Đồng tấm",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2013", y: 9651 },
                { label: "Năm 2014", y: 10507 },
                { label: "Năm 2015", y: 11316 },
                { label: "Năm 2016", y: 11500 }
            ]
        }]
    });

    chartDN_SX2.render();

    function toggleDataSeries2(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartDN_SX2.render();
    }
    var chartDN_SX3 = new CanvasJS.Chart("chartDN_SX3", {
        animationEnabled: true,
        exportEnabled: true,
        theme: "light2",
        title: {
            text: "Chỉ số tổn thất điện năng theo năm",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            title: "%"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries3
        },
        data: [{
            type: "spline",
            name: "Tổng tổn thất điện năng",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2011", y: 9.23 },
                { label: "Năm 2012", y: 8.85 },
                { label: "Năm 2013", y: 8.87 },
                { label: "Năm 2014", y: 8.6 },
                { label: "Năm 2015", y: 7.94 },
                { label: "Năm 2016", y: 7.57 }

            ]
        },
        {
            type: "spline",
            name: "Tổn thất do truyền tải điện",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2011", y: 2.56 },
                { label: "Năm 2012", y: 2.33 },
                { label: "Năm 2013", y: 2.69 },
                { label: "Năm 2014", y: 2.49},
                { label: "Năm 2015", y: 2.34 },
                { label: "Năm 2016", y: 2.39 }
            ]
        },
        {
            type: "spline",
            name: "Tổn thất do phân phối",
            showInLegend: true,
            dataPoints: [
                { label: "Năm 2011", y: 6.8},
                { label: "Năm 2012", y: 6.64 },
                { label: "Năm 2013", y: 6.31 },
                { label: "Năm 2014", y: 6.17 },
                { label: "Năm 2015", y: 5.6 },
                { label: "Năm 2016", y: 5.21 }
            ]
        }]
    });

    chartDN_SX3.render();

    function toggleDataSeries3(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartDN_SX3.render();
    }
    var chartDN_KH2 = new CanvasJS.Chart("chartDN_KH2", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "KẾ HOẠCH NĂM",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },

        axisX: {
            interval: 1
        },
        axisY: {
            title: "Tỷ. Đồng",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y} Tỷ.đồng",
            dataPoints: [
                { label: "Doanh thu", y: 112700 },
                { label: "Lợi nhuận trước thuế", y: 500 },
                { label: "Nộp ngân sách", y: 13500 },
                { label: "Kế hoạch đầu tư phát triển", y: 21518 }
            ]
        }]
    });
    chartDN_KH2.render();
    var chartDN_KH3 = new CanvasJS.Chart("chartDN_KH3", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "DOANH SỐ BÁN HÀNG THEO NHÓM KHÁCH HÀNG",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },

        axisX: {
            interval: 1
        },
        axisY: {
            title: "%",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y} %",
            dataPoints: [
                { label: "Nông, lâm nghiệp và NT TS", y: 2 },
                { label: "Công nghiệp và xây dựng", y: 54 },
                { label: "Thương mại và Khách sạn, ngân hàng", y: 6 },
                { label: "Quản lý, khu dân cư", y: 34 },
                { label: "Khác", y: 4 }
            ]
        }]
    });
    chartDN_KH3.render();
    $('#BieudoCty1').show();
    $('#Bieudo2Cty1').show();
    $('#BieudoCty2').hide();
    $('#Bieudo2Cty2').hide();
    $('#BieudoCty3').hide();
    $('#Bieudo2Cty3').hide();
    $('#bcpetro').hide();
    $('#nvpetro').hide();
    $('#baocaoDN1').show();
    $('#baocaoDN2').hide();
    $('#baocaoDN3').hide();
    $('#NhiemvuDN1').show();
    $('#NhiemvuDN2').hide();
    $('#NhiemvuDN3').hide();
})
//báo cáo Doanh nghiệp

$('#congty1').click(function () {
    $('#BieudoCty1').show();
    $('#Bieudo2Cty1').show();
    $('#BieudoCty2').hide();
    $('#Bieudo2Cty2').hide();
    $('#BieudoCty3').hide();
    $('#Bieudo2Cty3').hide();
    $('#bcvinachem').show();
    $('#bcpetro').hide();
    $('#nvvinamchem').show();
    $('#nvpetro').hide();
    $('#baocaoDN1').show();
    $('#baocaoDN2').hide();
    $('#baocaoDN3').hide();
    $('#NhiemvuDN1').show();
    $('#NhiemvuDN2').hide();
    $('#NhiemvuDN3').hide();
})
$('#congty2').click(function () {
    $('#BieudoCty1').hide();
    $('#Bieudo2Cty1').hide();

    $('#BieudoCty2').show();
    $('#Bieudo2Cty2').show();
    $('#BieudoCty3').hide();
    $('#Bieudo2Cty3').hide();
    $('#bcvinachem').hide();
    $('#bcpetro').show();
    $('#nvvinamchem').hide();
    $('#nvpetro').show();
    $('#baocaoDN2').show();
    $('#baocaoDN1').hide();
    $('#baocaoDN3').hide();
    $('#NhiemvuDN2').show();
    $('#NhiemvuDN1').hide();
    $('#NhiemvuDN3').hide();
})
$('#congty3').click(function () {
    $('#BieudoCty1').hide();
    $('#Bieudo2Cty1').hide();
    $('#BieudoCty2').hide();
    $('#Bieudo2Cty2').hide();
    $('#BieudoCty3').show();
    $('#Bieudo2Cty3').show();
    $('#bcvinachem').hide();
    $('#bcpetro').show();
    $('#nvvinamchem').hide();
    $('#nvpetro').show();
    $('#baocaoDN3').show();
    $('#baocaoDN1').hide();
    $('#baocaoDN2').hide();
    $('#NhiemvuDN3').show();
    $('#NhiemvuDN1').hide();
    $('#NhiemvuDN2').hide();
})