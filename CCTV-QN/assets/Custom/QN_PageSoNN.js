﻿$(document).ready(function () {
    var dataPoints1 = [];
    var dataPoints2 = [];

    var chartMXH = new CanvasJS.Chart("chartMXH", {
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: "Chỉ số mạng xã hội",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisX: {
            title: ""
        },
        axisY: {
            prefix: "",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            verticalAlign: "top",
            fontSize: 22,
            fontColor: "dimGrey",
            itemclick: toggleDataSeriesMXH
        },
        data: [{
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            xValueFormatString: "hh:mm:ss TT",
            showInLegend: true,
            name: "Facebook",
            dataPoints: dataPoints1
        },
        {
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            showInLegend: true,
            name: "Twitter",
            dataPoints: dataPoints2
        }]
    });

    function toggleDataSeriesMXH(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartMXH.render();
    }

    var updateInterval = 3000;
    // initial value
    var yValue1 = 600;
    var yValue2 = 605;

    var time = new Date;
    // starting at 9.30 am
    time.setHours(9);
    time.setMinutes(30);
    time.setSeconds(0);
    time.setMilliseconds(0);

    function updateChart(count) {
        count = count || 1;
        var deltaY1, deltaY2;
        for (var i = 0; i < count; i++) {
            time.setTime(time.getTime() + updateInterval);
            deltaY1 = .5 + Math.random() * (-.5 - .5);
            deltaY2 = .5 + Math.random() * (-.5 - .5);

            // adding random value and rounding it to two digits.
            yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
            yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

            // pushing the new values
            dataPoints1.push({
                x: time.getTime(),
                y: yValue1
            });
            dataPoints2.push({
                x: time.getTime(),
                y: yValue2
            });
        }

        // updating legend text with  updated with y Value
        chartMXH.options.data[0].legendText = " Facebook " + yValue1;
        chartMXH.options.data[1].legendText = " Twitter " + yValue2;
        chartMXH.render();
    }
    // generates first set of dataPoints
    updateChart(100);
    setInterval(function () { updateChart() }, updateInterval);
    var chartVBPL1 = new CanvasJS.Chart("chartVBPL1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Chỉ số xây dựng VB QPPL 2016",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 21.88, label: "Nghi định", color: "#c70000", },
                { y: 9.37, label: "Quyết định" },
                { y: 67.19, label: "Thông tư", color: "#f1f706" },
                { y: 1.56, label: "VB chưa trình/ban hành", color: "#06eefb" },

            ]
        }]
    });
    chartVBPL1.render();
    
    var chartVBPL = new CanvasJS.Chart("chartVBPL", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tình hình giải quyết hồ sơ về TTHC",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Hồ sơ",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
      
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Hồ sơ đúng hạn",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Tháng 1", y: 300 },
                { label: "Tháng 2", y: 204 },
                { label: "Tháng 3", y: 450 },
                { label: "Tháng 4", y: 390 },
                { label: "Tháng 5", y: 171 },
                { label: "Tháng 6", y: 153 },
                { label: "Tháng 7", y: 139 },
                { label: "Tháng 8", y: 171 },
                { label: "Tháng 9", y: 239 },
                { label: "Tháng 10", y: 210 }
            ]
        },
        {
            type: "column",
            name: "Trễ hạn xử lý",
            color: "#FF6500",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Tháng 1", y: 20 },
                { label: "Tháng 2", y: 24 },
                { label: "Tháng 3", y: 25 },
                { label: "Tháng 4", y: 19 },
                { label: "Tháng 5", y: 17 },
                { label: "Tháng 6", y: 15 },
                { label: "Tháng 7", y: 13 },
                { label: "Tháng 8", y: 21 },
                { label: "Tháng 9", y: 23 },
                { label: "Tháng 10", y: 21 }
            ]
        
        }]
    });
    chartVBPL.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
    $("#HTPage111").height($("#HTPage2").height());
   
});
var stateSliderNow = true;
var _stateSliderNow = 1; 
$('#NumberPage1').click(function () {
    if (!stateSliderNow || _stateSliderNow === 0) return;
    _stateSliderNow = 0;
    stateSliderNow = false;
    $('#NumberPage1').toggleClass('mauChu');
    $('#NumberPage2').toggleClass('mauChu');
    $('#HTPage1').toggleClass('hide');
    $("#HTPage111").height($("#HTPage1").height());
    $('#HTPage2').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage2').toggleClass('hide');
        $('#HTPage2').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)
   

    var chartDichbenh = new CanvasJS.Chart("chartDichbenh", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Thống kê tình hình tiêm phòng, dịch bệnh trong chăn nuôi theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },

        axisX: {
            title: "Tháng"
        },
        axisY: {
            title: "mắc bệnh mới"
        },
        data: [{
            type: "bubble",
            legendMarkerType: "circle",
            toolTipContent: "<span style=\"color:#4F81BC \"><b>{name}</b></span><br/><b> Tháng:</b> {x} <br/><b> Mắc bệnh LMLM:</b> {z} con<br/><b> Đã tiêm phòng:</b></span> {y} con",
            name: "Trâu bò ",
            showInLegend: true,
            color: "#f1a129",
            dataPoints: [
                { x: 1, z: 23, y: 330, },
                { x: 2, z: 28, y: 390 },
                { x: 3, z: 39, y: 400 },
                { x: 4, z: 34, y: 430 },
                { x: 5, z: 24, y: 321 },
                { x: 6, z: 29, y: 250 },
                { x: 7, z: 29, y: 370 },
            ]
        },
        {
            type: "bubble",
            legendMarkerType: "circle",
            name: "Gia cầm",
            showInLegend: true,
            color: "#65c4f3",
            toolTipContent: "<span style=\"color:#C0504E \"><b>{name}</b></span><br/><b> Tháng:</b> {x} <br/><b> Mắc bệnh cúm GC:</b> {z} con<br/><b> Đã tiêm phòng:</b></span> {y} người",
            dataPoints: [
                { x: 1, z: 19, y: 200 },
                { x: 2, z: 27, y: 300 },
                { x: 3, z: 35, y: 330 },
                { x: 4, z: 32, y: 190 },
                { x: 5, z: 29, y: 189 },
                { x: 6, z: 22, y: 150 },
                { x: 7, z: 27, y: 200 },
            ]
        }]
    });
    chartDichbenh.render();
    var chartYT = new CanvasJS.Chart("chartYT", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tình hình Quản lý thị trường ở địa phương",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Số vụ",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
        axisY2: {
            title: "Tỷ đồng",
            lineColor: "#fb9c3a",
            tickColor: "#fb9c3a",
            labelFontColor: "#fb9c3a",
            titleFontColor: "#fb9c3a",

        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Số vụ kiểm tra",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "TP. HCM", y: 3 },
                { label: "Cẩm Phả", y: 104 },
                { label: "Móng Cái", y: 8 },
                { label: "Vân Đồn", y: 39 },
                { label: "Đầm Hà", y: 171 },
                { label: "Tiên Yên", y: 53 }
            ]
        },
        {
            type: "column",
            name: "Số vụ vi phạm",
            color: "#72c6f1",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "TP. HCM", y: 0 },
                { label: "Cẩm Phả", y: 3 },
                { label: "Móng Cái", y: 0 },
                { label: "Vân Đồn", y: 1 },
                { label: "Đầm Hà", y: 2 },
                { label: "Tiên Yên", y: 0 }
            ]
        },
        {
            type: "column",
            name: "Thu ngân sách",
            color: "#fb9c3a",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { label: "TP. HCM", y: 3 },
                { label: "Cẩm Phả", y: 6 },
                { label: "Móng Cái", y: 1 },
                { label: "Vân Đồn", y: 2 },
                { label: "Đầm Hà", y: 5 },
                { label: "Tiên Yên", y: 1 }
            ]
        }]
    });
    chartYT.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
    var chartDautu = new CanvasJS.Chart("chartDautu", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Nộp ngân sách, GT hàng tiêu hủy theo năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {
            interval: 1,
            intervalType: "year"
        },
        axisY: {
            title: "Tỷ đồng",
            valueFormatString: "#,000",
            gridColor: "#dedbd7",
            tickColor: "#dedbd7"
        },
        toolTip: {
            shared: true,
            content: toolTipContentDautu
        },
        data: [{
            type: "stackedColumn",
            showInLegend: true,
            color: "#0e9ce2",
            name: "Nộp ngân sách NN",
            dataPoints: [
                { y: 387.9, label: "2014" },
                { y: 459.8, label: "2015" },
                { y: 349.8, label: "2016" },
                { y: 279.8, label: "2017" },
                { y: 370, label: "Ước 2018" }
            ]
        },
        {
            type: "stackedColumn",
            showInLegend: true,
            name: "GT hàng tiêu hủy",
            color: "#98d8f7",
            dataPoints: [
                { y: 187.9, label: "2014" },
                { y: 159.8, label: "2015" },
                { y: 149.8, label: "2016" },
                { y: 179.8, label: "2017" },
                { y: 170, label: "Ước 2018" }
            ]
        }]
    });
    chartDautu.render();

    function toolTipContentDautu(e) {
        var str = "";
        var total = 0;
        var str2, str3;
        for (var i = 0; i < e.entries.length; i++) {
            var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\"> " + e.entries[i].dataSeries.name + "</span>: $<strong>" + e.entries[i].dataPoint.y + "</strong>bn<br/>";
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
        }
        str2 = "<span style = \"color:DodgerBlue;\"><strong>" + (e.entries[0].dataPoint.x).getFullYear() + "</strong></span><br/>";
        total = Math.round(total * 100) / 100;
        str3 = "<span style = \"color:Tomato\">Total:</span><strong> $" + total + "</strong>bn<br/>";
        return (str2.concat(str)).concat(str3);
    }

    var chartDautuNN = new CanvasJS.Chart("chartDautuNN", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Vốn đầu tư phân theo nguồn vốn",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Triệu đồng",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "line",
            showInLegend: true,
            name: "KV Nhà nước",
            markerType: "square",
            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 3230768 },
                { label: "2013", y: 3524822 },
                { label: "2014", y: 4956082 },
                { label: "2015", y: 3305064 },
                { label: "2016", y: 3308432 }

            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "KV ngoài Nhà nước",
            //lineDashType: "dash",
            color: "#98d8f7",
            dataPoints: [
                { label: "2010", y: 2976488 },
                { label: "2013", y: 4432259 },
                { label: "2014", y: 4418650 },
                { label: "2015", y: 5068851 },
                { label: "2016", y: 6212677 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Nước ngoài trực tiếp ĐT",
            //  lineDashType: "dash",
            color: "#5bec75",
            dataPoints: [
                { label: "2010", y: 93981 },
                { label: "2013", y: 150707 },
                { label: "2014", y: 157096 },
                { label: "2015", y: 250612 },
                { label: "2016", y: 421967 }
            ]
        }]
    });
    chartDautuNN.render();

    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "light2",
        animationEnabled: true,

        title: {
            text: "Dự án đầu tư trực tiếp của nước ngoài được cấp phép",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} dự án (#percent%)",
            dataPoints: [
                { y: 4, label: "Khác ", color: "#0a94cc" },
                { y: 3, label: "Đài Loan", color: "#1fbaf9" },
                { y: 4, label: "Ấn Độ ", color: "#4fc5f5" },
                { y: 7, label: "Trung Quốc", color: "#5aa8c7" },
                { y: 4, label: "Hàn Quốc", color: "#bfdbe6" },
            ]
        }]
    });
    chartDautuCong.render();

    //Tai nạn giao thông
    var chartATGT = new CanvasJS.Chart("chartATGT", {
        theme: "light2",
        title: {
            text: "Thống kê an toàn thực phẩm theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY:[ {
            title: "Số vụ kiểm tra",
            lineColor: "#f1b656",
            tickColor: "#f1b656",
            labelFontColor: "#f1b656",
            titleFontColor: "#f1b656",


        }, {
                title: "Số vụ vi phạm",
                lineColor: "#cc56f1",
                tickColor: "#cc56f1",
                labelFontColor: "#cc56f1",
                titleFontColor: "#cc56f1",
            }],
        axisY2: [
        {
            title: "Thu ngân sách",
            lineColor: "#30fbfd",
            tickColor: "#30fbfd",
            labelFontColor: "#30fbfd",
            titleFontColor: "#30fbfd",

        }],
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATGT
        },
        data: [{
            type: "line",
            name: "Số vụ kiểm tra",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 50 },
                { x: 2, y: 20 },
                { x: 3, y: 30 },
                { x: 4, y: 25 },
                { x: 5, y: 28 },
                { x: 6, y: 15 },
                { x: 7, y: 30 },
                { x: 8, y: 31 },

            ]
        },
        {
            type: "line",
            name: "Số vụ vi phạm",
            color: "#30fbfd",
            axisYIndex: 1,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 45 },
                { x: 2, y: 24 },
                { x: 3, y: 40 },
                { x: 4, y: 30 },
                { x: 5, y: 35 },
                { x: 6, y: 18 },
                { x: 7, y: 31 },
                { x: 8, y: 29 },

            ]
        },
        {
            type: "line",
            name: "Thu ngân sách",
            color: "#cc56f1",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42 },
                { x: 2, y: 15 },
                { x: 3, y: 28 },
                { x: 4, y: 22 },
                { x: 5, y: 25 },
                { x: 6, y: 16 },
                { x: 7, y: 33 },
                { x: 8, y: 20 },

            ]
        }]
    });
    chartATGT.render();

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    var chartHanhKhach = new CanvasJS.Chart("chartHanhKhach", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Tình hình kiểm tra ATTP theo năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "vụ",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "column",
            showInLegend: true,

            name: "Số vụ kiểm tra",
            //  markerType: "square",

            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 220 },
                { label: "2011", y: 200 },
                { label: "2012", y: 270 },
                { label: "2013", y: 400 },
                { label: "2014", y: 370 },
                { label: "2015", y: 450 },
                { label: "2016", y: 330 },
                { label: "2017", y: 390 },


            ]
        },
        {
            type: "column",
            showInLegend: true,
            name: "Số vụ vi phạm",
            // lineDashType: "dash",
            color: "#ea9407",
            dataPoints: [
                { label: "2010", y: 120 },
                { label: "2011", y: 145 },
                { label: "2012", y: 165 },
                { label: "2013", y: 176 },
                { label: "2014", y: 189 },
                { label: "2015", y: 197 },
                { label: "2016", y: 211 },
                { label: "2017", y: 250 },
            ]
        }]
    });
    chartHanhKhach.render();  
    var chartATTP_TK = new CanvasJS.Chart("chartATTP_TK", {
        animationEnabled: true,
        title: {
            text: ""
        },
        axisX: {
            interval: 1
        },
        axisY: {
            title: "",

        },
        data: [{
            type: "bar",
            toolTipContent: "<b>{label}</b><br>: {y}",
            dataPoints: [
                { label: "Cấp mới Giấy chứng nhận ATTP", y: 178 },
                { label: "Cấp lại giấy chứng nhận ATTP", y: 228 },
                { label: "Cơ sở được cấp giấy chứng nhận ATTP", y: 128 },
                { label: "Cơ sở vi phạm phạt hành chính", y: 24 },
                { label: "Cơ sở vi phạm bị thu hồi giấy chứng nhận", y: 8 }

            ]
        }]
    });
    chartATTP_TK.render();
    var chartQLTT = new CanvasJS.Chart("chartQLTT", {
        theme: "light2",
        title: {
            text: "Thống kê tình hình xuất nhập khẩu theo mặt hàng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        axisY: {
            title: "Xuất khẩu",
            suffix: "nghìn USD",
            lineColor: "#4F81BC",
            tickColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            gridThickness: 0
        },
        axisY2: {
            title: "Nhập khẩu",
            suffix: "nghìn USD",
            gridThickness: 0,
            lineColor: "#C0504E",
            tickColor: "#C0504E",
            labelFontColor: "#C0504E"
        },
        data: [{
            type: "column",
            name: "Xuất khẩu",
            legendText: "Xuất khẩu",
            yValueFormatString: "0.##' nghìn USD'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            dataPoints: [
                { label: "Lúa gạo", y: 4663, color: "#d6d2ce" },
                { label: "Thức ăn chăn nuôi", y: 367, color: "#8e8984" },
                { label: "Phân bón", y: 367, color: "#f1be8c" },
                { label: "Gõ và SP gỗ", y: 2342, color: "#eca056" },
                { label: "Rau, quả", y: 367, color: "#f9922d" },              
                { label: "Gia cầm", y: 1722, color: "#ef7a08" },
                { label: "Gia súc", y: 1482, color: "#cc6807" },
                { label: "Cà phê", y: 567, color: "#9a5614" },
                { label: "Khác", y: 367, color: "#dc9208" },
            ]
        },

        {
            type: "line",
            name: "Nhập khẩu",
            legendText: "Nhập khẩu",
            axisYType: "secondary",
            showInLegend: true,
            yValueFormatString: "0.##' nghìn USD'",
            indexLabel: "Nhập khẩu",
            indexLabelFontColor: "#1494ef",
            dataPoints: [
                { label: "Lúa gạo", y: 3663, color: "#d6d2ce" },
                { label: "Thức ăn chăn nuôi", y: 567, color: "#8e8984" },
                { label: "Phân bón", y: 467, color: "#f1be8c" },
                { label: "Gõ và SP gỗ", y: 1342, color: "#eca056" },
                { label: "Rau, quả", y: 1367, color: "#f9922d" },
                { label: "Gia cầm", y: 1922, color: "#ef7a08" },
                { label: "Gia súc", y: 1512, color: "#cc6807" },
                { label: "Cà phê", y: 367, color: "#9a5614" },
                { label: "Khác", y: 412, color: "#dc9208" },

            ]
        }]
    });
    chartQLTT.render();
    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartQLTT.render();
    }
    var chartLN = new CanvasJS.Chart("chartLN", {
        theme: "light2",
        title: {
            text: "Xu hướng trồng mới và sản lượng khai thác lâm nghiệp",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesLN
        },
        axisY: {
            title: "Diện tích trồng mới",
            suffix: "nghìn ha",
            lineColor: "#0783ce",
            tickColor: "#0783ce",
            labelFontColor: "#0783ce",
            gridThickness: 0
        },
        axisY2: {
            title: "Sản lượng khai thác",
            suffix: "nghìn m3",
            gridThickness: 0,
            lineColor: "#eca056",
            tickColor: "#eca056",
            labelFontColor: "#eca056"
        },
        data: [{
            type: "line",
            name: "Diện tích trồng mới",
            legendText: "Diện tích trồng mới",
            yValueFormatString: "0.##' nghìn ha'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            color: "#0783ce",
            dataPoints: [
                { label: "Năm 6", y: 187},
                { label: "Năm 7", y: 227.1},
                { label: "Năm 8", y: 221},
                { label: "Năm 9", y: 250},
                { label: "Năm 10", y: 233.1}              
            ]
        },

        {
            type: "line",
            name: "Sản lượng khai thác",
            legendText: "Sản lượng khai thác",
            axisYType: "secondary",
            showInLegend: true,
            yValueFormatString: "0.##' nghìn m3'",
            indexLabel: "Sản lượng khai thác",
            indexLabelFontColor: "#eca056",
            color: "#eca056",
            dataPoints: [
                { label: "Năm 6", y: 6655.1 },
                { label: "Năm 7", y: 8711 },
                { label: "Năm 8", y: 9849 },
                { label: "Năm 9", y: 12947},
                { label: "Năm 10", y: 15544 }            
            ]
        }]
    });
    chartLN.render();
    function toggleDataSeriesLN(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartLN.render();
    }
    var chartCCDIEN = new CanvasJS.Chart("chartCCDIEN", {
        animationEnabled: true,      
        theme: "light2",
        title: {
            text: "Cơ cấu sản xuất trong Nông, lâm , thủy sản",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            yValueFormatString: "#'%'",
            indexLabelFontColor: "black",
            indexLabelFontSize: 16,
            indexLabel: "{label} - {y}",
            //reversed: true, // Reverses the pyramid
            dataPoints: [
                { y: 42.85, label: "Chăn nuôi", color: "#06948f" },
                { y: 29.79, label: "Trồng trọt", color: "#09d4cd" },
                { y: 20.51, label: "Nuôi trồng TS", color: "#24f1ea" },
                { y: 5.17, label: "Lâm nghiệp", color: "#8cf7f3" },
                { y: 1.33, label: "Dịch vụ khác", color: "#065653" },
              
            ]
        }]
    });
    chartCCDIEN.render();  
    var chartTrangtrai = new CanvasJS.Chart("chartTrangtrai", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Xu hướng phát triển trang trại",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {
            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Số trang trại",
            crosshair: {
                enabled: true
            }
        },
        data: [{
            type: "area",
            showInLegend: true,
            name: "Trang trại",
            markerType: "square",          
            color: "#077ed6",
            dataPoints: [
                { label: "2010", y: 2257 },
                { label: "2011", y: 63 },
                { label: "2011", y: 141 },
                { label: "2013", y: 212 },
                { label: "2014", y: 316 },
                { label: "2015", y: 329 },
                { label: "2016", y: 319 },
                { label: "2017", y: 627 },
                { label: "2018", y: 651 }

            ]       
        }]
    });
    chartTrangtrai.render();
    var chartSLcay = new CanvasJS.Chart("chartSLcay", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Xu hướng sản lượng qua các năm (triệu tấn)",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {
            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Số trang trại",
            crosshair: {
                enabled: true
            }
        },
        data: [{
            type: "column",
            showInLegend: true,
            name: "SL lúa",
            yValueFormatString: "#0.## triệu tấn",
            color: "#8a4a07",
            dataPoints: [
                { label: "2010", y: 209 },
                { label: "2011", y: 213 },
                { label: "2011", y: 217 },
                { label: "2013", y: 210 },
                { label: "2014", y: 211 },
                { label: "2015", y: 212 },
                { label: "2016", y: 212 },
                { label: "2018", y: 217 }
            ]
        }, {
                type: "column",
                showInLegend: true,
                name: "SL cây công nghiệp",
                yValueFormatString: "#0.## triệu tấn",
                color: "#f57f04",
                dataPoints: [
                    { label: "2010", y: 232.8 },
                    { label: "2011", y: 236.6 },
                    { label: "2011", y: 239.2 },
                    { label: "2013", y: 232.9 },
                    { label: "2014", y: 234 },
                    { label: "2015", y: 235.3 },
                    { label: "2016", y: 235.3 },
                    { label: "2018", y: 239 }
                ]
        }, {
                type: "column",
                showInLegend: true,
                name: "SL hoa màu và cây trồng khác",
                yValueFormatString: "#0.## triệu tấn",
                color: "#fbb165",
                dataPoints: [
                    { label: "2010", y: 229 },
                    { label: "2011", y: 233 },
                    { label: "2011", y: 235 },
                    { label: "2013", y: 230 },
                    { label: "2014", y: 231 },
                    { label: "2015", y: 231},
                    { label: "2016", y: 232 },
                    { label: "2018", y: 229 }
                ]
        }]
    });
    chartSLcay.render();
})
$('#NumberPage2').click(function () {
    if (!stateSliderNow || _stateSliderNow === 1) return;
    _stateSliderNow = 1;
    stateSliderNow = false;
    $('#HTPage2').toggleClass('hide');
    $('#NumberPage1').toggleClass('mauChu');
    $('#NumberPage2').toggleClass('mauChu');
    $("#HTPage111").height($("#HTPage2").height());
    $('#HTPage1').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage1').toggleClass('hide');
        $('#HTPage1').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)

  
   
})