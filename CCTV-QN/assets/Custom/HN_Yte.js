﻿$(document).ready(function () {
    

    var chartDautuNN = new CanvasJS.Chart("chartDautuNN", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Vốn đầu tư phân theo nguồn vốn",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Triệu đồng",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "line",
            showInLegend: true,
            name: "KV Nhà nước",
            markerType: "square",
            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 3230768 },
                { label: "2013", y: 3524822 },
                { label: "2014", y: 4956082 },
                { label: "2015", y: 3305064 },
                { label: "2016", y: 3308432 }

            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "KV ngoài Nhà nước",
            //lineDashType: "dash",
            color: "#98d8f7",
            dataPoints: [
                { label: "2010", y: 2976488 },
                { label: "2013", y: 4432259 },
                { label: "2014", y: 4418650 },
                { label: "2015", y: 5068851 },
                { label: "2016", y: 6212677 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Nước ngoài trực tiếp ĐT",
            //  lineDashType: "dash",
            color: "#5bec75",
            dataPoints: [
                { label: "2010", y: 93981 },
                { label: "2013", y: 150707 },
                { label: "2014", y: 157096 },
                { label: "2015", y: 250612 },
                { label: "2016", y: 421967 }
            ]
        }]
    });
    chartDautuNN.render();

    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "light2",
        animationEnabled: true,

        title: {
            text: "Dự án đầu tư trực tiếp của nước ngoài được cấp phép",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} dự án (#percent%)",
            dataPoints: [
                { y: 4, label: "Khác ", color: "#0a94cc" },
                { y: 3, label: "Đài Loan", color: "#1fbaf9" },
                { y: 4, label: "Ấn Độ ", color: "#4fc5f5" },
                { y: 7, label: "Trung Quốc", color: "#5aa8c7" },
                { y: 4, label: "Hàn Quốc", color: "#bfdbe6" },
            ]
        }]
    });
    chartDautuCong.render();

    //Tai nạn giao thông
    var chartATGT = new CanvasJS.Chart("chartATGT", {
        theme: "light2",
        title: {
            text: "Thống kê an toàn giao thông theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            title: "Số vụ tai nạn giao thông",
            lineColor: "#f1b656",
            tickColor: "#f1b656",
            labelFontColor: "#f1b656",
            titleFontColor: "#f1b656",


        },
        axisY2: [{
            title: "Số người bị thương",
            lineColor: "#cc56f1",
            tickColor: "#cc56f1",
            labelFontColor: "#cc56f1",
            titleFontColor: "#cc56f1",
        },
        {
            title: "Số người tử vong",
            lineColor: "#30fbfd",
            tickColor: "#30fbfd",
            labelFontColor: "#30fbfd",
            titleFontColor: "#30fbfd",

        }],
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATGT
        },
        data: [{
            type: "line",
            name: "Số vụ tai nạn giao thông",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 50 },
                { x: 2, y: 20 },
                { x: 3, y: 30 },
                { x: 4, y: 25 },
                { x: 5, y: 28 },
                { x: 6, y: 15 },
                { x: 7, y: 30 },
                { x: 8, y: 27 },

            ]
        },
        {
            type: "line",
            name: "Số người tử vong",
            color: "#30fbfd",
            axisYIndex: 0,
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 45 },
                { x: 2, y: 24 },
                { x: 3, y: 40 },
                { x: 4, y: 30 },
                { x: 5, y: 35 },
                { x: 6, y: 18 },
                { x: 7, y: 31 },
                { x: 8, y: 29 },

            ]
        },
        {
            type: "line",
            name: "Số người bị thương",
            color: "#cc56f1",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42 },
                { x: 2, y: 15 },
                { x: 3, y: 28 },
                { x: 4, y: 22 },
                { x: 5, y: 25 },
                { x: 6, y: 16 },
                { x: 7, y: 33 },
                { x: 8, y: 20 },

            ]
        }]
    });
    chartATGT.render();

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    //Y tế

    var chartYte = new CanvasJS.Chart("chartYte", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Thống kê ca mắc cúm và chân tay miệng theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },

        axisX: {
            title: "Tháng"
        },
        axisY: {
            title: "mắc bệnh mới"
        },
        data: [{
            type: "bubble",
            legendMarkerType: "circle",
            toolTipContent: "<span style=\"color:#4F81BC \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            name: "Bệnh cúm",
            showInLegend: true,
            color: "#f1a129",
            dataPoints: [
                { x: 1, z: 23, y: 330, },
                { x: 2, z: 28, y: 390 },
                { x: 3, z: 39, y: 400 },
                { x: 4, z: 34, y: 430 },
                { x: 5, z: 24, y: 321 },
                { x: 6, z: 29, y: 250 },
                { x: 7, z: 29, y: 370 },
            ]
        },
        {
            type: "bubble",
            legendMarkerType: "circle",
            name: "Bệnh chân tay miệng",
            showInLegend: true,
            color: "#65c4f3",
            toolTipContent: "<span style=\"color:#C0504E \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            dataPoints: [
                { x: 1, z: 19, y: 200 },
                { x: 2, z: 27, y: 300 },
                { x: 3, z: 35, y: 330 },
                { x: 4, z: 32, y: 190 },
                { x: 5, z: 29, y: 189 },
                { x: 6, z: 22, y: 150 },
                { x: 7, z: 27, y: 200 },
            ]
        }]
    });
    chartYte.render();
    var chartYT = new CanvasJS.Chart("chartYT", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Phụ nữ, trẻ em đến khám chữa bệnh ở địa phương",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: [{
            title: "Người",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
        axisY2: {
            title: "Số ca tử vong",
            lineColor: "#fb9c3a",
            tickColor: "#fb9c3a",
            labelFontColor: "#fb9c3a",
            titleFontColor: "#fb9c3a",

        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Trẻ em <15 tuổi",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Hoàn Kiếm", y: 102143 },
                { label: "H.B.T", y: 98017 },
                { label: "Ba Đình", y: 100649 },
                { label: "Đống Đa", y: 77716 },
                { label: "Cấu Giấy", y: 83137 },
                { label: "Thanh Xuân", y: 85163 },
                { label: "Nam T.L", y: 63973 },
                { label: "Bắc T.L", y: 54990 },
                { label: "Tây Hồ", y: 71993 },
                { label: "T.X Sơn Tây", y: 75164 },
                { label: "H. Hoài Đức", y: 52558 },
                { label: "Đông Anh", y: 20758 },
            ]
        },
        {
            type: "column",
            name: "PN từ 15- 49 tuổi",
            color: "#72c6f1",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Hoàn Kiếm", y: 107133 },
                { label: "H.B.T", y: 101720 },
                { label: "Ba Đình", y: 92298 },
                { label: "Đống Đa", y: 82333 },
                { label: "Cấu Giấy", y: 89024 },
                { label: "Thanh Xuân", y: 84447 },
                { label: "Nam T.L", y: 71755 },
                { label: "Bắc T.L", y: 63520 },
                { label: "Tây Hồ", y: 72994 },
                { label: "T.X Sơn Tây", y: 75440 },
                { label: "H. Hoài Đức", y: 64133 },
                { label: "Đông Anh", y: 21689 },

            ]
        },
        {
            type: "column",
            name: "Số ca tử vong",
            color: "#fb9c3a",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { label: "Hoàn Kiếm", y: 2895 },
                { label: "H.B.T", y: 2936 },
                { label: "Ba Đình", y: 2525 },
                { label: "Đống Đa", y: 2562 },
                { label: "Cấu Giấy", y: 2045 },
                { label: "Thanh Xuân", y: 1840 },
                { label: "Nam T.L", y: 2592 },
                { label: "Bắc T.L", y: 1973 },
                { label: "Tây Hồ", y: 1930 },
                { label: "T.X Sơn Tây", y: 1448 },
                { label: "H. Hoài Đức", y: 1847 },
                { label: "Đông Anh", y: 579 },

            ]
        }]
    });
    chartYT.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
});
