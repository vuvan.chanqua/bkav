﻿$(document).ready(function () {
    //thủy lợi
    var chartTL = new CanvasJS.Chart("chartTL", {
        theme: "dark2",
        title: {
            text: "Mức nước hiện tại của các trạm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },

        axisY: {
            title: "Mực nước (cm)",

        },
        data: [{
            type: "column",
            yValueFormatString: "#,### cm",
            indexLabel: "{y}",
            dataPoints: [
                { label: "Cẩm Nhượng", y: 206 },
                { label: "Chu Lễ", y: 163 },
                { label: "Hòa Duyệt", y: 154 },
                { label: "Linh Cảm", y: 176 },
                { label: "Sơn Diệm", y: 184 },

            ]
        }]
    });

    function updateChartTL() {
        var boilerColor, deltaY, yVal;
        var dps = chartTL.options.data[0].dataPoints;
        var ten = ["Cẩm Nhượng", "Chu Lễ", "Hòa Duyệt", "Linh Cảm", "Sơn Diệm"];
        for (var i = 0; i < dps.length; i++) {
            deltaY = Math.round(2 + Math.random() * (-2 - 2));
            yVal = deltaY + dps[i].y > 0 ? dps[i].y + deltaY : 0;
            boilerColor = yVal > 200 ? "#FF2500" : yVal >= 170 ? "#FF6000" : yVal < 170 ? "#6B8E23 " : null;
            dps[i] = { label: ten[i], y: yVal, color: boilerColor };
        }
        chartTL.options.data[0].dataPoints = dps;
        chartTL.render();
    };
    updateChartTL();

    setInterval(function () { updateChartTL() }, 500);
    //Y tế

    var chartYte = new CanvasJS.Chart("chartYte", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Thống kê ca mắc cúm và chân tay miệng theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },

        axisX: {
            title: "Tháng"
        },
        axisY: {
            title: "mắc bệnh mới"
        },
        data: [{
            type: "bubble",
            legendMarkerType: "circle",
            toolTipContent: "<span style=\"color:#4F81BC \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            name: "Bệnh cúm",
            showInLegend: true,
            color: "#f1a129",
            dataPoints: [
                { x: 1, z: 23, y: 330, },
                { x: 2, z: 28, y: 390 },
                { x: 3, z: 39, y: 400 },
                { x: 4, z: 34, y: 430 },
                { x: 5, z: 24, y: 321 },
                { x: 6, z: 29, y: 250 },
                { x: 7, z: 29, y: 370 },
            ]
        },
        {
            type: "bubble",
            legendMarkerType: "circle",
            name: "Bệnh chân tay miệng",
            showInLegend: true,
            color: "#65c4f3",
            toolTipContent: "<span style=\"color:#C0504E \"><b>{name}</b></span><br/><b> Tháng:</b> {x} người<br/><b> Nội trú:</b> {z} người<br/><b> Mắc mới:</b></span> {y} người",
            dataPoints: [
                { x: 1, z: 19, y: 200 },
                { x: 2, z: 27, y: 300 },
                { x: 3, z: 35, y: 330 },
                { x: 4, z: 32, y: 190 },
                { x: 5, z: 29, y: 189 },
                { x: 6, z: 22, y: 150 },
                { x: 7, z: 27, y: 200 },
            ]
        }]
    });
    chartYte.render();
    var chartYT = new CanvasJS.Chart("chartYT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Phụ nữ, trẻ em đến khám chữa bệnh ở địa phương",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: [{
            title: "Người",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
        axisY2: {
            title: "Số ca tử vong",
            lineColor: "#fb9c3a",
            tickColor: "#fb9c3a",
            labelFontColor: "#fb9c3a",
            titleFontColor: "#fb9c3a",

        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Trẻ em <15 tuổi",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Cẩm Xuyên", y: 102143 },
                { label: "Thạch Hà", y: 98017 },
                { label: "Can Lộc", y: 100649 },
                { label: "Hương Sơn", y: 77716 },
                { label: "Kỳ Anh", y: 83137 },
                { label: "Hồng Lĩnh", y: 85163 },
                { label: "Đức Thọ", y: 63973 },
                { label: "Hương Khê", y: 54990 },
                { label: "Nghi Xuân", y: 71993 },
                { label: "T.P Hà Tĩnh", y: 75164 },
                { label: "Lộc Hà", y: 52558 },
                { label: "Vũ Quang", y: 20758 },
            ]
        },
        {
            type: "column",
            name: "PN từ 15- 49 tuổi",
            color: "#72c6f1",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Cẩm Xuyên", y: 107133 },
                { label: "Thạch Hà", y: 101720 },
                { label: "Can Lộc", y: 92298 },
                { label: "Hương Sơn", y: 82333 },
                { label: "Kỳ Anh", y: 89024 },
                { label: "Hồng Lĩnh", y: 84447 },
                { label: "Đức Thọ", y: 71755 },
                { label: "Hương Khê", y: 63520 },
                { label: "Nghi Xuân", y: 72994 },
                { label: "T.P Hà Tĩnh", y: 75440 },
                { label: "Lộc Hà", y: 64133 },
                { label: "Vũ Quang", y: 21689 },

            ]
        },
        {
            type: "column",
            name: "Số ca tử vong",
            color: "#fb9c3a",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { label: "Cẩm Xuyên", y: 2895 },
                { label: "Thạch Hà", y: 2936 },
                { label: "Can Lộc", y: 2525 },
                { label: "Hương Sơn", y: 2562 },
                { label: "Kỳ Anh", y: 2045 },
                { label: "Hồng Lĩnh", y: 1840 },
                { label: "Đức Thọ", y: 2592 },
                { label: "Hương Khê", y: 1973 },
                { label: "Nghi Xuân", y: 1930 },
                { label: "T.P Hà Tĩnh", y: 1448 },
                { label: "Lộc Hà", y: 1847 },
                { label: "Vũ Quang", y: 579 },

            ]
        }]
    });
    chartYT.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
    var chartDautu = new CanvasJS.Chart("chartDautu", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Cấp giấy phép lái xe",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {
            interval: 1,
            intervalType: "year"
        },
        axisY: {
            title: "chiếc",
            valueFormatString: "#,000",
            gridColor: "#dedbd7",
            tickColor: "#dedbd7"
        },
        toolTip: {
            shared: true,
            content: toolTipContentDautu
        },
        data: [{
            type: "stackedColumn",
            showInLegend: true,
            color: "#0e9ce2",
            name: "Cấp mới",
            dataPoints: [
                { y: 1470, label: "A1" },
                { y: 1053, label: "Ô tô" },
                { y: 13000, label: "Ước cả năm" }
            ]
        },
        {
            type: "stackedColumn",
            showInLegend: true,
            name: "Cấp lại,đổi",
            color: "#98d8f7",
            dataPoints: [
                { y: 835, label: "A1" },
                { y: 847, label: "Ô tô" },
                { y: 8000, label: "Ước cả năm" }
            ]
        }]
    });
    chartDautu.render();

    function toolTipContentDautu(e) {
        var str = "";
        var total = 0;
        var str2, str3;
        for (var i = 0; i < e.entries.length; i++) {
            var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\"> " + e.entries[i].dataSeries.name + "</span>: $<strong>" + e.entries[i].dataPoint.y + "</strong>bn<br/>";
            total = e.entries[i].dataPoint.y + total;
            str = str.concat(str1);
        }
        str2 = "<span style = \"color:DodgerBlue;\"><strong>" + (e.entries[0].dataPoint.x).getFullYear() + "</strong></span><br/>";
        total = Math.round(total * 100) / 100;
        str3 = "<span style = \"color:Tomato\">Total:</span><strong> $" + total + "</strong>bn<br/>";
        return (str2.concat(str)).concat(str3);
    }

    var chartDautuNN = new CanvasJS.Chart("chartDautuNN", {
        animationEnabled: true,
        theme: "dark2", // "light1", "dark2", "dark1", "dark2"
        title: {
            text: "Vốn đầu tư thực hiện của khu vực kinh tế Nhà nước",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Tỷ đồng",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "line",
            showInLegend: true,

            name: "Theo giá hiện hành",
            markerType: "square",

            color: "#F08080",
            dataPoints: [
                { label: "2009", y: 13301 },
                { label: "2010", y: 16257 },
                { label: "2011", y: 18273 },
                { label: "2012", y: 23659 },
                { label: "2013", y: 36151 },
                { label: "2014", y: 31155 },
                { label: "2015", y: 32492 },
                { label: "2016", y: 33450 },

            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Theo giá so sánh 2010",
            lineDashType: "dash",
            color: "#0a9be2",
            dataPoints: [
                { label: "2009", y: 14234 },
                { label: "2010", y: 16257 },
                { label: "2011", y: 15166 },
                { label: "2012", y: 18838 },
                { label: "2013", y: 28642 },
                { label: "2014", y: 24074 },
                { label: "2015", y: 24833 },
                { label: "2016", y: 25815 },
            ]
        }]
    });
    chartDautuNN.render();
    var chartGiaodich = new CanvasJS.Chart("chartGiaodich", {
        theme: "dark2", // "light1", "dark2", "dark1", "dark2"
        animationEnabled: true,
        title: {
            text: "Lượng giao dịch bất động sản 2017 theo tháng",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {
            interval: 1,
            intervalType: "month",
            valueFormatString: "MM"
        },
        axisY: {
            title: "Giao dịch BĐS",
            valueFormatString: "#0"
        },
        data: [{
            type: "column",
            markerSize: 12,
            showInLegend: true,
            color: "#f7900d",
            name: "Năm 2018",
            xValueFormatString: "MM",
            yValueFormatString: "###.#",
            dataPoints: [
                { x: new Date(2017, 00, 1), y: 1300 },
                { x: new Date(2017, 01, 1), y: 800 },
                { x: new Date(2017, 02, 1), y: 1000 },
                { x: new Date(2017, 03, 1), y: 1050 },
                { x: new Date(2017, 04, 1), y: 1200 },
                { x: new Date(2017, 05, 1), y: 1300 },
                { x: new Date(2017, 06, 1), y: 1300 },
                { x: new Date(2017, 07, 1), y: 1350 },
                { x: new Date(2017, 08, 1), y: 1200 },
                { x: new Date(2017, 09, 1), y: 1400 },

            ]
        }, {
            type: "column",
            markerSize: 12,
            showInLegend: true,
            color: "#f7b969",
            name: "Năm 2017",
            xValueFormatString: "MM",
            yValueFormatString: "###.#",
            dataPoints: [
                { x: new Date(2017, 00, 1), y: 1395 },
                { x: new Date(2017, 01, 1), y: 900 },
                { x: new Date(2017, 02, 1), y: 1100 },
                { x: new Date(2017, 03, 1), y: 1170 },
                { x: new Date(2017, 04, 1), y: 1300 },
                { x: new Date(2017, 05, 1), y: 1405 },
                { x: new Date(2017, 06, 1), y: 1350 },
                { x: new Date(2017, 07, 1), y: 1400 },
                { x: new Date(2017, 08, 1), y: 1300 },
                { x: new Date(2017, 09, 1), y: 1500 },

            ]

        }]
    });
    chartGiaodich.render();

    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "dark2",
        animationEnabled: true,

        title: {
            text: "Các dự án gọi vốn đầu tư giai đoạn 2016-2020",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} dự án (#percent%)",
            dataPoints: [
                { y: 21, label: "Vốn <100 triệu USD ", color: "#0a94cc" },
                { y: 4, label: "Vốn 100 triệu USD ", color: "#1fbaf9" },
                { y: 3, label: "Vốn 200 triệu USD ", color: "#4fc5f5" },
                { y: 4, label: "Vốn 300 triệu USD ", color: "#5aa8c7" },
                { y: 8, label: "Vốn >300 triệu USD ", color: "#bfdbe6" },
            ]
        }]
    });
    chartDautuCong.render();

    //Tai nạn giao thông
    var chartATGT = new CanvasJS.Chart("chartATGT", {
        theme: "dark2",
        title: {
            text: "Thống kê an toàn giao thông theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: {
            title: "Số vụ tai nạn giao thông",
            lineColor: "#f1b656",
            tickColor: "#f1b656",
            labelFontColor: "#f1b656",
            titleFontColor: "#f1b656",


        },
        axisY2: [{
            title: "Số người bị thương",
            lineColor: "#cc56f1",
            tickColor: "#cc56f1",
            labelFontColor: "#cc56f1",
            titleFontColor: "#cc56f1",
        },
        {
            title: "Số người tử vong",
            lineColor: "#30fbfd",
            tickColor: "#30fbfd",
            labelFontColor: "#30fbfd",
            titleFontColor: "#30fbfd",

        }],
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATGT
        },
        data: [{
            type: "line",
            name: "Số vụ tai nạn giao thông",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 50 },
                { x: 2, y: 20 },
                { x: 3, y: 30 },
                { x: 4, y: 25 },
                { x: 5, y: 28 },
                { x: 6, y: 15 },
                { x: 7, y: 30 },
                { x: 8, y: 27 },

            ]
        },
        {
            type: "line",
            name: "Số người tử vong",
            color: "#30fbfd",
            axisYIndex: 0,
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 45 },
                { x: 2, y: 24 },
                { x: 3, y: 40 },
                { x: 4, y: 30 },
                { x: 5, y: 35 },
                { x: 6, y: 18 },
                { x: 7, y: 31 },
                { x: 8, y: 29 },

            ]
        },
        {
            type: "line",
            name: "Số người bị thương",
            color: "#cc56f1",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42 },
                { x: 2, y: 15 },
                { x: 3, y: 28 },
                { x: 4, y: 22 },
                { x: 5, y: 25 },
                { x: 6, y: 16 },
                { x: 7, y: 33 },
                { x: 8, y: 20 },

            ]
        }]
    });
    chartATGT.render();

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    var chartHanhKhach = new CanvasJS.Chart("chartHanhKhach", {
        animationEnabled: true,
        theme: "dark2", // "light1", "dark2", "dark1", "dark2"
        title: {
            text: "Lượng hành khách vận chuyển, luân chuyển theo năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "nghìn người",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "column",
            showInLegend: true,

            name: "KH vận chuyển",
            //  markerType: "square",

            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 9200 },
                { label: "2011", y: 9800 },
                { label: "2012", y: 10900 },
                { label: "2013", y: 11400 },
                { label: "2014", y: 12400 },
                { label: "2015", y: 13300 },
                { label: "2016", y: 14600 },
                { label: "2017", y: 16800 },


            ]
        },
        {
            type: "column",
            showInLegend: true,
            name: "KH luân chuyển",
            // lineDashType: "dash",
            color: "#ea9407",
            dataPoints: [
                { label: "2010", y: 1200 },
                { label: "2011", y: 1456 },
                { label: "2012", y: 1653 },
                { label: "2013", y: 1768 },
                { label: "2014", y: 1898 },
                { label: "2015", y: 1973.3 },
                { label: "2016", y: 2113.1 },
                { label: "2017", y: 2501.7 },
            ]
        }]
    });
    chartHanhKhach.render();

   
   
    $("#HTPage111").height($("#HTPage1").height());
   
});
var stateSliderNow = true;
var _stateSliderNow = 0; 
$('#NumberPage1').click(function () {
    if (!stateSliderNow || _stateSliderNow == 0) return;
    _stateSliderNow = 0;
    stateSliderNow = false;
    $('#HTPage1').toggleClass('hide');
    $("#HTPage111").height($("#HTPage1").height());
    $('#HTPage2').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage2').toggleClass('hide');
        $('#HTPage2').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)
  
})
$('#NumberPage2').click(function () {
    if (!stateSliderNow || _stateSliderNow == 1) return;
    _stateSliderNow = 1;
    stateSliderNow = false;
    $('#HTPage2').toggleClass('hide');
    $("#HTPage111").height($("#HTPage2").height());
    $('#HTPage1').toggleClass('activeSliderBody');
    setTimeout(function () {
        $('#HTPage1').toggleClass('hide');
        $('#HTPage1').toggleClass('activeSliderBody');
        stateSliderNow = true;
    }, 1000)
    var chartCLCT = new CanvasJS.Chart("chartCLCT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Công tác kiểm soát chất lượng công trình theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisY: {
            valueFormatString: "#0",
            title: "công trình"
        },
        axisX: {
            title: "tháng"
        },
        toolTip: {
            shared: true
        },
        data: [{
            type: "stackedArea",
            color: "#0e9ce2",
            showInLegend: true,
            toolTipContent: "<span style=\"color:#4F81BC\"><strong>{name}: </strong></span> {y}",
            name: "Số công trình đã kiểm tra",
            dataPoints: [

                { x: 2, y: 7 },
                { x: 3, y: 39 },
                { x: 4, y: 23 },
                { x: 5, y: 28 },
                { x: 6, y: 31 },
                { x: 7, y: 28 },
                { x: 8, y: 22 },
                { x: 9, y: 30 },
                { x: 10, y: 24 },

            ]
        },
        {
            type: "stackedArea",
            color: "#08608c",
            name: "Số công trình trọng điểm",
            toolTipContent: "<span style=\"color:#C0504E\"><strong>{name}: </strong></span> {y}",
            showInLegend: true,
            dataPoints: [
                { x: 2, y: 3 },
                { x: 3, y: 11 },
                { x: 4, y: 11 },
                { x: 5, y: 9 },
                { x: 6, y: 9 },
                { x: 7, y: 7 },
                { x: 8, y: 7 },
                { x: 9, y: 7 },
                { x: 10, y: 8 }
            ]
        }]
    });
    chartCLCT.render();
    var dataPoints1 = [];
    var dataPoints2 = [];

    var chartMXH = new CanvasJS.Chart("chartMXH", {
        zoomEnabled: true,
        theme: "dark2",
        title: {
            text: "Chỉ số mạng xã hội",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisX: {
            title: ""
        },
        axisY: {
            prefix: "",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            verticalAlign: "top",
            fontSize: 22,
            fontColor: "dimGrey",
            itemclick: toggleDataSeriesMXH
        },
        data: [{
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            xValueFormatString: "hh:mm:ss TT",
            showInLegend: true,
            name: "Facebook",
            dataPoints: dataPoints1
        },
        {
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            showInLegend: true,
            name: "Twitter",
            dataPoints: dataPoints2
        }]
    });

    function toggleDataSeriesMXH(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartMXH.render();
    }

    var updateInterval = 3000;
    // initial value
    var yValue1 = 600;
    var yValue2 = 605;

    var time = new Date;
    // starting at 9.30 am
    time.setHours(9);
    time.setMinutes(30);
    time.setSeconds(00);
    time.setMilliseconds(00);

    function updateChart(count) {
        count = count || 1;
        var deltaY1, deltaY2;
        for (var i = 0; i < count; i++) {
            time.setTime(time.getTime() + updateInterval);
            deltaY1 = .5 + Math.random() * (-.5 - .5);
            deltaY2 = .5 + Math.random() * (-.5 - .5);

            // adding random value and rounding it to two digits.
            yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
            yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

            // pushing the new values
            dataPoints1.push({
                x: time.getTime(),
                y: yValue1
            });
            dataPoints2.push({
                x: time.getTime(),
                y: yValue2
            });
        }

        // updating legend text with  updated with y Value
        chartMXH.options.data[0].legendText = " Facebook " + yValue1;
        chartMXH.options.data[1].legendText = " Twitter " + yValue2;
        chartMXH.render();
    }
    // generates first set of dataPoints
    updateChart(100);
    setInterval(function () { updateChart() }, updateInterval);

    //Xay dung
    var chartGiaXD = new CanvasJS.Chart("chartGiaXD", {
        // "light1", "dark2", "dark1", "dark2"

        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Giá trị sản xuất XD tháng 9",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 41.57, label: "Công trình nhà ở ", color: "#e8880c" },
                { y: 58.43, label: "Công trình khác", color: "#f5bd55" },


            ]
        }]
    });
    chartGiaXD.render();

    var chartDTSan = new CanvasJS.Chart("chartDTSan", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Giá trị sản xuất XD tháng 9",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisY: {
            includeZero: false,
            title: "Nghìn m2",
            valueFormatString: "#,000",
        },
        data: [{
            type: "column",
            name: "Nhà ở chung cư",
            color: "#e8880c",
            showInLegend: true,
            // axisYIndex: 1,
            dataPoints: [
                { label: "2007", y: 2359 },
                { label: "2008", y: 2058 },
                { label: "2009", y: 3409 },
                { label: "2010", y: 4559 },
                { label: "2011", y: 4219 },
                { label: "2012", y: 1844 },
                { label: "2013", y: 3361 },
                { label: "2014", y: 2326 },
                { label: "2015", y: 2324 }
            ]
        }, {
            type: "column",
            name: "Nhà ở riêng lẻ",
            color: "#f5bd55",
            showInLegend: true,
            //axisYIndex: 1,
            dataPoints: [
                { label: "2007", y: 48776 },
                { label: "2008", y: 55853 },
                { label: "2009", y: 64801 },
                { label: "2010", y: 81326 },
                { label: "2011", y: 80147 },
                { label: "2012", y: 79469 },
                { label: "2013", y: 83260 },
                { label: "2014", y: 87517 },
                { label: "2015", y: 91098 }
            ]
        }]
    });
    chartDTSan.render();

    var chartVBPL = new CanvasJS.Chart("chartVBPL", {
        theme: "dark2",
        animationEnabled: true,
        //  exportEnabled: true,
        title: {
            text: "Tình hình giải quyết hồ sơ trong tháng",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} hồ sơ (#percent%)",
            dataPoints: [
                { y: 6, label: "Tiếp nhận", color: "#045b9a" },
                { y: 11, label: "Đang xử lý", color: "#1296f7" },
                { y: 15, label: "Đã trả kết quả", color: "#5cb3f3" },
            ]
        }]
    });
    chartVBPL.render();
    var chartTieuchuan = new CanvasJS.Chart("chartTieuchuan", {
        theme: "dark2",
        animationEnabled: true,
        // exportEnabled: true,
        title: {
            text: "Tình hình xử lý hồ sơ ",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} hồ sơ (#percent%)",
            dataPoints: [
                { y: 99, label: "Đúng hạn", color: "#045b9a" },
                { y: 10, label: "Quá hạn", color: "#1296f7" },

            ]
        }]
    });
    chartTieuchuan.render();
    //Biểu đồ tội phạm
    var chartToipham = new CanvasJS.Chart("chartToipham", {
        theme: "dark2", // "light1", "light2", "dark1", "dark2"

        animationEnabled: true,
        title: {
            text: "Các loại tội phạm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "false",
            indexLabelPlacement: "inside",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{y}%",
            dataPoints: [
                { y: 47.4, label: "Tội phạm trật tự xã hội", color: "#ef960e" },
                { y: 33.1, label: "Tội phạm trật tự QL kinh tế", color: "#e8a43a" },
                { y: 10.5, label: "Tội phạm ma túy", color: "#f1c177" },
                { y: 9, label: "Tội phạm môi trường", color: "#dcccb2" }

            ]
        }]
    });
    chartToipham.render();
    var chartTainanGT = new CanvasJS.Chart("chartTainanGT", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "An ninh trật tự",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "line",
            name: "Tai nạn giao thông",
            color: "#3ea8f7",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 7 },
                { label: "T4", y: 12 },
                { label: "T5", y: 1 },
            ]
        }, {
            type: "line",
            name: "Cướp giật",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 5 },
                { label: "T2", y: 3 },
                { label: "T3", y: 2 },
                { label: "T4", y: 7 },
                { label: "T5", y: 5 },
            ]

        }, {
            type: "line",
            name: "Hỏa hoạn",
            color: "#e809f5",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 2 },
                { label: "T2", y: 4 },
                { label: "T3", y: 2 },
                { label: "T4", y: 0 },
                { label: "T5", y: 1 },
            ]

        }, {
            type: "line",
            name: "Gây rối trật tự công cộng",
            color: "#09f3f5",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 2 },
                { label: "T2", y: 0 },
                { label: "T3", y: 1 },
                { label: "T4", y: 0 },
                { label: "T5", y: 2 },
            ]

        }, {
            type: "line",
            name: "Giết người",
            color: "#41f509",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 1 },
                { label: "T2", y: 0 },
                { label: "T3", y: 0 },
                { label: "T4", y: 1 },
                { label: "T5", y: 0 },
            ]
        }]
    });
    chartTainanGT.render();
    var chartTainanGT1 = new CanvasJS.Chart("chartTainanGT1", {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "An ninh trật tự",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#fff"
        },
        axisY: {
            includeZero: false
        },
        data: [{
            type: "column",
            name: "Số vụ",
            color: "#ea9407",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 500 },
                { label: "T2", y: 300 },
                { label: "T3", y: 700 },
                { label: "T4", y: 120 },
                { label: "T5", y: 100 },
            ]
        }, {
            type: "column",
            name: "Bị thương",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 250 },
                { label: "T2", y: 230 },
                { label: "T3", y: 420 },
                { label: "T4", y: 70 },
                { label: "T5", y: 150 },
            ]

        }, {
            type: "column",
            name: "Người chết",
            color: "#a56908",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "T1", y: 150 },
                { label: "T2", y: 90 },
                { label: "T3", y: 150 },
                { label: "T4", y: 50 },
                { label: "T5", y: 70 },
            ]


        }]
    });
    chartTainanGT1.render();
})