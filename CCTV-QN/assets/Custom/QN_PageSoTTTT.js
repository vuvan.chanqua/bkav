﻿$(document).ready(function () {
    var dataPoints1 = [];
    var dataPoints2 = [];

    var chartMXH = new CanvasJS.Chart("chartMXH", {
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: "Chỉ số mạng xã hội",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisX: {
            title: ""
        },
        axisY: {
            prefix: "",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            verticalAlign: "top",
            fontSize: 22,
            fontColor: "dimGrey",
            itemclick: toggleDataSeriesMXH
        },
        data: [{
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            xValueFormatString: "hh:mm:ss TT",
            showInLegend: true,
            name: "Facebook",
            dataPoints: dataPoints1
        },
        {
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            showInLegend: true,
            name: "Twitter",
            dataPoints: dataPoints2
        }]
    });

    function toggleDataSeriesMXH(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartMXH.render();
    }

    var updateInterval = 3000;
    // initial value
    var yValue1 = 600;
    var yValue2 = 605;

    var time = new Date;
    // starting at 9.30 am
    time.setHours(9);
    time.setMinutes(30);
    time.setSeconds(0);
    time.setMilliseconds(0);

    function updateChart(count) {
        count = count || 1;
        var deltaY1, deltaY2;
        for (var i = 0; i < count; i++) {
            time.setTime(time.getTime() + updateInterval);
            deltaY1 = .5 + Math.random() * (-.5 - .5);
            deltaY2 = .5 + Math.random() * (-.5 - .5);

            // adding random value and rounding it to two digits.
            yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
            yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

            // pushing the new values
            dataPoints1.push({
                x: time.getTime(),
                y: yValue1
            });
            dataPoints2.push({
                x: time.getTime(),
                y: yValue2
            });
        }

        // updating legend text with  updated with y Value
        chartMXH.options.data[0].legendText = " Facebook " + yValue1;
        chartMXH.options.data[1].legendText = " Twitter " + yValue2;
        chartMXH.render();
    }
    // generates first set of dataPoints
    updateChart(100);
    setInterval(function () { updateChart() }, updateInterval);
    var chartVBPL1 = new CanvasJS.Chart("chartVBPL1", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Chỉ số xây dựng VB QPPL 2016",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 21.88, label: "Nghi định", color: "#c70000", },
                { y: 9.37, label: "Quyết định" },
                { y: 67.19, label: "Thông tư", color: "#f1f706" },
                { y: 1.56, label: "VB chưa trình/ban hành", color: "#06eefb" },

            ]
        }]
    });
    chartVBPL1.render();
    
    var chartVBPL = new CanvasJS.Chart("chartVBPL", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Tình hình giải quyết hồ sơ về TTHC",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Hồ sơ",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",


        }],
      
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Hồ sơ đúng hạn",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "Tháng 1", y: 300 },
                { label: "Tháng 2", y: 204 },
                { label: "Tháng 3", y: 450 },
                { label: "Tháng 4", y: 390 },
                { label: "Tháng 5", y: 171 },
                { label: "Tháng 6", y: 153 },
                { label: "Tháng 7", y: 139 },
                { label: "Tháng 8", y: 171 },
                { label: "Tháng 9", y: 239 },
                { label: "Tháng 10", y: 210 }
            ]
        },
        {
            type: "column",
            name: "Trễ hạn xử lý",
            color: "#FF6500",
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: [
                { label: "Tháng 1", y: 20 },
                { label: "Tháng 2", y: 24 },
                { label: "Tháng 3", y: 25 },
                { label: "Tháng 4", y: 19 },
                { label: "Tháng 5", y: 17 },
                { label: "Tháng 6", y: 15 },
                { label: "Tháng 7", y: 13 },
                { label: "Tháng 8", y: 21 },
                { label: "Tháng 9", y: 23 },
                { label: "Tháng 10", y: 21 }
            ]
        
        }]
    });
    chartVBPL.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }

    var chartYT = new CanvasJS.Chart("chartYT", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Hiện trạng điểm cung cấp dịch vụ viễn thông công cộng có người phục vụ",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisY: [{
            title: "Điểm giao dịch KH",
            lineColor: "#2fb1f3",
            tickColor: "#2fb1f3",
            labelFontColor: "#2fb1f3",
            titleFontColor: "#2fb1f3",
        }],
       
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesYT
        },
        data: [{
            type: "column",
            name: "Điểm giao dịch khách hàng",
            color: "#2fb1f3",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { label: "TP. HCM", y: 12 },
                { label: "Cẩm Phả", y: 8 },
                { label: "Móng Cái", y: 8 },
                { label: "Uông Bí", y: 6 },
                { label: "H.Ba Chẽ", y: 3 },
                { label: "H.Bình Lưu", y: 4 },
                { label: "H.Cô Tô", y: 2 },
                { label: "H.Đầm Hà", y: 2 },
                { label: "H.Đông Triều", y: 4 },
                { label: "H.Hải Hà", y: 2 },
                { label: "H.Hoành Bồ", y: 2 },
                { label: "H.Tiên Yên", y: 2 },
                { label: "H.Vân Đồn", y: 2 },
            ]      
        }]
    });
    chartYT.render();

    function toggleDataSeriesYT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }
  

    var chartDautuNN = new CanvasJS.Chart("chartDautuNN", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Dự toán chi cho phát thanh - truyền hình qua các năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Triệu đồng",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "line",
            showInLegend: true,
            name: "Chi thường xuyên",
            markerType: "square",
            color: "#f1b656",
            dataPoints: [
                { label: "2014", y: 20000 },
                { label: "2015", y: 18500},
                { label: "2016", y: 19000},
                { label: "2017", y: 23000},
                { label: "2018", y: 21000 }

            ]
       
        }]
    });
    chartDautuNN.render();

    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "light2",
        animationEnabled: true,

        title: {
            text: "Dự toán chi thường xuyên của Ngân sách cấp tỉnh năm 2018",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} Triệu đồng (#percent%)",
            dataPoints: [
                { y: 3854, label: "Hoạt động Kinh tế", color: "#0a94cc" },               
                { y: 6999, label: "Hoạt động QLNN, Đảng, đoàn thể", color: "#4fc5f5" },
               
            ]
        }]
    });
    chartDautuCong.render();

    //Tai nạn giao thông
    var chartATGT = new CanvasJS.Chart("chartATGT", {
        theme: "light2",
        title: {
            text: "Thống kê thanh kiểm tra thông tin báo chí, xuất bản theo tháng",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        axisY: [{
            title: "Số vụ kiểm tra",
            lineColor: "#f1b656",
            tickColor: "#f1b656",
            labelFontColor: "#f1b656",
            titleFontColor: "#f1b656",


        }, {
            title: "Số vụ vi phạm",
            lineColor: "#30fbfd",
            tickColor: "#30fbfd",
            labelFontColor: "#30fbfd",
            titleFontColor: "#30fbfd",
        }],
        axisY2: [
            {
                title: "Thu ngân sách",
                lineColor: "#cc56f1",
                tickColor: "#cc56f1",
                labelFontColor: "#cc56f1",
                titleFontColor: "#cc56f1",

            }],
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeriesATGT
        },
        data: [{
            type: "line",
            name: "Số vụ kiểm tra",
            color: "#f1b656",
            showInLegend: true,
            axisYIndex: 1,
            dataPoints: [
                { x: 1, y: 50 },
                { x: 2, y: 20 },
                { x: 3, y: 30 },
                { x: 4, y: 25 },
                { x: 5, y: 28 },
                { x: 6, y: 15 },
                { x: 7, y: 30 },
                { x: 8, y: 31 },

            ]
        },
        {
            type: "line",
            name: "Số vụ vi phạm",
            color: "#30fbfd",
            axisYIndex: 1,
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 5 },
                { x: 2, y: 7 },
                { x: 3, y: 4 },
                { x: 4, y: 6 },
                { x: 5, y: 4 },
                { x: 6, y: 3 },
                { x: 7, y: 7 },
                { x: 8, y: 2 },

            ]
        },
        {
            type: "line",
            name: "Thu ngân sách",
            color: "#cc56f1",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { x: 1, y: 42 },
                { x: 2, y: 15 },
                { x: 3, y: 28 },
                { x: 4, y: 22 },
                { x: 5, y: 25 },
                { x: 6, y: 16 },
                { x: 7, y: 33 },
                { x: 8, y: 20 },

            ]
        }]
    });
    chartATGT.render();

    function toggleDataSeriesATGT(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else {
            e.dataSeries.visible = true;
        }
        e.chartATGT.render();
    }
    var chartHanhKhach = new CanvasJS.Chart("chartHanhKhach", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "light2"
        title: {
            text: "Tình hình cấp thẻ nhà báo theo năm",
            fontFamily: "arial",
            fontSize: 16,
            fontColor: "#000"
        },
        axisX: {

            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "thẻ",
            crosshair: {
                enabled: true
            }
        },

        data: [{
            type: "column",
            showInLegend: true,

            name: "Tổng số thẻ đã cấp",
            //  markerType: "square",

            color: "#f1b656",
            dataPoints: [
                { label: "2010", y: 70 },
                { label: "2011", y: 75 },
                { label: "2012", y: 74 },
                { label: "2013", y: 78 },
                { label: "2014", y: 83 },
                { label: "2015", y: 95 },
                { label: "2016", y: 98 },
                { label: "2017", y: 100 },
                { label: "2018", y: 107 }

            ]
        },
        {
            type: "column",
            showInLegend: true,
            name: "Cấp mới",
            // lineDashType: "dash",
            color: "#ea9407",
            dataPoints: [
                { label: "2010", y: 20 },
                { label: "2011", y: 45 },
                { label: "2012", y: 35 },
                { label: "2013", y: 26 },
                { label: "2014", y: 18 },
                { label: "2015", y: 19 },
                { label: "2016", y: 21 },
                { label: "2017", y: 25 },
                { label: "2018", y: 15 }
            ]
        }]
    });
    chartHanhKhach.render();
   
    var chartQLTT = new CanvasJS.Chart("chartQLTT", {
        theme: "light2",
        title: {
            text: "Thống kê số thuê bao điện thoại",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        axisY: {
            title: "Nghìn thuê bao",
          
            lineColor: "#4F81BC",
            tickColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            gridThickness: 0
        },
     
        data: [{
            type: "area",
            name: "Di động",
            legendText: "Di động",
            yValueFormatString: "0.##' nghìn Thuê bao'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            color: "#d6d2ce",
            dataPoints: [
                { label: "2010", y: 1885.4},
                { label: "2015", y: 1393.8 },
                { label: "2016", y: 1935.1},
                { label: "2017", y: 1981.7 }
            ]
        },

        {
            type: "area",
            name: "Cố định",
            legendText: "Xuất khẩu",
            yValueFormatString: "0.##' nghìn Thuê bao'",
            indexLabelFontFamily: "tahoma",
            showInLegend: true,
            color: "#ef7a08",
            dataPoints: [
                { label: "2010", y: 270.2 },
                { label: "2015", y: 152.9 },
                { label: "2016", y: 69.4 },
                { label: "2017", y: 56.4 }
            ]
        }]
    });
    chartQLTT.render();
    function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartQLTT.render();
    }
    var chartCCDIEN = new CanvasJS.Chart("chartCCDIEN", {
        animationEnabled: true,
        theme: "light2",
        title: {
            text: "Cơ cấu hạ tầng cột ăng ten thu phát sóng thông tin di động",
            fontSize: 16,
            indexLabelFontFamily: "tahoma",
        },
        data: [{
            type: "pie",
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} vị trí (#percent%)",          
            indexLabelFontColor: "black",
            indexLabelFontSize: 16,
            indexLabel: "{label} -  #percent%",
            //reversed: true, // Reverses the pyramid
            dataPoints: [
                { y: 361, label: "Vinaphone", color: "#06948f" },
                { y: 372, label: "MobiFone", color: "#09d4cd" },
                { y: 486, label: "Viettel Mobile", color: "#24f1ea" },
                { y: 100, label: "GMobile", color: "#8cf7f3" },
                { y: 117, label: "Vietnamobile", color: "#065653" },

            ]
        }]
    });
    chartCCDIEN.render();
    var chartTHU_BCVT = new CanvasJS.Chart("chartTHU_BCVT", {
        theme: "light2",
        animationEnabled: true,
        title: {
            text: "Cơ cấu Doanh thu dịch vụ bưu chính",
            horizontalAlign: "left",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} triệu đồng (#percent%)",
            dataPoints: [
                { y: 169, label: "dịch vụ thư", color: "#f5a108" },
                {y: 463.1, label: "gói, kiện HH trong nước", color: "#edc06e" },               
                   { y: 563.1, label: "gói, kiện HH đi các nước", color: "#edc06e" },
                { y: 763.1, label: "gói, kiện hàng hóa đi các nước", color: "#edc06e" },
            ]
        }]
    });
    chartTHU_BCVT.render();
   
});
