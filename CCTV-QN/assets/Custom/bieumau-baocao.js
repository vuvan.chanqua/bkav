﻿$(document).ready(function () {
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})

$('#btntatca').click(function () {
    $('#dstatca').show();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbmdagui').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').show();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbmdanhan').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').show();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbmdaxuly').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').show();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbmdangxuly').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').show();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbcdangxuly').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').show();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbcdaxuly').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').show();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbmquahan').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').show();
    $('#dsbcquahan').hide();
    $('#dskhac').hide();
})
$('#btnbcquahan').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').show();
    $('#dskhac').hide();
})
$('#btnkhac').click(function () {
    $('#dstatca').hide();
    $('#dsbmgui').hide();
    $('#dsbmnhan').hide();
    $('#dsbmdaxuly').hide();
    $('#dsbmdangxuly').hide();
    $('#dsbcdangxuly').hide();
    $('#dsbcdaxuly').hide();
    $('#dsbmquahan').hide();
    $('#dsbcquahan').hide();
    $('#dskhac').show();
})

$('#btntaobieumau').click(function () {
    $('#ModalTaoBieuMau').modal('show');
});

$('.itembmbc').click(function () {
    $('#ModalHSCongViec').modal('show');
});
