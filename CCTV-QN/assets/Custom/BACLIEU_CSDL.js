﻿$(document).ready(function () {
 
    var chartDautuCong = new CanvasJS.Chart("chartDautuCong", {
        theme: "light2",
        animationEnabled: true,

        title: {
            text: "Chỉ số truy cập",
            fontSize: 16,
            indexLabelFontFamily: "Arial",
        },

        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 12,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} (#percent%)",
            dataPoints: [
                { y: 4, label: "TP.Bạc Liêu ", color: "#0a94cc" },
                { y: 3, label: "Giá Rai", color: "#1fbaf9" },
                { y: 4, label: "Đông Hải ", color: "#4fc5f5" },
                { y: 7, label: "Phước Long", color: "#5aa8c7" },
                { y: 4, label: "Ngan Dừa", color: "#bfdbe6" },
            ]
        }]
    });
    chartDautuCong.render();

  
   //thủy lợi
    var chartTL = new CanvasJS.Chart("chartTL", {
        theme: "light2",
        title: {
            text: "Lưu lượng sử dụng tại các điểm",
            fontFamily: "arial",
            fontSize: 16,
           
        },

        axisY: {
            title: "(TB)",
        },
        data: [{
            type: "column",
            yValueFormatString: "#,### TB",
            indexLabel: "{y}",
            dataPoints: [
                { label: "TP.Bạc Liêu", y: 206 },
                { label: "Giá Rai", y: 163 },
                { label: "Đông Hải", y: 154 },
                { label: "Hòa Bình", y: 176 },
                { label: "Hồng Dân", y: 164 },
                { label: "Phước Long", y: 156 },
                { label: "Gành Hào", y: 143 },
                { label: "Ngan Dừa", y: 164 },
                { label: "Châu Hưng", y: 153 },
            ]
        }]
    });

    function updateChartTL() {
        var boilerColor, deltaY, yVal;
        var dps = chartTL.options.data[0].dataPoints;
        var ten = ["TP.Bạc Liêu", "Giá Rai", "Đông Hải", "Hòa Bình", "Hồng Dân", "Phước Long", "Gành Hào", "Ngan Dừa", "Châu Hưng"];
        for (var i = 0; i < dps.length; i++) {
            deltaY = Math.round(2 + Math.random() * (-2 - 2));
            yVal = deltaY + dps[i].y > 0 ? dps[i].y + deltaY : 0;
            boilerColor = yVal > 200 ? "#FF2500" : yVal >= 170 ? "#FF6000" : yVal < 170 ? "#6B8E23 " : null;
            dps[i] = { label: ten[i], y: yVal, color: boilerColor };
        }
        chartTL.options.data[0].dataPoints = dps;
        chartTL.render();
    };
    updateChartTL();

    setInterval(function () { updateChartTL() }, 500);
 var dataPoints1 = [];
    var dataPoints2 = [];

    var chartMXH = new CanvasJS.Chart("chartMXH", {
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: "Chỉ số Truy cập",
            fontSize: 16,
            indexLabelFontFamily: "arial",
        },
        axisX: {
            title: ""
        },
        axisY: {
            prefix: "",
            includeZero: false
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            verticalAlign: "top",
            fontSize: 22,
            fontColor: "dimGrey",
            itemclick: toggleDataSeriesMXH
        },
        data: [{
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            xValueFormatString: "hh:mm:ss TT",
            showInLegend: true,
            name: "Facebook",
            dataPoints: dataPoints1
        },
        {
            type: "line",
            xValueType: "dateTime",
            yValueFormatString: "$####.00",
            showInLegend: true,
            name: "Twitter",
            dataPoints: dataPoints2
        }]
    });

    function toggleDataSeriesMXH(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else {
            e.dataSeries.visible = true;
        }
        chartMXH.render();
    }

    var updateInterval = 3000;
    // initial value
    var yValue1 = 600;
    var yValue2 = 605;

    var time = new Date;
    // starting at 9.30 am
    time.setHours(9);
    time.setMinutes(30);
    time.setSeconds(00);
    time.setMilliseconds(00);

    function updateChart(count) {
        count = count || 1;
        var deltaY1, deltaY2;
        for (var i = 0; i < count; i++) {
            time.setTime(time.getTime() + updateInterval);
            deltaY1 = .5 + Math.random() * (-.5 - .5);
            deltaY2 = .5 + Math.random() * (-.5 - .5);

            // adding random value and rounding it to two digits.
            yValue1 = Math.round((yValue1 + deltaY1) * 100) / 100;
            yValue2 = Math.round((yValue2 + deltaY2) * 100) / 100;

            // pushing the new values
            dataPoints1.push({
                x: time.getTime(),
                y: yValue1
            });
            dataPoints2.push({
                x: time.getTime(),
                y: yValue2
            });
        }

        // updating legend text with  updated with y Value
        chartMXH.options.data[0].legendText = "Nội mạng: " + yValue1;
        chartMXH.options.data[1].legendText = "Ngoại mạng: " + yValue2;
        chartMXH.render();
    }
    // generates first set of dataPoints
    updateChart(100);
    setInterval(function () { updateChart() }, updateInterval);

});
